package net.sigmainfo.lf.automation.common;

import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.HttpURLConnection;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.testng.Assert.fail;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.listener.Reporter;

import org.openqa.selenium.Proxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.CaptureType;

/**
 * Created by : Rengarajan.m on 27-09-2017. Test class : AbstractTests.java
 * Description : Drives automation suite and delegates testng annotations
 * Includes : 1. Setup and quit method for browser opening and closing for
 * portal cases 2. Initializes test data and test property files 3. Custom
 * Reporting methods
 */

@TestExecutionListeners(inheritListeners = false, listeners = { DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class })
@WebAppConfiguration
@Test
public class AbstractTests extends AbstractTestNGSpringContextTests {

	private static Logger logger = LoggerFactory.getLogger(AbstractTests.class);

	private static boolean setupDone = false;

	@Autowired
	ApiParam apiParam;

	public static WebDriver webdriver;

	public static WebElement webElement;

	public static WebElement webElements;

	public static Properties prop = new Properties();

	public static String dayString = null;

	public static String monthString = null;

	public static String ScreenShotLocation = ".\\Screenshot";

	public static long random;

	public List<WebElement> rows = null;

	public List<WebElement> columns = null;

	public WebElement column = null;

	public WebElement row = null;

	public ArrayList<String> ar = new ArrayList<String>();

	public HashSet<String> hs = new HashSet<String>();

	private static String[] links = null;

	public int INCORRECT_AUTHORIZATION = 401;
	public int INTERNAL_SERVER_ERROR = 500;
	public int BAD_REQUEST = 400;
	public int NOT_FOUND = 404;
	public int SUCCESS_RESPONSE = 200;
	public int SUCCESS_RESPONSE_WITH_NOCONTENT = 204;
	public int METHOD_NOT_ALLOWED = 405;

	public static BrowserMobProxyServer server;
	public static String CHROME_DRIVER_PATH = ".\\drivers\\chromedriver.exe";

	public void postConstruct(String env) throws Exception {
		if (!setupDone) {

			logger.info("=========================== Post Construct Invoked. ==============================");
			setupDBParams(env);
			setupDone = true;
		}
	}

	public void setupDBParams(String env) throws FileNotFoundException, IOException {

		logger.info("------------------------------- READING API PROPERTIES FILE -----------------------------------");
		logger.info("Environment : " + env);
		String environmentenvironment = ".\\" + env + "\\api.properties";
		logger.info("Configuration file path  : " + environmentenvironment);
		prop.load(new FileInputStream(environmentenvironment));
		logger.info("------------------------------------------------------------------------------------------------");

		logger.info("-----------------------  READING Back office Detail ---------------------------------");
		apiParam.BackOfficeURL = prop.getProperty("BackOfficeURL");
		logger.info("apiParam.BackOfficeURL.WebElement : " + apiParam.BackOfficeURL);
		apiParam.BorrowerPortal = prop.getProperty("BorrowerPortal");
		logger.info("apiParam.BorrowerPortal.WebElement : " + apiParam.BorrowerPortal);
		apiParam.BackOfficeImageURL = prop.getProperty("BackOfficeImageURL");
		logger.info("apiParam.BackOfficeImageURL.WebElement : " + apiParam.BackOfficeImageURL);
		apiParam.BrandingLogoSearchPage = prop.getProperty("BrandingLogoSearchpage");
		logger.info("apiParam.BrandingLogoSearchPage.WebElement : " + apiParam.BrandingLogoSearchPage);
		apiParam.UserName = prop.getProperty("UserName");
		logger.info("apiParam.UserName.WebElement : " + apiParam.UserName);
		apiParam.Password = prop.getProperty("Password");
		logger.info("apiParam.Password.WebElement : " + apiParam.Password);
		apiParam.ConsentCheckBox = prop.getProperty("ConsentCheckBox");
		logger.info("apiParam.ConsentCheckBox.WebElement : " + apiParam.ConsentCheckBox);
		apiParam.SignButton = prop.getProperty("SignButton");
		logger.info("apiParam.SignButton.WebElement : " + apiParam.SignButton);
		apiParam.BrandingLogoLoginPage = prop.getProperty("BrandingLogoLoginpage");
		logger.info("apiParam.BrandingLogoLoginPage.WebElement : " + apiParam.BrandingLogoLoginPage);
		apiParam.ProfileLink = prop.getProperty("ProfileLink");
		logger.info("apiParam.ProfileLink.WebElement : " + apiParam.ProfileLink);
		apiParam.LogOutLink = prop.getProperty("LogOutLink");
		logger.info("apiParam.LogOutLink.WebElement : " + apiParam.LogOutLink);
		apiParam.ErrorMessageonHomePage = prop.getProperty("ErrorMessageonHomePage");
		logger.info("apiParam.ErrorMessageonHomePage.WebElement : " + apiParam.ErrorMessageonHomePage);
		apiParam.ConsentMessageonHomePage = prop.getProperty("ConsentMessageonHomePage");
		logger.info("apiParam.ConsentMessageonHomePage.WebElement : " + apiParam.ConsentMessageonHomePage);
		apiParam.Forgotpasswordlink = prop.getProperty("Forgotpasswordlink");
		logger.info("apiParam.Forgotpasswordlink.WebElement : " + apiParam.Forgotpasswordlink);
		apiParam.MessageonResetPasswordPage = prop.getProperty("MessageonResetPasswordPage");
		logger.info("apiParam.MessageonResetPasswordPage.WebElement : " + apiParam.MessageonResetPasswordPage);
		apiParam.SendRestPasswordButton = prop.getProperty("SendRestPasswordButton");
		logger.info("apiParam.SendRestPasswordButton.WebElement : " + apiParam.SendRestPasswordButton);
		apiParam.SuccessMessage = prop.getProperty("SuccessMessage");
		logger.info("apiParam.SuccessMessage.WebElement : " + apiParam.SuccessMessage);
		apiParam.TextOnRestPasswordPage = prop.getProperty("TextOnRestPasswordPage");
		logger.info("apiParam.TextOnRestPasswordPage.WebElement : " + apiParam.TextOnRestPasswordPage);
		apiParam.NewPasswordField = prop.getProperty("NewPasswordField");
		logger.info("apiParam.NewPasswordField.WebElement : " + apiParam.NewPasswordField);
		apiParam.ConfirmPasswordField = prop.getProperty("ConfirmPasswordField");
		logger.info("apiParam.ConfirmPasswordField.WebElement : " + apiParam.ConfirmPasswordField);
		apiParam.RestButton = prop.getProperty("RestButton");
		logger.info("apiParam.RestButton.WebElement : " + apiParam.RestButton);
		apiParam.SuccessMessageAfterRest = prop.getProperty("SuccessMessageAfterRest");
		logger.info("apiParam.SuccessMessageAfterRest.WebElement : " + apiParam.SuccessMessageAfterRest);
		apiParam.ReLogin = prop.getProperty("ReLogin");
		logger.info("apiParam.ReLogin.WebElement : " + apiParam.ReLogin);
		apiParam.ErrorMessageAfterRest = prop.getProperty("ErrorMessageAfterRest");
		logger.info("apiParam.ErrorMessageAfterRest.WebElement : " + apiParam.ErrorMessageAfterRest);
		apiParam.verifyPasswordErrorMessage = prop.getProperty("verifyPasswordErrorMessage");
		logger.info("apiParam.verifyPasswordErrorMessage.WebElement : " + apiParam.verifyPasswordErrorMessage);
		apiParam.ApplicationNumber = prop.getProperty("ApplicationNumber");
		logger.info("apiParam.ApplicationNumber.WebElement : " + apiParam.ApplicationNumber);
		apiParam.ProductID = prop.getProperty("ProductID");
		logger.info("apiParam.ProductID.WebElement : " + apiParam.ProductID);
		apiParam.FirstName = prop.getProperty("FirstName");
		logger.info("apiParam.FirstName.WebElement : " + apiParam.FirstName);
		apiParam.LastName = prop.getProperty("LastName");
		logger.info("apiParam.LastName.WebElement : " + apiParam.LastName);
		apiParam.BusinessEmail = prop.getProperty("BusinessEmail");
		logger.info("apiParam.BusinessEmail.WebElement : " + apiParam.BusinessEmail);
		apiParam.BusinessName = prop.getProperty("BusinessName");
		logger.info("apiParam.BusinessName.WebElement : " + apiParam.BusinessName);
		apiParam.Status = prop.getProperty("Status");
		logger.info("apiParam.Status.WebElement : " + apiParam.Status);
		apiParam.ApplicationDate = prop.getProperty("ApplicationDate");
		logger.info("apiParam.ApplicationDate.WebElement : " + apiParam.ApplicationDate);
		apiParam.ExpiryDate = prop.getProperty("ExpiryDate");
		logger.info("apiParam.ExpiryDate.WebElement : " + apiParam.ExpiryDate);
		apiParam.MobileNumber = prop.getProperty("MobileNumber");
		logger.info("apiParam.MobileNumber.WebElement : " + apiParam.MobileNumber);
		apiParam.SearchButton = prop.getProperty("SearchButton");
		logger.info("apiParam.SearchButton.WebElement : " + apiParam.SearchButton);
		apiParam.SearchResult = prop.getProperty("SearchResult");
		logger.info("apiParam.SearchResult.WebElement : " + apiParam.SearchResult);
		apiParam.MessageOnSearchPage = prop.getProperty("MessageOnSearchPage");
		logger.info("apiParam.MessageOnSearchPage.WebElement : " + apiParam.MessageOnSearchPage);
		apiParam.Workflow = prop.getProperty("Workflow");
		logger.info("apiParam.Workflow.WebElement : " + apiParam.Workflow);
		logger.info("--------------------------------------------------------------------------------------");

		logger.info("--------------------- READING Borrwer portal Detail ---------------------------");
		apiParam.BPURL = prop.getProperty("BPURL");
		logger.info("apiParam.BPURL.WebElement : " + apiParam.BPURL);
		apiParam.BPImageURL = prop.getProperty("BPImageURL");
		logger.info("apiParam.BPImageURL.WebElement : " + apiParam.BPImageURL);
		apiParam.BPBrandingLogo = prop.getProperty("BPBrandingLogo");
		logger.info("apiParam.BPBrandingLogo.WebElement : " + apiParam.BPBrandingLogo);
		apiParam.BPSignButton = prop.getProperty("BPSignButton");
		logger.info("apiParam.BPSignButton.WebElement : " + apiParam.BPSignButton);
		apiParam.SignInPageEmailAddress = prop.getProperty("SignInPageEmailAddress");
		logger.info("apiParam.SignInPageEmailAddress.WebElement : " + apiParam.SignInPageEmailAddress);
		apiParam.SignInPagePassword = prop.getProperty("SignInPagePassword");
		logger.info("apiParam.SignInPagePassword.WebElement : " + apiParam.SignInPagePassword);
		apiParam.SignInPageLoginButton = prop.getProperty("SignInPageLoginButton");
		logger.info("apiParam.SignInPageLoginButton.WebElement : " + apiParam.SignInPageLoginButton);
		apiParam.SignInPageForgotPasswordLink = prop.getProperty("SignInPageForgotPasswordLink");
		logger.info("apiParam.SignInPageForgotPasswordLink.WebElement : " + apiParam.SignInPageForgotPasswordLink);
		apiParam.SignInErrorMessage = prop.getProperty("SignInErrorMessage");
		logger.info("apiParam.SignInErrorMessage.WebElement : " + apiParam.SignInErrorMessage);
		apiParam.ForgotPasswordPageEmailAddress = prop.getProperty("ForgotPasswordPageEmailAddress");
		logger.info("apiParam.ForgotPasswordPageEmailAddress.WebElement : " + apiParam.ForgotPasswordPageEmailAddress);
		apiParam.ForgotPasswordTitle = prop.getProperty("ForgotPasswordTitle");
		logger.info("apiParam.ForgotPasswordTitle.WebElement : " + apiParam.ForgotPasswordTitle);
		apiParam.FotgotPasswordSubmitButton = prop.getProperty("FotgotPasswordSubmitButton");
		logger.info("apiParam.FotgotPasswordSubmitButton.WebElement : " + apiParam.FotgotPasswordSubmitButton);
		apiParam.ProfilePage = prop.getProperty("ProfilePage");
		logger.info("apiParam.ProfilePage.WebElement : " + apiParam.ProfilePage);
		apiParam.ToDoPage = prop.getProperty("ToDoPage");
		logger.info("apiParam.ToDoPage.WebElement : " + apiParam.ToDoPage);
		apiParam.OverViewPage = prop.getProperty("OverViewPage");
		logger.info("apiParam.OverViewPage.WebElement : " + apiParam.OverViewPage);
		apiParam.ChangePasswordPage = prop.getProperty("ChangePasswordPage");
		logger.info("apiParam.ChangePasswordPage.WebElement : " + apiParam.ChangePasswordPage);
		apiParam.BorrowerName = prop.getProperty("BorrowerName");
		logger.info("apiParam.BorrowerName.WebElement : " + apiParam.BorrowerName);
		apiParam.ChangePasswordLink = prop.getProperty("ChangePasswordLink");
		logger.info("apiParam.ChangePasswordLink.WebElement : " + apiParam.ChangePasswordLink);
		apiParam.BPLogout = prop.getProperty("BPLogout");
		logger.info("apiParam.BPLogout.WebElement : " + apiParam.BPLogout);
		apiParam.SuccessMessageOnForgotPasswordPage = prop.getProperty("SuccessMessageOnForgotPasswordPage");
		logger.info("apiParam.SuccessMessageOnForgotPasswordPage.WebElement : "
				+ apiParam.SuccessMessageOnForgotPasswordPage);
		apiParam.SuccessMessageOnChangePasswordPage = prop.getProperty("SuccessMessageOnChangePasswordPage");
		logger.info("apiParam.SuccessMessageOnChangePasswordPage.WebElement : "
				+ apiParam.SuccessMessageOnChangePasswordPage);
		apiParam.BPBusinessName = prop.getProperty("BPBusinessName");
		logger.info("apiParam.BPBusinessName.WebElement : " + apiParam.BPBusinessName);
		apiParam.BPAmountSeeking = prop.getProperty("BPAmountSeeking");
		logger.info("apiParam.BPAmountSeeking.WebElement : " + apiParam.BPAmountSeeking);
		apiParam.BPUseOfFunds = prop.getProperty("BPUseOfFunds");
		logger.info("apiParam.BPUseOfFunds.WebElement : " + apiParam.BPUseOfFunds);
		apiParam.BPAnnualBusinessRevenue = prop.getProperty("BPAnnualBusinessRevenue");
		logger.info("apiParam.BPAnnualBusinessRevenue.WebElement : " + apiParam.BPAnnualBusinessRevenue);
		apiParam.BPFirstName = prop.getProperty("BPFirstName");
		logger.info("apiParam.BPFirstName.WebElement : " + apiParam.BPFirstName);
		apiParam.BPLastName = prop.getProperty("BPLastName");
		logger.info("apiParam.BPLastName.WebElement : " + apiParam.BPLastName);
		apiParam.BPPhone = prop.getProperty("BPPhone");
		logger.info("apiParam.BPPhone.WebElement : " + apiParam.BPPhone);
		apiParam.BPEmail = prop.getProperty("BPEmail");
		logger.info("apiParam.BPEmail.WebElement : " + apiParam.BPEmail);
		apiParam.BPPaasword = prop.getProperty("BPPaasword");
		logger.info("apiParam.BPPaasword.WebElement : " + apiParam.BPPaasword);
		apiParam.BPConfirmPassword = prop.getProperty("BPConfirmPassword");
		logger.info("apiParam.BPConfirmPassword.WebElement : " + apiParam.BPConfirmPassword);
		apiParam.BPNextButtonInBasicTab = prop.getProperty("BPNextButtonInBasicTab");
		logger.info("apiParam.BPNextButtonInBasicTab.WebElement : " + apiParam.BPNextButtonInBasicTab);
		apiParam.BusinessTabDBA = prop.getProperty("BusinessTabDBA");
		logger.info("apiParam.BusinessTabDBA.WebElement : " + apiParam.BusinessTabDBA);
		apiParam.BusinessTabBusinessAddress = prop.getProperty("BusinessTabBusinessAddress");
		logger.info("apiParam.BusinessTabBusinessAddress.WebElement : " + apiParam.BusinessTabBusinessAddress);
		apiParam.BusinessTabCity = prop.getProperty("BusinessTabCity");
		logger.info("apiParam.BusinessTabCity.WebElement : " + apiParam.BusinessTabCity);
		apiParam.BusinessTabState = prop.getProperty("BusinessTabState");
		logger.info("apiParam.BusinessTabState.WebElement : " + apiParam.BusinessTabState);
		apiParam.BusinessTabZipcode = prop.getProperty("BusinessTabZipcode");
		logger.info("apiParam.BusinessTabZipcode.WebElement : " + apiParam.BusinessTabZipcode);
		apiParam.BusinessTabBusinessPhone = prop.getProperty("BusinessTabBusinessPhone");
		logger.info("apiParam.BusinessTabBusinessPhone.WebElement : " + apiParam.BusinessTabBusinessPhone);
		apiParam.BusinessTabMonth = prop.getProperty("BusinessTabMonth");
		logger.info("apiParam.BusinessTabMonth.WebElement : " + apiParam.BusinessTabMonth);
		apiParam.BusinessTabDay = prop.getProperty("BusinessTabDay");
		logger.info("apiParam.BusinessTabDay.WebElement : " + apiParam.BusinessTabDay);
		apiParam.BusinessTabYear = prop.getProperty("BusinessTabYear");
		logger.info("apiParam.BusinessTabYear.WebElement : " + apiParam.BusinessTabYear);
		apiParam.BusinessTabBusinessWebsite = prop.getProperty("BusinessTabBusinessWebsite");
		logger.info("apiParam.BusinessTabBusinessWebsite.WebElement : " + apiParam.BusinessTabBusinessWebsite);
		apiParam.BusinessTabLegalEntity = prop.getProperty("BusinessTabLegalEntity");
		logger.info("apiParam.BusinessTabLegalEntity.WebElement : " + apiParam.BusinessTabLegalEntity);
		apiParam.BusinessTabBusinessLoacation = prop.getProperty("BusinessTabBusinessLoacation");
		logger.info("apiParam.BusinessTabBusinessLoacation.WebElement : " + apiParam.BusinessTabBusinessLoacation);
		apiParam.BusinessTabTaxId = prop.getProperty("BusinessTabTaxId");
		logger.info("apiParam.BusinessTabTaxId.WebElement : " + apiParam.BusinessTabTaxId);
		apiParam.BusinessTabIndustry = prop.getProperty("BusinessTabIndustry");
		logger.info("apiParam.BusinessTabIndustry.WebElement : " + apiParam.BusinessTabIndustry);
		apiParam.BusinessTabImportant = prop.getProperty("BusinessTabImportant");
		logger.info("apiParam.BusinessTabImportant.WebElement : " + apiParam.BusinessTabImportant);
		apiParam.BusinessTabNext = prop.getProperty("BusinessTabNext");
		logger.info("apiParam.BusinessTabNext.WebElement : " + apiParam.BusinessTabNext);
		apiParam.MessageTitleOnCreatePasswordPage = prop.getProperty("MessageTitleOnCreatePasswordPage");
		logger.info(
				"apiParam.MessageTitleOnCreatePasswordPage.WebElement : " + apiParam.MessageTitleOnCreatePasswordPage);
		apiParam.BPCreateNewPassword = prop.getProperty("BPCreateNewPassword");
		logger.info("apiParam.BPCreateNewPassword.WebElement : " + apiParam.BPCreateNewPassword);
		apiParam.BPCreateConfirmPassword = prop.getProperty("BPCreateConfirmPassword");
		logger.info("apiParam.BPCreateConfirmPassword.WebElement : " + apiParam.BPCreateConfirmPassword);
		apiParam.BPCreatePasswordButton = prop.getProperty("BPCreatePasswordButton");
		logger.info("apiParam.BPCreatePasswordButton.WebElement : " + apiParam.BPCreatePasswordButton);
		apiParam.MessageAfterCraetePasswordRequest = prop.getProperty("MessageAfterCraetePasswordRequest");
		logger.info("apiParam.MessageAfterCraetePasswordRequest.WebElement : "
				+ apiParam.MessageAfterCraetePasswordRequest);
		apiParam.SuccessMessageAftersuccessCreatePasswordRequest = prop
				.getProperty("SuccessMessageAftersuccessCreatePasswordRequest");
		logger.info("apiParam.SuccessMessageAftersuccessCreatePasswordRequest.WebElement : "
				+ apiParam.SuccessMessageAftersuccessCreatePasswordRequest);
		apiParam.OwnersEmail = prop.getProperty("OwnersEmail");
		logger.info("apiParam.OwnersEmail.WebElement : " + apiParam.OwnersEmail);
		apiParam.OwnersFirstName = prop.getProperty("OwnersFirstName");
		logger.info("apiParam.OwnersFirstName.WebElement : " + apiParam.OwnersFirstName);
		apiParam.OwnersLastName = prop.getProperty("OwnersLastName");
		logger.info("apiParam.OwnersLastName.WebElement : " + apiParam.OwnersLastName);
		apiParam.OwnersAddress = prop.getProperty("OwnersAddress");
		logger.info("apiParam.OwnersAddress.WebElement : " + apiParam.OwnersAddress);
		apiParam.OwnersCity = prop.getProperty("OwnersCity");
		logger.info("apiParam.OwnersCity.WebElement : " + apiParam.OwnersCity);
		apiParam.OwnersState = prop.getProperty("OwnersState");
		logger.info("apiParam.OwnersState.WebElement : " + apiParam.OwnersState);
		apiParam.OwnersZipcode = prop.getProperty("OwnersZipcode");
		logger.info("apiParam.OwnersZipcode.WebElement : " + apiParam.OwnersZipcode);
		apiParam.OwnersPhoneNo = prop.getProperty("OwnersPhoneNo");
		logger.info("apiParam.OwnersPhoneNo.WebElement : " + apiParam.OwnersPhoneNo);
		apiParam.Ownersmobileno = prop.getProperty("Ownersmobileno");
		logger.info("apiParam.Ownersmobileno.WebElement : " + apiParam.Ownersmobileno);
		apiParam.OwnersOwnership = prop.getProperty("OwnersOwnership");
		logger.info("apiParam.OwnersOwnership.WebElement : " + apiParam.OwnersOwnership);
		apiParam.OwnersSSN = prop.getProperty("OwnersSSN");
		logger.info("apiParam.OwnersSSN.WebElement : " + apiParam.OwnersSSN);
		apiParam.OwnersMonth = prop.getProperty("OwnersMonth");
		logger.info("apiParam.OwnersMonth.WebElement : " + apiParam.OwnersMonth);
		apiParam.OwnersDay = prop.getProperty("OwnersDay");
		logger.info("apiParam.OwnersDay.WebElement : " + apiParam.OwnersDay);
		apiParam.OwnersYear = prop.getProperty("OwnersYear");
		logger.info("apiParam.OwnersYear.WebElement : " + apiParam.OwnersYear);
		apiParam.OwnersNext = prop.getProperty("OwnersNext");
		logger.info("apiParam.OwnersNext.WebElement : " + apiParam.OwnersNext);
		apiParam.FinalFinish = prop.getProperty("FinalFinish");
		logger.info("apiParam.FinalFinish.WebElement : " + apiParam.FinalFinish);
		apiParam.ValidationMessageOnTodo = prop.getProperty("ValidationMessageOnTodo");
		logger.info("apiParam.ValidationMessageOnTodo.WebElement : " + apiParam.ValidationMessageOnTodo);
		apiParam.FinancialTabProgressBar = prop.getProperty("FinancialTabProgressBar");
		logger.info("apiParam.FinancialTabProgressBar.WebElement : " + apiParam.FinancialTabProgressBar);
		apiParam.OwnersTabProgressBar = prop.getProperty("OwnersTabProgressBar");
		logger.info("apiParam.OwnersTabProgressBar.WebElement : " + apiParam.OwnersTabProgressBar);
		apiParam.BusinessTabProgressBar = prop.getProperty("BusinessTabProgressBar");
		logger.info("apiParam.BusinessTabProgressBar.WebElement : " + apiParam.BusinessTabProgressBar);
		apiParam.BPProgressBar = prop.getProperty("BPProgressBar");
		logger.info("apiParam.BPProgressBar.WebElement : " + apiParam.BPProgressBar);
		apiParam.BPDuplicateCheckMessage = prop.getProperty("BPDuplicateCheckMessage");
		logger.info("apiParam.BPDuplicateCheckMessage.WebElement : " + apiParam.BPDuplicateCheckMessage);
		apiParam.BusinessStartDateErrorMessage = prop.getProperty("BusinessStartDateErrorMessage");
		logger.info("apiParam.BusinessStartDateErrorMessage.WebElement : " + apiParam.BusinessStartDateErrorMessage);
		apiParam.OwnerShipErrorMessage = prop.getProperty("OwnerShipErrorMessage");
		logger.info("apiParam.OwnerShipErrorMessage.WebElement : " + apiParam.OwnerShipErrorMessage);
		apiParam.ErrormessageAge = prop.getProperty("ErrormessageAge");
		logger.info("apiParam.ErrormessageAge.WebElement : " + apiParam.ErrormessageAge);
		apiParam.ChangePasswordPageRestButton = prop.getProperty("ChangePasswordPageRestButton");
		logger.info("apiParam.ChangePasswordPageRestButton.WebElement : " + apiParam.ChangePasswordPageRestButton);
		apiParam.TodoVlidationMessage = prop.getProperty("TodoVlidationMessage");
		logger.info("apiParam.TodoVlidationMessage.WebElement : " + apiParam.TodoVlidationMessage);
		apiParam.TodoDeclineMessage = prop.getProperty("TodoDeclineMessage");
		logger.info("apiParam.TodoDeclineMessage.WebElement : " + apiParam.TodoDeclineMessage);
		apiParam.TodoApplicationNo = prop.getProperty("TodoPageApplicationNo");
		logger.info("apiParam.TodoApplicationNo.WebElement : " + apiParam.TodoApplicationNo);
		apiParam.SuccessMessageAfterBankLinked = prop.getProperty("SuccessMessageAfterBankLinked");
		logger.info("apiParam.SuccessMessageAfterBankLinked.WebElement : " + apiParam.SuccessMessageAfterBankLinked);
		apiParam.BankSuccessMsgUI = prop.getProperty("BankSuccessMsgUI");
		logger.info("apiParam.BankSuccessMsgUI.WebElement : " + apiParam.BankSuccessMsgUI);
		apiParam.TodoBankSuccessMsg = prop.getProperty("TodoBankSuccessMsg");
		logger.info("apiParam.TodoBankSuccessMsg.WebElement : " + apiParam.TodoBankSuccessMsg);
		logger.info("------------------------------------------------------------------------------------------------");

		logger.info("--------------------- READING Mongo DB Connection Detail ---------------------------");
		apiParam.DBHostName = prop.getProperty("DBHostName");
		logger.info("apiParam.DBHostName.WebElement : " + apiParam.DBHostName);
		apiParam.DBPort = prop.getProperty("DBPort");
		logger.info("apiParam.DBPort.WebElement : " + apiParam.DBPort);
		apiParam.DBUserName = prop.getProperty("DBUserName");
		logger.info("apiParam.DBUserName.WebElement : " + apiParam.DBUserName);
		apiParam.DBPassword = prop.getProperty("DBPassword");
		logger.info("apiParam.DBPassword.WebElement : " + apiParam.DBPassword);
		apiParam.AuthenticationDB = prop.getProperty("AuthenticationDB");
		logger.info("apiParam.AuthenticationDB.WebElement : " + apiParam.AuthenticationDB);
		logger.info("------------------------------------------------------------------------------------------------");
		logger.info("--------------------- READING Inbox Details ---------------------------");
		apiParam.InboxURL = prop.getProperty("InboxURL");
		logger.info("apiParam.InboxURL.WebElement : " + apiParam.InboxURL);
		apiParam.EmailtextField = prop.getProperty("EmailtextField");
		logger.info("apiParam.EmailtextField.WebElement : " + apiParam.EmailtextField);
		apiParam.Go = prop.getProperty("Go");
		logger.info("apiParam.Go.WebElement : " + apiParam.Go);
		apiParam.RestPasswordSubject = prop.getProperty("RestPasswordSubject");
		logger.info("apiParam.RestPasswordSubject.WebElement : " + apiParam.RestPasswordSubject);
		apiParam.RestPasswordlink = prop.getProperty("RestPasswordlink");
		logger.info("apiParam.RestPasswordlink.WebElement : " + apiParam.RestPasswordlink);
		apiParam.TimePeriod = prop.getProperty("TimePeriod");
		logger.info("apiParam.TimePeriod.WebElement : " + apiParam.TimePeriod);
		apiParam.IframeID = prop.getProperty("IframeID");
		logger.info("apiParam.IframeID.WebElement : " + apiParam.IframeID);
		logger.info("-------------------------------------------------------------------------------------");

		logger.info("--------------------- READING Simulation Data creation ---------------------------");
		apiParam.baseresturl = prop.getProperty("baseresturl");
		logger.info("apiParam.baseresturl : " + apiParam.baseresturl);
		apiParam.AuthToken = prop.getProperty("AuthToken");
		logger.info("apiParam.AuthToken : " + apiParam.AuthToken);
		apiParam.contentType = prop.getProperty("contentType");
		logger.info("apiParam.contentType : " + apiParam.contentType);
		apiParam.portNo = prop.getProperty("portNo");
		logger.info("apiParam.portNo : " + apiParam.portNo);
		apiParam.ASPort = prop.getProperty("ASPort");
		logger.info("apiParam.ASPort : " + apiParam.ASPort);
		apiParam.PlaidBankPort = prop.getProperty("PlaidBankPort");
		logger.info("apiParam.PlaidBankPort : " + apiParam.PlaidBankPort);
		apiParam.PlaidBankAuthToken = prop.getProperty("PlaidBankAuthToken");
		logger.info("apiParam.PlaidBankAuthToken : " + apiParam.PlaidBankAuthToken);

		logger.info("-------------------------------------------------------------------------------------");

		logger.info("--------------------- READING File Utility ---------------------------");
		apiParam.ScreenShotPath = prop.getProperty("ScreenShotPath");
		logger.info("apiParam.ScreenShotPath : " + apiParam.ScreenShotPath);
		apiParam.RecorderFilePath = prop.getProperty("RecorderFilePath");
		logger.info("apiParam.RecorderFilePath : " + apiParam.RecorderFilePath);
		logger.info("-------------------------------------------------------------------------------------");

		logger.info("--------------------- DocuSign ---------------------------");
		apiParam.DocusignAcceptCheckBox = prop.getProperty("DocusignAcceptCheckBox");
		logger.info("apiParam.DocusignAcceptCheckBox : " + apiParam.DocusignAcceptCheckBox);
		apiParam.DocusignContinue = prop.getProperty("DocusignContinue");
		logger.info("apiParam.DocusignContinue : " + apiParam.DocusignContinue);
		apiParam.DocusignStart = prop.getProperty("DocusignStart");
		logger.info("apiParam.DocusignStart : " + apiParam.DocusignStart);
		apiParam.DocusignSign = prop.getProperty("DocusignSign");
		logger.info("apiParam.DocusignSign : " + apiParam.DocusignSign);
		apiParam.DocusignAdoptSign = prop.getProperty("DocusignAdoptSign");
		logger.info("apiParam.DocusignAdoptSign : " + apiParam.DocusignAdoptSign);
		apiParam.DocusignFinish = prop.getProperty("DocusignFinish");
		logger.info("apiParam.DocusignFinish : " + apiParam.DocusignFinish);
		apiParam.DocuSignLabel = prop.getProperty("DocuSignLabel");
		logger.info("apiParam.DocuSignLabel : " + apiParam.DocuSignLabel);
		logger.info("-------------------------------------------------------------------------------------");

		logger.info("--------------------- Plaid Details ---------------------------");
		apiParam.LinkBankAccount = prop.getProperty("LinkBankAccount");
		logger.info("apiParam.LinkBankAccount : " + apiParam.LinkBankAccount);
		apiParam.IFrameIDForPlaid = prop.getProperty("IFrameIDForPlaid");
		logger.info("apiParam.IFrameIDForPlaid : " + apiParam.IFrameIDForPlaid);
		apiParam.PlaidSearch = prop.getProperty("PlaidSearch");
		logger.info("apiParam.PlaidSearch : " + apiParam.PlaidSearch);
		apiParam.SelectionOfPlaidBank = prop.getProperty("SelectionOfPlaidBank");
		logger.info("apiParam.SelectionOfPlaidBank : " + apiParam.SelectionOfPlaidBank);
		apiParam.PlaidUserName = prop.getProperty("PlaidUserName");
		logger.info("apiParam.PlaidUserName : " + apiParam.PlaidUserName);
		apiParam.PlaidPassword = prop.getProperty("PlaidPassword");
		logger.info("apiParam.PlaidPassword : " + apiParam.PlaidPassword);
		apiParam.SubmitPlaidCredential = prop.getProperty("SubmitPlaidCredential");
		logger.info("apiParam.SubmitPlaidCredential : " + apiParam.SubmitPlaidCredential);
		apiParam.PlaidReCaptcha = prop.getProperty("PlaidReCaptcha");
		logger.info("apiParam.PlaidReCaptcha : " + apiParam.PlaidReCaptcha);
		apiParam.SendType = prop.getProperty("SendType");
		logger.info("apiParam.SendType : " + apiParam.SendType);
		apiParam.ContinueButtonAfterSendType = prop.getProperty("ContinueButtonAfterSendType");
		logger.info("apiParam.ContinueButtonAfterSendType : " + apiParam.ContinueButtonAfterSendType);
		apiParam.PlaidCode = prop.getProperty("PlaidCode");
		logger.info("apiParam.PlaidCode : " + apiParam.PlaidCode);
		apiParam.SubmitPlaidBank = prop.getProperty("SubmitPlaidBank");
		logger.info("apiParam.SubmitPlaidBank : " + apiParam.SubmitPlaidBank);
		apiParam.ContinueButtonAfterAccountLinked = prop.getProperty("ContinueButtonAfterAccountLinked");
		logger.info("apiParam.ContinueButtonAfterAccountLinked : " + apiParam.ContinueButtonAfterAccountLinked);
		apiParam.ClosePlaid = prop.getProperty("ClosePlaid");
		logger.info("apiParam.ClosePlaid : " + apiParam.ClosePlaid);
		logger.info("-------------------------------------------------------------------------------------");
		
		logger.info("--------------------- Finicity Details ---------------------------");
		apiParam.FinicityBankButton = prop.getProperty("FinicityBankButton");
		logger.info("apiParam.FinicityBankButton : " + apiParam.FinicityBankButton);
		apiParam.FinicityIframe = prop.getProperty("FinicityIframe");
		logger.info("apiParam.FinicityIframe : " + apiParam.FinicityIframe);
		apiParam.FinicityConsent = prop.getProperty("FinicityConsent");
		logger.info("apiParam.FinicityConsent : " + apiParam.FinicityConsent);
		apiParam.AddNewAccountButton = prop.getProperty("AddNewAccountButton");
		logger.info("apiParam.AddNewAccountButton : " + apiParam.AddNewAccountButton);
		apiParam.FinicitySearch = prop.getProperty("FinicitySearch");
		logger.info("apiParam.FinicitySearch : " + apiParam.FinicitySearch);
		apiParam.SelectFinBank = prop.getProperty("SelectFinBank");
		logger.info("apiParam.SelectFinBank : " + apiParam.SelectFinBank);
		apiParam.BankingUsername = prop.getProperty("BankingUsername");
		logger.info("apiParam.BankingUsername : " + apiParam.BankingUsername);
		apiParam.BankingPassword = prop.getProperty("BankingPassword");
		logger.info("apiParam.BankingPassword : " + apiParam.BankingPassword);
		apiParam.SubmitFinBankCredential = prop.getProperty("SubmitFinBankCredential");
		logger.info("apiParam.SubmitFinBankCredential : " + apiParam.SubmitFinBankCredential);
		apiParam.AddSelectedAccounts = prop.getProperty("AddSelectedAccounts");
		logger.info("apiParam.AddSelectedAccounts : " + apiParam.AddSelectedAccounts);
		apiParam.MakeDoneForAddingAccounts = prop.getProperty("MakeDoneForAddingAccounts");
		logger.info("apiParam.MakeDoneForAddingAccounts : " + apiParam.MakeDoneForAddingAccounts);
		logger.info("-------------------------------------------------------------------------------------");

	}

	public String toHHMMDD(long time) {

		String hms = String.format("%02d" + " hrs" + ":%02d" + " mins" + ":%02d" + " sec",
				TimeUnit.MILLISECONDS.toHours(time),
				TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)),
				TimeUnit.MILLISECONDS.toSeconds(time)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
		return hms;
	}

	public String getOnlyStrings(String s) {
		Pattern pattern = Pattern.compile("[^a-z A-Z]");
		Matcher matcher = pattern.matcher(s);
		String str = matcher.replaceAll("");
		return str;
	}

	public static void InitializeBrowser(String browserType) {

		if (browserType.equalsIgnoreCase("CHROME")) {

			logger.info("## Setting " + browserType + " browser");
			System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
			System.setProperty("webdriver.chrome.bin",
					"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--allow-running-insecure-content");

			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setBrowserName("chrome");
			capabilities.setPlatform(Platform.WINDOWS);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
			logger.info("## " + browserType + "Browser driver path  : " + ".\\driver\\chromedriver.exe");
			capabilities.setCapability("chrome.binary",
					"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
			capabilities.setCapability("webdriveSr.chrome.bin",
					"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");

			webdriver = new ChromeDriver(options);
			logger.info("## Lanuched " + browserType + " browser");
		}

	}

	public static void MongoDBConnection() {

		ApiParam apiParam = new ApiParam();
		try {

			String URL = "mongodb://" + apiParam.DBUserName + ":" + apiParam.DBPassword + "@" + apiParam.DBHostName
					+ ":" + apiParam.DBPort + "/?authSource=" + apiParam.AuthenticationDB;
			logger.info("**********Mongo DB Connection URL : " + URL + "***********");
			MongoClientURI uri = new MongoClientURI(URL);
			MongoClient mongoClient = new MongoClient(uri);
			DB database = mongoClient.getDB("product");

			DBCollection collection = database.getCollection("product");

			BasicDBObject query = new BasicDBObject();
			query.put("Description", "Loc Product");

			DBCursor cursor = collection.find(query);
			try {
				while (cursor.hasNext()) {
					BasicDBObject document = (BasicDBObject) cursor.next();
					System.out.println(document.get("_id"));
					logger.info("**********Document ID : " + document.get("_id") + "***********");
				}
			} finally {
				cursor.close();
			}

		} catch (MongoException e) {
			logger.info("**********Error Message : " + e.getMessage() + "*************");
		}
	}

	public String day(String value) {

		switch (value) {
		case "01":
			dayString = " 1 ";
			break;
		case "02":
			dayString = " 2 ";
			break;
		case "03":
			dayString = " 3 ";
			break;
		case "04":
			dayString = " 4 ";
			break;
		case "05":
			dayString = " 5 ";
			break;
		case "06":
			dayString = " 6 ";
			break;
		case "07":
			dayString = " 7 ";
			break;
		case "08":
			dayString = " 8 ";
			break;
		case "09":
			dayString = " 9 ";
			break;
		case "10":
			dayString = " 10 ";
			break;
		default:
			dayString = value; // they are executed if none of the above case is
								// satisfied
			break;
		}
		return dayString;

	}

	public String month(String value)

	{
		switch (value) {
		case "01":
			monthString = " Jan ";
			break;
		case "02":
			monthString = " Feb ";
			break;
		case "03":
			monthString = " Mar ";
			break;
		case "04":
			monthString = " Apr ";
			break;
		case "05":
			monthString = " May ";
			break;
		case "06":
			monthString = " Jun ";
			break;
		case "07":
			monthString = " Jul ";
			break;
		case "08":
			monthString = " Aug ";
			break;
		case "09":
			monthString = " Sep ";
			break;
		case "10":
			monthString = " Oct ";
			break;
		case "11":
			monthString = " Nov ";
			break;
		case "12":
			monthString = " Dec ";
			break;
		case "1":
			monthString = " Jan ";
			break;
		case "2":
			monthString = " Feb ";
			break;
		case "3":
			monthString = " Mar ";
			break;
		case "4":
			monthString = " Apr ";
			break;
		case "5":
			monthString = " May ";
			break;
		case "6":
			monthString = " Jun ";
			break;
		case "7":
			monthString = " Jul ";
			break;
		case "8":
			monthString = " Aug ";
			break;
		case "9":
			monthString = " Sep ";
			break;
		}
		System.out.println("monthString :" + monthString);
		return (monthString);

	}

	public static void InputBox(By Element, String value) throws FileNotFoundException, IOException {
		webdriver.findElement(Element).clear();
		webdriver.findElement(Element).click();
		webdriver.findElement(Element).sendKeys(value);
	}

	public static void Button(By Element) throws FileNotFoundException, IOException {
		webdriver.findElement(Element).click();
	}

	public static void MouseScrollUp() {

		JavascriptExecutor jse1 = (JavascriptExecutor) webdriver;
		jse1.executeScript("window.scrollBy(0,-250)", "");
	}

	public static void MouseScrollDown() {

		JavascriptExecutor jse1 = (JavascriptExecutor) webdriver;
		jse1.executeScript("window.scrollBy(0,250)", "");
	}

	public static String getext(By Element) {
		String val = webdriver.findElement(Element).getText();
		return val;
	}

	public static String getElementByAttribute(By Element, String Value) {
		webElement = webdriver.findElement(Element);
		String img = webElement.getAttribute(Value);
		return img;
	}

	public static void DropDown(By Element, String Value) throws FileNotFoundException, IOException {
		Select selectBox = new Select(webdriver.findElement(Element));
		List<WebElement> selectOptions = selectBox.getOptions();
		Search: for (WebElement temp : selectOptions) {
			if (temp.getText().equalsIgnoreCase(Value)) {
				selectBox.selectByVisibleText(Value);
				break Search;
			} else {
			}
		}
	}

	public static String getext(By Element, int id) throws FileNotFoundException, IOException {
		String val = webdriver.findElements(Element).get(id).getText();
		return val;
	}

	public static void Button(By Element, int id) throws FileNotFoundException, IOException {
		webdriver.findElements(Element).get(id).click();
	}

	public static void SwitchToIframeID(String IdElement) throws InterruptedException {
		webdriver.switchTo().frame(webdriver.findElement(By.id(IdElement)));
		Thread.sleep(2000);
	}

	public static void ReturnToDefaultContent() throws InterruptedException {
		webdriver.switchTo().defaultContent();
		Thread.sleep(5000);
	}

	public static void SwitchToTab2() throws InterruptedException {
		ArrayList<String> tabs2 = new ArrayList<String>(webdriver.getWindowHandles());
		webdriver.switchTo().window(tabs2.get(0));
		webdriver.close();
		webdriver.switchTo().window(tabs2.get(1));
		Thread.sleep(7000);
	}

	public static long TakeScreenShot() throws Exception {
		File dir = new File("Screenshot");
		dir.mkdir();
		File scrFile = ((TakesScreenshot) webdriver).getScreenshotAs(OutputType.FILE);
		random = System.currentTimeMillis();
		String filename = ScreenShotLocation + "\\" + random + ".png";
		FileUtils.copyFile(scrFile, new File(filename));
		logger.info("*****ScreenShot File Name : " + random + "*******");
		return (random);

	}

	public static void AddScreenShotReport(String testresults) throws Exception {
		ApiParam apiParam = new ApiParam();
		String filePath = null;
		if (testresults.equalsIgnoreCase("Failed")) {
			long random = TakeScreenShot();
			filePath = apiParam.ScreenShotPath + "/" + random + ".png";
			logger.info("*****ScreenShot Path : " + filePath + "*******");
			Reporter.addScreenCaptureFromPath(filePath);
			Thread.sleep(1000);
		}
	}

	public static void JavascriptsselectMethod(By element) {
		JavascriptExecutor js = (JavascriptExecutor) webdriver;
		WebElement button = webdriver.findElement(element);
		js.executeScript("arguments[0].click();", button);
	}

	public static void WindowResize() throws Exception {
		webdriver.manage().window().setSize(new org.openqa.selenium.Dimension(1440, 900));
		Thread.sleep(2000);
	}

	public String AutoSuggestion(WebElement Element, int Id) {
		List<WebElement> rows = Element.findElements(By.tagName("tr"));
		Iterator<WebElement> i = rows.iterator();
		System.out.println("-----------------------------------------");
		while (i.hasNext()) {
			row = i.next();
			List<WebElement> columns = row.findElements(By.tagName("td"));
			Iterator<WebElement> j = columns.iterator();
			while (j.hasNext()) {
				column = j.next();
				ar.add(column.getText());
				hs.addAll(ar);
				ar.clear();
				ar.addAll(hs);
				// System.out.println(value);
			}
		}
		logger.info("*****Total Size in Auto Suggestion list : " + ar.size() + "*******");
		return (ar.get(Id));
	}

	public static void linkExists(String URLName) {
		try {
			logger.info("###Checking url exist or not ");
			HttpURLConnection.setFollowRedirects(false);

			HttpURLConnection con = (HttpURLConnection) new URL(URLName).openConnection();

			logger.info("*****### Got the connection*******");
			con.setRequestMethod("HEAD");
			logger.info("### Setting header .. ");
			logger.info("### Http Response code : " + con.getResponseCode());
			logger.info("### Http Response message : " + con.getResponseMessage());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void GetAllLinkUrlforPage() {

		List<WebElement> linksize = webdriver.findElements(By.tagName("a"));

		apiParam.linkcount = linksize.size();
		logger.info("### Total Links for page .. :" + apiParam.linkcount + "*************");
		links = new String[apiParam.linkcount];

		for (int i = 0; i < apiParam.linkcount; i++) {
			links[i] = linksize.get(i).getAttribute("href");
			apiParam.currentLink = links[i];
			logger.info("### Current link :" + apiParam.currentLink + "**********");
			linkExists(apiParam.currentLink);
		}

	}

	public static boolean isElementPresent(WebDriver driver, By locator) {
		try {
			driver.findElement(locator);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public static void waitForElementPresent(WebDriver driver, By locator) throws Exception {
		// Lets Wait 5 seconds To the element to appear
		for (int second = 0;; second++) {
			if (second >= 5)
				fail("timeout");
			if (isElementPresent(driver, locator))
				break;
			Thread.sleep(1000);
		}
	}

	public static void waitForElementPresent(WebDriver driver, By locator, int timeout) throws Exception {
		// Lets Wait 5 timeout To the element to sappear
		for (int second = 0;; second++) {
			if (second >= timeout)
				fail("timeout");
			if (isElementPresent(driver, locator))
				break;
			Thread.sleep(1000);
		}
	}

	public static void waitForElementNotPresent(WebDriver driver, By locator) throws Exception {
		// Lets Wait 60 Seconds To the progress bar disappear
		for (int second = 0;; second++) {
			if (second >= 60)
				fail("timeout");
			if (!driver.findElement(locator).isDisplayed())
				break;
			Thread.sleep(1000);
		}
	}

	public static void waitForElementNotPresent(WebDriver driver, By locator, int timeout) throws Exception {
		// Lets Wait x Seconds To the progress bar disappear
		for (int second = 0;; second++) {
			if (second >= timeout)
				fail("timeout");
			if (!driver.findElement(locator).isDisplayed())
				break;
			Thread.sleep(1000);
		}
	}

	public static void BackCommand() {
		webdriver.navigate().back();
	}

	public static void ForwardCommand() {
		webdriver.navigate().forward();
	}

	public static String getTagName(By Element) {
		WebElement Tag = webdriver.findElement(Element);
		return (Tag.getTagName());
	}

	public static String getCssValue(By Element, String propertyName) {
		WebElement cssvalue = webdriver.findElement(Element);
		// propertyName = font-size
		return (cssvalue.getCssValue(propertyName));
	}

	public static void getLocation(By Element) {
		WebElement name = webdriver.findElement(Element);
		Point point = name.getLocation();
		String strLine = System.getProperty("line.separator");
		logger.info("X cordinate# " + point.x + strLine + "Y cordinate# " + point.y);
	}

	public static void openNewTab(By locator) {
		webdriver.findElement(locator).sendKeys(Keys.CONTROL + "t");
		ArrayList<String> tabs = new ArrayList<String>(webdriver.getWindowHandles());
		webdriver.switchTo().window(tabs.get(0));
	}

	public static void WritingInFile(String filename, String val) throws Throwable {
		WriteFile data = new WriteFile(filename, true);
		data.writeToFile(val);
		System.setProperty("org.uncommons.reportng.escape-output", "false");
	}

	public static void ExecuteAutoITProg() throws InterruptedException, IOException {
		Runtime.getRuntime().exec("scenario.exe");
		Thread.sleep(1000);
	}

	public static String GetCurrentPageSource() {
		String PageSource = webdriver.getPageSource();
		return (PageSource);
	}

	public static boolean VerifyIsEnabled(By Element) {
		boolean IsEnable = webdriver.findElement(Element).isEnabled();
		return (IsEnable);
	}

	public static boolean VerifyIsSelected(By Element) {
		boolean IsSelected = webdriver.findElement(Element).isSelected();
		return (IsSelected);
	}

	public static boolean VerifyIsDisplayed(By Element) {
		boolean IsDisplayed = webdriver.findElement(Element).isDisplayed();
		return (IsDisplayed);
	}

	public static String getTitle() {
		String title = webdriver.getTitle();
		return (title);
	}

	public static void AlertDismiss() {
		webdriver.switchTo().alert().dismiss();
	}

	public static void AlertAccept() {
		webdriver.switchTo().alert().accept();
	}

	public static String getTextFromAlertPopup() {
		return (webdriver.switchTo().alert().getText());
	}

	public static void sendKeysToAlertpopup(String stringToSend) {
		webdriver.switchTo().alert().sendKeys(stringToSend);
	}

	public static String HandleAuthPopup(String UserName, String Password, String url) {
		return ("http://" + UserName + ":" + Password + "@" + url);
	}

	public static void sendkeysUsingJS(By locator, String value) {
		WebElement searchbox = webdriver.findElement(locator);
		JavascriptExecutor myExecutor = ((JavascriptExecutor) webdriver);
		String JsSendKey = "arguments[0].value='" + value + "';";
		myExecutor.executeScript(JsSendKey, searchbox);
	}

	public static void ClickOn_CheckBoxUsingJS(String Id) {
		JavascriptExecutor myExecutor = ((JavascriptExecutor) webdriver);
		String JsSendKey = "document.getElementById('" + Id + "').checked=true;";
		myExecutor.executeScript(JsSendKey);
	}

	public static void UnCheck_CheckBoxUsingJS(String Id) {
		JavascriptExecutor myExecutor = ((JavascriptExecutor) webdriver);
		String JsSendKey = "document.getElementById('" + Id + "').checked=false;";
		myExecutor.executeScript(JsSendKey);
	}

	public static void FocusElememtUsingJS(String Id) {
		JavascriptExecutor myExecutor = ((JavascriptExecutor) webdriver);
		String JsSendKey = "document.getElementById('" + Id + "').focus();";
		myExecutor.executeScript(JsSendKey);
	}

	public static void FocusElementUsingAction(By locator) {
		WebElement Element = webdriver.findElement(locator);
		new Actions(webdriver).moveToElement(Element).click().perform();
	}

	public static void DragAndDropUsingAction(By LocatorFrom, By LocatorTo) {
		WebElement From = webdriver.findElement(LocatorFrom);
		WebElement To = webdriver.findElement(LocatorTo);
		Actions builder = new Actions(webdriver);
		Action dragAndDrop = builder.clickAndHold(From).moveToElement(To).release(To).build();
		dragAndDrop.perform();
	}

	public static void MouseOverMainMenu_ClickSubMenu(By LocatorMainMenu, By LocatorSubMenu) {
		WebElement mainMenu = webdriver.findElement(LocatorMainMenu);
		WebElement subMenu = webdriver.findElement(LocatorSubMenu);
		Actions action = new Actions(webdriver);
		action.moveToElement(mainMenu).moveToElement(subMenu).click().build().perform();
	}

	public static void DoubleClickUsingActions(By Locator) {
		WebElement element = webdriver.findElement(Locator);
		Actions action = new Actions(webdriver).doubleClick(element);
		action.build().perform();
	}

	public static void RightClickUsingActions(By Locator) {
		WebElement element = webdriver.findElement(Locator);
		Actions action = new Actions(webdriver).contextClick(element);
		action.build().perform();
	}

	public static String GetTextFromToolTip(By Locator) {
		WebElement toolTipElement = webdriver.findElement(Locator);
		return (toolTipElement.getAttribute("title"));
	}

	public static void MoveToXYCo_ordinates(int x, int y) {
		Actions a = new Actions(webdriver);
		a.moveByOffset(x, y).build().perform();
	}

	public static void RefreshCurrentTab() {
		webdriver.navigate().refresh();
	}

	public static void DeselectFromDropDown(By Locator, int index) {
		WebElement element = webdriver.findElement(Locator);
		Select selectData = new Select(element);
		selectData.deselectByIndex(index);
	}

	public static void InitializeProxyBrowser(String browserType, String URL) throws InterruptedException {

		if (browserType.equalsIgnoreCase("CHROME")) {
			server = new BrowserMobProxyServer();
			server.start();
			Proxy proxy = ClientUtil.createSeleniumProxy(server);
			Thread.sleep(10000);
			server.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
			webdriver = createProxyDriver("chrome", proxy, CHROME_DRIVER_PATH);
			server.newHar(URL);

		}
	}

	public static WebDriver createProxyDriver(String type, Proxy proxy, String path) {
		if (type.equalsIgnoreCase("chrome"))
			return createChromeDriver(createProxyCapabilities(proxy), path);
		throw new RuntimeException("Unknown WebDriver browser: " + type);
	}

	public static DesiredCapabilities createProxyCapabilities(Proxy proxy) {
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability("proxy", proxy);
		return capabilities;
	}

	public static WebDriver createChromeDriver(DesiredCapabilities capabilities, String path) {
		System.setProperty("webdriver.chrome.driver", path);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--allow-running-insecure-content");
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		return new ChromeDriver(options);
	}

	public static void CreateHARFile(String filename) throws IOException {
		Har har = server.getHar();
		File harFile = new File(filename);
		har.writeTo(harFile);
	}

	public void CountRowsInTable(By Element) {
		// By Element = By.xpath("//div[@class='su-table']/table/tbody/tr");
		List<WebElement> Rows = webdriver.findElements(Element);
		apiParam.TabletotalRows = Rows.size();
		logger.info(" Total rows : " + apiParam.TabletotalRows);
	}

	public void CountColInTable(By Element) {
		// By Element =By.xpath("//div[@class='su-table']/table/tbody/tr[1]/td")
		List<WebElement> Columns = webdriver.findElements(By.xpath("//div[@class='su-table']/table/tbody/tr[1]/td"));
		apiParam.TabletotalColumns = Columns.size();
		logger.info(" Total Columns : " + apiParam.TabletotalColumns);
	}

	public void ExtractDataFromTable() {
		for (int i = 1; i <= apiParam.TabletotalRows; i++) {
			for (int j = 1; j <= apiParam.TabletotalColumns; j++) {
				WebElement dataCell = webdriver
						.findElement(By.xpath("//div[@class='su-table']/table/tbody/tr[" + i + "]/td[" + j + "]"));
				logger.info(" Table Data : " + dataCell.getText());
			}
		}
	}

	public static void deleteCookie()
	{
		
	}
	public static void getCookie() {
		Set<Cookie> cookie = webdriver.manage().getCookies();
		for (Cookie c : cookie) {
			logger.info(" Cookie Data : " + c);
		}
	}

	public static void addCookie() {
		Cookie name = new Cookie("This is my cookie.", "123456");
		webdriver.manage().addCookie(name);
		Set<Cookie> cookie = webdriver.manage().getCookies();
		for (Cookie c : cookie) {
			logger.info(" Cookie Data : " + c);
		}
	}

	public static void MutipleSelectInDropDown(By Element) {
		WebElement dropdown = webdriver.findElement(Element);
		Select select = new Select(dropdown);
		if (select.isMultiple()) {
			// Here we will perform the multiselect operation in the dropdown.
			select.selectByVisibleText("Chrome");
			select.selectByValue("Safari");
		}
	}

}
