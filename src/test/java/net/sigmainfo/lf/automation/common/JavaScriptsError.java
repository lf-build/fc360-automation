package net.sigmainfo.lf.automation.common;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

import net.sigmainfo.lf.automation.stepdefinition.StepDefinitionTest;

/**
 * Created by : Rengarajan.m on 06-12-2017. Test class : JavaScriptsError.java
 * Description : This class used for Capturing java scripts error
 */

public class JavaScriptsError {

	public org.slf4j.Logger logger = LoggerFactory.getLogger(JavaScriptsError.class);
	public static AbstractTests ABtests = new AbstractTests();

	public void javaScriptExec(String testcase) {

		logger.info("****Java Scripts Error related Information*****");
		JavascriptExecutor js = (JavascriptExecutor) ABtests.webdriver;

		String readyState = (String) js.executeScript("return document.readyState");
		logger.info("*****ReadyState: " + readyState + "*****");

		String title = (String) js.executeScript("return document.title");
		logger.info("*****Title: " + title + "*****");

		String domain = (String) js.executeScript("return document.domain");
		logger.info("*****Domain: " + domain + "*****");

		String lastModified = (String) js.executeScript("return document.lastModified");
		logger.info("*****LastModified: " + lastModified + "*****");

		String URL = (String) js.executeScript("return document.URL");
		logger.info("*****Full URL : " + URL + "*****");

		String error = (String) ((JavascriptExecutor) ABtests.webdriver).executeScript("return window.jsErrors");
		logger.warn("*****Javascript Error : " + error + "*****");

	}

}
