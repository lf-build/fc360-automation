package net.sigmainfo.lf.automation.api.dataset;
import org.springframework.stereotype.Component;

/**
 * Created by Rengarajan.M on 30-10-2017.
 */
@Component
public class PhoneNumbers {
	private String Phone;
	private String PhoneType;
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return this.Phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.Phone = phone;
	}
	/**
	 * @return the phoneType
	 */
	public String getPhoneType() {
		return this.PhoneType;
	}
	/**
	 * @param phoneType the phoneType to set
	 */
	public void setPhoneType(String phoneType) {
		this.PhoneType = phoneType;
	}

}
