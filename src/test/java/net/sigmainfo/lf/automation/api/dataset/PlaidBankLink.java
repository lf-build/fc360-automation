package net.sigmainfo.lf.automation.api.dataset;
import org.springframework.stereotype.Component;

/**
 * Created by Rengarajan.M on 30-10-2017.
 */
@Component
public class PlaidBankLink {
	
	private String PublicToken;
	private String BankSupportedProductType;
	private String EntityId;
	private String EntityType;
	/**
	 * @return the publicToken
	 */
	public String getPublicToken() {
		return this.PublicToken;
	}
	/**
	 * @param publicToken the publicToken to set
	 */
	public void setPublicToken(String publicToken) {
		this.PublicToken = publicToken;
	}
	/**
	 * @return the bankSupportedProductType
	 */
	public String getBankSupportedProductType() {
		return this.BankSupportedProductType;
	}
	/**
	 * @param bankSupportedProductType the bankSupportedProductType to set
	 */
	public void setBankSupportedProductType(String bankSupportedProductType) {
		this.BankSupportedProductType = bankSupportedProductType;
	}
	/**
	 * @return the entityId
	 */
	public String getEntityId() {
		return this.EntityId;
	}
	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(String entityId) {
		this.EntityId = entityId;
	}
	/**
	 * @return the entityType
	 */
	public String getEntityType() {
		return this.EntityType;
	}
	/**
	 * @param entityType the entityType to set
	 */
	public void setEntityType(String entityType) {
		this.EntityType = entityType;
	}

}
