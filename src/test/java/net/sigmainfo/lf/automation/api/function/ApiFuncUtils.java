package net.sigmainfo.lf.automation.api.function;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.internal.mapping.Jackson2Mapper;
import com.jayway.restassured.mapper.ObjectMapper;
import com.jayway.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.dataset.*;
import net.sigmainfo.lf.automation.common.AbstractTests;
import org.apache.commons.lang.RandomStringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;

import static com.jayway.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Created by : Rengarajan.m on 27-09-2017. Test class : ApiFuncUtils.java
 * Description : Contains reusable methods used while API automation Includes :
 * 1. Get request implementation 2. Post request implementation 3. Response
 * verification methods
 */
@Component
public class ApiFuncUtils extends AbstractTests {

	private static Logger logger = LoggerFactory.getLogger(ApiFuncUtils.class);

	/**
	 * THIS CLASS DEFINES ALL REUSABLE UTILITIES I.E. REUSABLE FUNCTIONS
	 */

	/**
	 * BELOW LINE IS VERY MUCH SIMILAR TO FOLLOWING STATEMENT ApiParam apiParam
	 * = new ApiParam();
	 *
	 * =======================================
	 *
	 * @Autowired ApiParam apiParam;
	 */

	ApiParam apiParam = new ApiParam();
	Addresses addresses = new Addresses();
	ApplicationSubmit applicationSubmit = new ApplicationSubmit();
	EmailAddresses emailAddresses = new EmailAddresses();
	Owners owners = new Owners();
	PhoneNumbers phoneNumbers = new PhoneNumbers();
	PlaidBankLink plaid = new PlaidBankLink();

	public Response triggerPostRequest(String serviceUrl, Boolean isCorrectAuthorization, String authToken) {

		logger.info("=======================================================================================");
		logger.info("Triggering a POST request to :" + serviceUrl);
		logger.info("=======================================================================================");
		Header header;
		if (!isCorrectAuthorization) {
			header = new Header("Authorization", "Bearer 12" + authToken + "12");
		} else {
			header = new Header("Authorization", "Bearer " + authToken);
		}
		RequestSpecification requestSpec = constructRequestPayload(apiParam.requestType, apiParam.contentType,
				serviceUrl);

		Response response = given().header(header).contentType(apiParam.contentType).spec(requestSpec).log().all()
				.when().post(serviceUrl);

		logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
		return response;
	}
	
	public Response triggerPostRequest(String serviceUrl, Boolean isCorrectAuthorization, String authToken,
			String Body) {

		logger.info("=======================================================================================");
		logger.info("Triggering a Post request to :" + serviceUrl);
		logger.info("=======================================================================================");
		Header header;
		if (!isCorrectAuthorization) {
			header = new Header("Authorization", "Bearer 12" + authToken + "12");
		} else {
			header = new Header("Authorization", "Bearer " + authToken);
		}

		logger.info("============== Auth Token : " +authToken + "=============================");
		logger.info("============== Request payload : " +Body + "=============================");
		Response response = given().header(header).contentType(apiParam.contentType).body(Body).when().post(serviceUrl);

		logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
		return response;
	}

	public Response triggerPutRequest(String serviceUrl, Boolean isCorrectAuthorization, String authToken,
			String Body) {

		logger.info("=======================================================================================");
		logger.info("Triggering a PUT request to :" + serviceUrl);
		logger.info("=======================================================================================");
		Header header;
		if (!isCorrectAuthorization) {
			header = new Header("Authorization", "Bearer 12" + authToken + "12");
		} else {
			header = new Header("Authorization", "Bearer " + authToken);
		}

		
		Response response = given().header(header).contentType(apiParam.contentType).body(Body).when().put(serviceUrl);

		logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
		return response;
	}

	private RequestSpecification constructRequestPayload(String requestType, String contentType, String serviceUrl) {

		RequestSpecBuilder builder = new RequestSpecBuilder();
		ObjectMapper objectMapper = null;

		if (contentType.contains("application/json")) {
			objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

				public com.fasterxml.jackson.databind.ObjectMapper create(Class arg0, String arg1) {
					com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
					objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
					return objectMapper;
				}

			});

		}
		builder.setContentType(contentType);

		// RequestSpecification requestSpec =
		// constructRequestPayload(requestType,contentType,serviceUrl);

		if (requestType.equalsIgnoreCase("/application/submit/business")) {

			logger.info("Constructing request body for " + apiParam.requestType + " in " + apiParam.contentType
					+ " format.");
			
			applicationSubmit.setAmountSeeking(apiParam.ASAmountSeeking);
			applicationSubmit.setBusinessName(apiParam.ASBusinessName);
			applicationSubmit.setUseOfFunds("1");
			applicationSubmit.setAnnualBusinessRevenue("1000000");
			applicationSubmit.setFirstName(apiParam.ASFirstName);
			applicationSubmit.setLastName(apiParam.ASLastName);
			applicationSubmit.setPhone(apiParam.ASPhone);
			applicationSubmit.setEmail(apiParam.ASEmail);
			applicationSubmit.setPassword("Sigma@1234");
			applicationSubmit.setDBA(apiParam.ASBusinessName);
			applicationSubmit.setBusinessAddress("869 Stockton St");
			applicationSubmit.setCity("Irvine");
			applicationSubmit.setState(apiParam.ASBusinessState);
			applicationSubmit.setZipCode("12345");
			applicationSubmit.setBusinessPhone(apiParam.ASPhone);
			applicationSubmit.setBusinessStartDate(apiParam.ASBSD);
			applicationSubmit.setBusinessWebsite("www.fc360.co.in");
			applicationSubmit.setLegalEntity("1");
			applicationSubmit.setBusinessLocation(apiParam.ASBusinessLocation);
			applicationSubmit.setFederalSalesTaxID(apiParam.ASTaxId);
			applicationSubmit.setIndustry("1");
			applicationSubmit.setClientIpAddress("100.100.100.100");
			applicationSubmit.setLoanPriority("1");
			
			ArrayList<Addresses> Addresses = new ArrayList<Addresses>();
			addresses.setAddressLine1("869 Stockton St");
			addresses.setCity("Irvine");
			addresses.setState("AL");
			addresses.setZipCode("12345");
			Addresses.add(addresses);
			
			ArrayList<PhoneNumbers> PhoneNumbers = new ArrayList<PhoneNumbers>();
			phoneNumbers.setPhone(apiParam.ASPhone);
			phoneNumbers.setPhoneType("1");
			PhoneNumbers.add(phoneNumbers);
			
			ArrayList<EmailAddresses> EmailAddresses = new ArrayList<EmailAddresses>();
			emailAddresses.setEmail(apiParam.ASEmail);
			EmailAddresses.add(emailAddresses);
			
			ArrayList<Owners> Owners = new ArrayList<Owners>();
			owners.setDOB(apiParam.ASODOB);
			owners.setFirstName(apiParam.ASFirstName);
			owners.setLastName(apiParam.ASLastName);
			owners.setSSN(apiParam.ASSSN);
			owners.setOwnershipPercentage(apiParam.ASOwnerShip);
			owners.setAddresses(Addresses);
			owners.setEmailAddresses(EmailAddresses);
			owners.setPhoneNumbers(PhoneNumbers);
			Owners.add(owners);
			
			applicationSubmit.setOwners(Owners);
			
			
			setBody(builder, objectMapper, applicationSubmit, apiParam.contentType);
		}
		
		if (requestType.equalsIgnoreCase("/plaid?token=SzqwCSPHEzE5HzY1ftH8CjTMBnlKoHojCNx9YcDiFzW290KfTctHVg==")) {
			
			plaid.setBankSupportedProductType("connect");
			plaid.setEntityId(apiParam.LoanApplicationNumber);
			plaid.setEntityType("application");
			plaid.setPublicToken(apiParam.PlaidBankName);
			
			
			setBody(builder, objectMapper, plaid, apiParam.contentType);
		}

		return builder.build();
	}

	private void setContentType(RequestSpecBuilder builder, ObjectMapper objectMapper, String contentType) {

		if (contentType.contains("application/json")) {
			objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

				public com.fasterxml.jackson.databind.ObjectMapper create(Class arg0, String arg1) {
					com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
					objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
					// objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
					return (objectMapper);
				}

			});
		}
		builder.setContentType(contentType);
	}

	private static void setBody(RequestSpecBuilder builder, ObjectMapper objectMapper, Object body, String conType) {

		if (conType.contains("json")) {
			builder.setBody(body, objectMapper);
		} else if (conType.contains("xml")) {
			builder.setBody(body);
		}
	}

}
