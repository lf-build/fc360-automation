package net.sigmainfo.lf.automation.api.tests;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.listener.ExtentProperties;
import net.sigmainfo.lf.automation.listener.Reporter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(strict = true, monochrome = true, features = "features", glue = "net.sigmainfo.lf.automation.stepdefinition", plugin = {
		"net.sigmainfo.lf.automation.listener.ExtentCucumberFormatter:output/LF-FC360-automation.html",
		"rerun:output/FailedScenario.txt" }, tags = { "@ApplicationSubmitThroughAPI" })

/**
 * Created by : Rengarajan.m on 27-09-2017. Test class : RunCukesTest.java
 * Description : This class holds cucumber Options
 */
public class APICukesTest extends AbstractTestNGCucumberTests {

	public static AbstractTests ABtests = new AbstractTests();
	public static String env = null;
    
	@BeforeClass
	@Parameters({ "Environment" })
	public void setupalways(String environment) throws Exception {
		env = environment;
		ABtests.postConstruct(environment);
	}

	@AfterClass
	public void teardown() throws Exception {
		Reporter.loadXMLConfig(new File("extent-config.xml"));
		Reporter.setSystemInfo("Tester name", System.getProperty("user.name"));
		Reporter.setSystemInfo("Tester Role", "admin");
		Reporter.setSystemInfo("os", "window 8");
		Reporter.setSystemInfo("Server", env);
		Reporter.setSystemInfo("Owner", "rengarajan.m");
	}
}