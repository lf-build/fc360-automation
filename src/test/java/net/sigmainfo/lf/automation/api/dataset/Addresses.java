package net.sigmainfo.lf.automation.api.dataset;
import org.springframework.stereotype.Component;

/**
 * Created by Rengarajan.M on 30-10-2017.
 */
@Component
public class Addresses {
	private String AddressLine1;
	private String City;
	private String ZipCode;
	private String State;
	 
	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return this.AddressLine1;
	}
	/**
	 * @param addressLine1 the addressLine1 to set
	 */
	public void setAddressLine1(String addressLine1) {
		this.AddressLine1 = addressLine1;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return this.City;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.City = city;
	}
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return this.ZipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.ZipCode = zipCode;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return this.State;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.State = state;
	}

	
	


}
