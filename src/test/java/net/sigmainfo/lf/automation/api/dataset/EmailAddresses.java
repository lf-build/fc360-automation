package net.sigmainfo.lf.automation.api.dataset;
import org.springframework.stereotype.Component;

/**
 * Created by Rengarajan.M on 30-10-2017.
 */
@Component
public class EmailAddresses {
	private String Email;

	/**
	 * @return the email
	 */
	public String getEmail() {
		return this.Email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.Email = email;
	}
	

}
