package net.sigmainfo.lf.automation.api.constant;

import org.springframework.stereotype.Component;

/**
 * Created by           : Rengarajan.m on 27-09-2017.
 * Test class           : ApiParam.java
 * Description          : Contains all placeholders declared within the test package
 * Includes             : 1. Declares the place holders being used
 *                        2. Contains getter and setter methods
 */

/**
 * ALL THE VARIABLES TO BE DECLARED IN THIS CLASS
 */
@Component
public class ApiParam {

	public static String getUserType() {
		return userType;
	}

	public static void setUserType(String userType) {
		ApiParam.userType = userType;
	}

	public static String userType;

	public static String BPBrandingLogo;
	public static String baseresturl;
	public static String AuthToken;
	public static String requestType;
	public static String contentType;
	public static String line1;
	public static String portNo;
	public static String BackOfficeURL;
	public static String InboxURL;
	public static String BorrowerPortal;
	public static String BackOfficeImageURL;
	public static String environment;
	public static String port;
	public static String BrandingLogoSearchPage;
	public static String BrandingLogoLoginPage;
	public static String UserName;
	public static String Password;
	public static String ConsentCheckBox;
	public static String SignButton;
	public static String ProfileLink;
	public static String LogOutLink;
	public static String ErrorMessageonHomePage;
	public static String ConsentMessageonHomePage;
	public static String Forgotpasswordlink;
	public static String MessageonResetPasswordPage;
	public static String SendRestPasswordButton;
	public static String SuccessMessage;
	public static String EmailtextField;
	public static String Go;
	public static String RestPasswordSubject;
	public static String RestPasswordlink;
	public static String TimePeriod;
	public static String TextOnRestPasswordPage;
	public static String NewPasswordField;
	public static String ConfirmPasswordField;
	public static String RestButton;
	public static String SuccessMessageAfterRest;
	public static String ReLogin;
	public static String ErrorMessageAfterRest;
	public static String verifyPasswordErrorMessage;
	public static String FirstName;
	public static String LastName;
	public static String BusinessEmail;
	public static String BusinessName;
	public static String Status;
	public static String ApplicationDate;
	public static String ExpiryDate;
	public static String MobileNumber;
	public static String SearchButton;
	public static String ApplicationNumber;
	public static String ProductID;
	public static String SearchResult;
	public static String DBHostName;
	public static String DBPort;
	public static String DBUserName;
	public static String DBPassword;
	public static String AuthenticationDB;
	public static String MessageOnSearchPage;
	public static String BPURL;
	public static String BPImageURL;
	public static String BPSignButton;
	public static String SignInPageEmailAddress;
	public static String SignInPagePassword;
	public static String SignInPageLoginButton;
	public static String SignInPageForgotPasswordLink;
	public static String ForgotPasswordPageEmailAddress;
	public static String FotgotPasswordSubmitButton;
	public static String ProfilePage;
	public static String ToDoPage;
	public static String OverViewPage;
	public static String ChangePasswordPage;
	public static String BorrowerName;
	public static String ChangePasswordLink;
	public static String BPLogout;
	public static String SuccessMessageOnForgotPasswordPage;
	public static String SuccessMessageOnChangePasswordPage;
	public static String BPBusinessName;
	public static String BPAmountSeeking;
	public static String BPUseOfFunds;
	public static String BPAnnualBusinessRevenue;
	public static String BPFirstName;
	public static String BPLastName;
	public static String BPPhone;
	public static String BPEmail;
	public static String BPPaasword;
	public static String BPNextButtonInBasicTab;
	public static String Workflow;
	public static String IframeID;
	public static String Document;
	public static int Fein;
	public static String ScreenShotPath;
	public static String ASPort;
	public static String ASAmountSeeking;
	public static String ASBusinessName;
	public static String ASFirstName;
	public static String ASLastName;
	public static String ASPhone;
	public static String ASTaxId;
	public static String ASEmail;
	public static String ASBSD;
	public static String ASODOB;
	public static String ASSSN;
	public static String ASOwnerShip;
	public static String BusinessTabDBA;
	public static String BusinessTabBusinessAddress;
	public static String BusinessTabCity;
	public static String BusinessTabState;
	public static String BusinessTabZipcode;
	public static String BusinessTabBusinessPhone;
	public static String BusinessTabMonth;
	public static String BusinessTabDay;
	public static String BusinessTabYear;
	public static String BusinessTabBusinessWebsite;
	public static String BusinessTabLegalEntity;
	public static String BusinessTabBusinessLoacation;
	public static String BusinessTabTaxId;
	public static String BusinessTabIndustry;
	public static String BusinessTabImportant;
	public static String BusinessTabNext;
	public static String SignInErrorMessage;
	public static String ForgotPasswordTitle;
	public static String MessageTitleOnCreatePasswordPage;
	public static String BPCreateNewPassword;
	public static String BPCreateConfirmPassword;
	public static String BPCreatePasswordButton;
	public static String MessageAfterCraetePasswordRequest;
	public static String SuccessMessageAftersuccessCreatePasswordRequest;
	public static String OwnersEmail;
	public static String OwnersFirstName;
	public static String OwnersLastName;
	public static String OwnersAddress;
	public static String OwnersCity;
	public static String OwnersState;
	public static String OwnersZipcode;
	public static String OwnersPhoneNo;
	public static String Ownersmobileno;
	public static String OwnersOwnership;
	public static String OwnersSSN;
	public static String OwnersMonth;
	public static String OwnersDay;
	public static String OwnersYear;
	public static String OwnersNext;
	public static String FinalFinish;
	public static String ValidationMessageOnTodo;
	public static String FinancialTabProgressBar;
	public static String OwnersTabProgressBar;
	public static String BusinessTabProgressBar;
	public static String BPProgressBar;
	public static String BPDuplicateCheckMessage;
	public static String BusinessStartDateErrorMessage;
	public static String OwnerShipErrorMessage;
	public static String ErrormessageAge;
	public static String ChangePasswordPageRestButton;
	public static String TodoVlidationMessage;
	public static String TodoDeclineMessage;
	public static String TodoApplicationNo;
	public static String ScenarioName;
	public static String BPConfirmPassword;
	public static String DocusignAcceptCheckBox;
	public static String DocusignContinue;
	public static String DocusignStart;
	public static String DocusignSign;
	public static String DocusignAdoptSign;
	public static String DocusignFinish;
	public static String DocuSignLabel;
	public static String LinkBankAccount;
	public static String IFrameIDForPlaid;
	public static String PlaidSearch;
	public static String SelectionOfPlaidBank;
	public static String PlaidUserName;
	public static String PlaidPassword;
	public static String SubmitPlaidCredential;
	public static String PlaidReCaptcha;
	public static String SendType;
	public static String ContinueButtonAfterSendType;
	public static String PlaidCode;
	public static String SubmitPlaidBank;
	public static String ContinueButtonAfterAccountLinked;
	public static String SuccessMessageAfterBankLinked;
	public static String LoanApplicationNumber;
	public static String PlaidBankName;
	public static String PlaidBankAuthToken;
	public static String PlaidBankPort;
	public static int linkcount;
	public static String currentLink;
	public static String currentFeatureFile;
	public static String currentScenarioName;
	public static String ASBusinessState;
	public static String ASBusinessLocation;
	public static String RecorderFilePath;
	public static long RecorcderFileName;
	public static String BankSuccessMsgUI;
	public static String TodoBankSuccessMsg;
	public static int TabletotalRows;
	public static int TabletotalColumns;
	public static String ClosePlaid;
	public static String FinicityBankButton;
	public static String FinicityConsent;
	public static String AddNewAccountButton;
	public static String FinicitySearch;
	public static String SelectFinBank;
	public static String BankingUsername;
	public static String BankingPassword;
	public static String SubmitFinBankCredential;
	public static String AddSelectedAccounts;
	public static String MakeDoneForAddingAccounts;
	public static String FinicityIframe;

	/**
	 * @return the finicityIframe
	 */
	public static String getFinicityIframe() {
		return FinicityIframe;
	}

	/**
	 * @param finicityIframe the finicityIframe to set
	 */
	public static void setFinicityIframe(String finicityIframe) {
		FinicityIframe = finicityIframe;
	}

	/**
	 * @return the closePlaid
	 */
	public static String getClosePlaid() {
		return ClosePlaid;
	}

	/**
	 * @param closePlaid the closePlaid to set
	 */
	public static void setClosePlaid(String closePlaid) {
		ClosePlaid = closePlaid;
	}

	/**
	 * @return the finicityBankButton
	 */
	public static String getFinicityBankButton() {
		return FinicityBankButton;
	}

	/**
	 * @param finicityBankButton the finicityBankButton to set
	 */
	public static void setFinicityBankButton(String finicityBankButton) {
		FinicityBankButton = finicityBankButton;
	}

	/**
	 * @return the finicityConsent
	 */
	public static String getFinicityConsent() {
		return FinicityConsent;
	}

	/**
	 * @param finicityConsent the finicityConsent to set
	 */
	public static void setFinicityConsent(String finicityConsent) {
		FinicityConsent = finicityConsent;
	}

	/**
	 * @return the addNewAccountButton
	 */
	public static String getAddNewAccountButton() {
		return AddNewAccountButton;
	}

	/**
	 * @param addNewAccountButton the addNewAccountButton to set
	 */
	public static void setAddNewAccountButton(String addNewAccountButton) {
		AddNewAccountButton = addNewAccountButton;
	}

	/**
	 * @return the finicitySearch
	 */
	public static String getFinicitySearch() {
		return FinicitySearch;
	}

	/**
	 * @param finicitySearch the finicitySearch to set
	 */
	public static void setFinicitySearch(String finicitySearch) {
		FinicitySearch = finicitySearch;
	}

	/**
	 * @return the selectFinBank
	 */
	public static String getSelectFinBank() {
		return SelectFinBank;
	}

	/**
	 * @param selectFinBank the selectFinBank to set
	 */
	public static void setSelectFinBank(String selectFinBank) {
		SelectFinBank = selectFinBank;
	}

	/**
	 * @return the bankingUsername
	 */
	public static String getBankingUsername() {
		return BankingUsername;
	}

	/**
	 * @param bankingUsername the bankingUsername to set
	 */
	public static void setBankingUsername(String bankingUsername) {
		BankingUsername = bankingUsername;
	}

	/**
	 * @return the bankingPassword
	 */
	public static String getBankingPassword() {
		return BankingPassword;
	}

	/**
	 * @param bankingPassword the bankingPassword to set
	 */
	public static void setBankingPassword(String bankingPassword) {
		BankingPassword = bankingPassword;
	}

	/**
	 * @return the submitFinBankCredential
	 */
	public static String getSubmitFinBankCredential() {
		return SubmitFinBankCredential;
	}

	/**
	 * @param submitFinBankCredential the submitFinBankCredential to set
	 */
	public static void setSubmitFinBankCredential(String submitFinBankCredential) {
		SubmitFinBankCredential = submitFinBankCredential;
	}

	/**
	 * @return the addSelectedAccounts
	 */
	public static String getAddSelectedAccounts() {
		return AddSelectedAccounts;
	}

	/**
	 * @param addSelectedAccounts the addSelectedAccounts to set
	 */
	public static void setAddSelectedAccounts(String addSelectedAccounts) {
		AddSelectedAccounts = addSelectedAccounts;
	}

	/**
	 * @return the makeDoneForAddingAccounts
	 */
	public static String getMakeDoneForAddingAccounts() {
		return MakeDoneForAddingAccounts;
	}

	/**
	 * @param makeDoneForAddingAccounts the makeDoneForAddingAccounts to set
	 */
	public static void setMakeDoneForAddingAccounts(String makeDoneForAddingAccounts) {
		MakeDoneForAddingAccounts = makeDoneForAddingAccounts;
	}

	/**
	 * @return the tabletotalRows
	 */
	public static int getTabletotalRows() {
		return TabletotalRows;
	}

	/**
	 * @param tabletotalRows the tabletotalRows to set
	 */
	public static void setTabletotalRows(int tabletotalRows) {
		TabletotalRows = tabletotalRows;
	}

	/**
	 * @return the tabletotalColumns
	 */
	public static int getTabletotalColumns() {
		return TabletotalColumns;
	}

	/**
	 * @param tabletotalColumns the tabletotalColumns to set
	 */
	public static void setTabletotalColumns(int tabletotalColumns) {
		TabletotalColumns = tabletotalColumns;
	}

	/**
	 * @return the todoBankSuccessMsg
	 */
	public static String getTodoBankSuccessMsg() {
		return TodoBankSuccessMsg;
	}

	/**
	 * @param todoBankSuccessMsg the todoBankSuccessMsg to set
	 */
	public static void setTodoBankSuccessMsg(String todoBankSuccessMsg) {
		TodoBankSuccessMsg = todoBankSuccessMsg;
	}

	/**
	 * @return the bankSuccessMsgUI
	 */
	public static String getBankSuccessMsgUI() {
		return BankSuccessMsgUI;
	}

	/**
	 * @param bankSuccessMsgUI the bankSuccessMsgUI to set
	 */
	public static void setBankSuccessMsgUI(String bankSuccessMsgUI) {
		BankSuccessMsgUI = bankSuccessMsgUI;
	}

	/**
	 * @return the recorcderFileName
	 */
	public static long getRecorcderFileName() {
		return RecorcderFileName;
	}

	/**
	 * @param recorcderFileName the recorcderFileName to set
	 */
	public static void setRecorcderFileName(long recorcderFileName) {
		RecorcderFileName = recorcderFileName;
	}

	/**
	 * @return the recorderFilePath
	 */
	public static String getRecorderFilePath() {
		return RecorderFilePath;
	}

	/**
	 * @param recorderFilePath the recorderFilePath to set
	 */
	public static void setRecorderFilePath(String recorderFilePath) {
		RecorderFilePath = recorderFilePath;
	}

	/**
	 * @return the aSBusinessState
	 */
	public static String getASBusinessState() {
		return ASBusinessState;
	}

	/**
	 * @param aSBusinessState the aSBusinessState to set
	 */
	public static void setASBusinessState(String aSBusinessState) {
		ASBusinessState = aSBusinessState;
	}

	/**
	 * @return the aSBusinessLocation
	 */
	public static String getASBusinessLocation() {
		return ASBusinessLocation;
	}

	/**
	 * @param aSBusinessLocation the aSBusinessLocation to set
	 */
	public static void setASBusinessLocation(String aSBusinessLocation) {
		ASBusinessLocation = aSBusinessLocation;
	}

	/**
	 * @return the currentFeatureFile
	 */
	public static String getCurrentFeatureFile() {
		return currentFeatureFile;
	}

	/**
	 * @param currentFeatureFile the currentFeatureFile to set
	 */
	public static void setCurrentFeatureFile(String currentFeatureFile) {
		ApiParam.currentFeatureFile = currentFeatureFile;
	}

	/**
	 * @return the currentScenarioName
	 */
	public static String getCurrentScenarioName() {
		return currentScenarioName;
	}

	/**
	 * @param currentScenarioName the currentScenarioName to set
	 */
	public static void setCurrentScenarioName(String currentScenarioName) {
		ApiParam.currentScenarioName = currentScenarioName;
	}

	/**
	 * @return the currentLink
	 */
	public static String getCurrentLink() {
		return currentLink;
	}

	/**
	 * @param currentLink the currentLink to set
	 */
	public static void setCurrentLink(String currentLink) {
		ApiParam.currentLink = currentLink;
	}

	/**
	 * @return the linkcount
	 */
	public static int getLinkcount() {
		return linkcount;
	}

	/**
	 * @param linkcount the linkcount to set
	 */
	public static void setLinkcount(int linkcount) {
		ApiParam.linkcount = linkcount;
	}

	/**
	 * @return the plaidBankAuthToken
	 */
	public static String getPlaidBankAuthToken() {
		return PlaidBankAuthToken;
	}

	/**
	 * @param plaidBankAuthToken the plaidBankAuthToken to set
	 */
	public static void setPlaidBankAuthToken(String plaidBankAuthToken) {
		PlaidBankAuthToken = plaidBankAuthToken;
	}

	/**
	 * @return the plaidBankPort
	 */
	public static String getPlaidBankPort() {
		return PlaidBankPort;
	}

	/**
	 * @param plaidBankPort the plaidBankPort to set
	 */
	public static void setPlaidBankPort(String plaidBankPort) {
		PlaidBankPort = plaidBankPort;
	}

	/**
	 * @return the loanApplicationNumber
	 */
	public static String getLoanApplicationNumber() {
		return LoanApplicationNumber;
	}

	/**
	 * @param loanApplicationNumber the loanApplicationNumber to set
	 */
	public static void setLoanApplicationNumber(String loanApplicationNumber) {
		LoanApplicationNumber = loanApplicationNumber;
	}

	/**
	 * @return the plaidBankName
	 */
	public static String getPlaidBankName() {
		return PlaidBankName;
	}

	/**
	 * @param plaidBankName the plaidBankName to set
	 */
	public static void setPlaidBankName(String plaidBankName) {
		PlaidBankName = plaidBankName;
	}

	/**
	 * @return the linkBankAccount
	 */
	public static String getLinkBankAccount() {
		return LinkBankAccount;
	}

	/**
	 * @param linkBankAccount the linkBankAccount to set
	 */
	public static void setLinkBankAccount(String linkBankAccount) {
		LinkBankAccount = linkBankAccount;
	}

	/**
	 * @return the iFrameIDForPlaid
	 */
	public static String getIFrameIDForPlaid() {
		return IFrameIDForPlaid;
	}

	/**
	 * @param iFrameIDForPlaid the iFrameIDForPlaid to set
	 */
	public static void setIFrameIDForPlaid(String iFrameIDForPlaid) {
		IFrameIDForPlaid = iFrameIDForPlaid;
	}

	/**
	 * @return the plaidSearch
	 */
	public static String getPlaidSearch() {
		return PlaidSearch;
	}

	/**
	 * @param plaidSearch the plaidSearch to set
	 */
	public static void setPlaidSearch(String plaidSearch) {
		PlaidSearch = plaidSearch;
	}

	/**
	 * @return the selectionOfPlaidBank
	 */
	public static String getSelectionOfPlaidBank() {
		return SelectionOfPlaidBank;
	}

	/**
	 * @param selectionOfPlaidBank the selectionOfPlaidBank to set
	 */
	public static void setSelectionOfPlaidBank(String selectionOfPlaidBank) {
		SelectionOfPlaidBank = selectionOfPlaidBank;
	}

	/**
	 * @return the plaidUserName
	 */
	public static String getPlaidUserName() {
		return PlaidUserName;
	}

	/**
	 * @param plaidUserName the plaidUserName to set
	 */
	public static void setPlaidUserName(String plaidUserName) {
		PlaidUserName = plaidUserName;
	}

	/**
	 * @return the plaidPassword
	 */
	public static String getPlaidPassword() {
		return PlaidPassword;
	}

	/**
	 * @param plaidPassword the plaidPassword to set
	 */
	public static void setPlaidPassword(String plaidPassword) {
		PlaidPassword = plaidPassword;
	}

	/**
	 * @return the submitPlaidCredential
	 */
	public static String getSubmitPlaidCredential() {
		return SubmitPlaidCredential;
	}

	/**
	 * @param submitPlaidCredential the submitPlaidCredential to set
	 */
	public static void setSubmitPlaidCredential(String submitPlaidCredential) {
		SubmitPlaidCredential = submitPlaidCredential;
	}

	/**
	 * @return the plaidReCaptcha
	 */
	public static String getPlaidReCaptcha() {
		return PlaidReCaptcha;
	}

	/**
	 * @param plaidReCaptcha the plaidReCaptcha to set
	 */
	public static void setPlaidReCaptcha(String plaidReCaptcha) {
		PlaidReCaptcha = plaidReCaptcha;
	}

	/**
	 * @return the sendType
	 */
	public static String getSendType() {
		return SendType;
	}

	/**
	 * @param sendType the sendType to set
	 */
	public static void setSendType(String sendType) {
		SendType = sendType;
	}

	/**
	 * @return the continueButtonAfterSendType
	 */
	public static String getContinueButtonAfterSendType() {
		return ContinueButtonAfterSendType;
	}

	/**
	 * @param continueButtonAfterSendType the continueButtonAfterSendType to set
	 */
	public static void setContinueButtonAfterSendType(String continueButtonAfterSendType) {
		ContinueButtonAfterSendType = continueButtonAfterSendType;
	}

	/**
	 * @return the plaidCode
	 */
	public static String getPlaidCode() {
		return PlaidCode;
	}

	/**
	 * @param plaidCode the plaidCode to set
	 */
	public static void setPlaidCode(String plaidCode) {
		PlaidCode = plaidCode;
	}

	/**
	 * @return the submitPlaidBank
	 */
	public static String getSubmitPlaidBank() {
		return SubmitPlaidBank;
	}

	/**
	 * @param submitPlaidBank the submitPlaidBank to set
	 */
	public static void setSubmitPlaidBank(String submitPlaidBank) {
		SubmitPlaidBank = submitPlaidBank;
	}

	/**
	 * @return the continueButtonAfterAccountLinked
	 */
	public static String getContinueButtonAfterAccountLinked() {
		return ContinueButtonAfterAccountLinked;
	}

	/**
	 * @param continueButtonAfterAccountLinked the continueButtonAfterAccountLinked to set
	 */
	public static void setContinueButtonAfterAccountLinked(String continueButtonAfterAccountLinked) {
		ContinueButtonAfterAccountLinked = continueButtonAfterAccountLinked;
	}

	/**
	 * @return the successMessageAfterBankLinked
	 */
	public static String getSuccessMessageAfterBankLinked() {
		return SuccessMessageAfterBankLinked;
	}

	/**
	 * @param successMessageAfterBankLinked the successMessageAfterBankLinked to set
	 */
	public static void setSuccessMessageAfterBankLinked(String successMessageAfterBankLinked) {
		SuccessMessageAfterBankLinked = successMessageAfterBankLinked;
	}

	/**
	 * @return the docuSignLabel
	 */
	public static String getDocuSignLabel() {
		return DocuSignLabel;
	}

	/**
	 * @param docuSignLabel the docuSignLabel to set
	 */
	public static void setDocuSignLabel(String docuSignLabel) {
		DocuSignLabel = docuSignLabel;
	}

	/**
	 * @return the docusignAcceptCheckBox
	 */
	public static String getDocusignAcceptCheckBox() {
		return DocusignAcceptCheckBox;
	}

	/**
	 * @param docusignAcceptCheckBox the docusignAcceptCheckBox to set
	 */
	public static void setDocusignAcceptCheckBox(String docusignAcceptCheckBox) {
		DocusignAcceptCheckBox = docusignAcceptCheckBox;
	}

	/**
	 * @return the docusignContinue
	 */
	public static String getDocusignContinue() {
		return DocusignContinue;
	}

	/**
	 * @param docusignContinue the docusignContinue to set
	 */
	public static void setDocusignContinue(String docusignContinue) {
		DocusignContinue = docusignContinue;
	}

	/**
	 * @return the docusignStart
	 */
	public static String getDocusignStart() {
		return DocusignStart;
	}

	/**
	 * @param docusignStart the docusignStart to set
	 */
	public static void setDocusignStart(String docusignStart) {
		DocusignStart = docusignStart;
	}

	/**
	 * @return the docusignSign
	 */
	public static String getDocusignSign() {
		return DocusignSign;
	}

	/**
	 * @param docusignSign the docusignSign to set
	 */
	public static void setDocusignSign(String docusignSign) {
		DocusignSign = docusignSign;
	}

	/**
	 * @return the docusignAdoptSign
	 */
	public static String getDocusignAdoptSign() {
		return DocusignAdoptSign;
	}

	/**
	 * @param docusignAdoptSign the docusignAdoptSign to set
	 */
	public static void setDocusignAdoptSign(String docusignAdoptSign) {
		DocusignAdoptSign = docusignAdoptSign;
	}

	/**
	 * @return the docusignFinish
	 */
	public static String getDocusignFinish() {
		return DocusignFinish;
	}

	/**
	 * @param docusignFinish the docusignFinish to set
	 */
	public static void setDocusignFinish(String docusignFinish) {
		DocusignFinish = docusignFinish;
	}

	/**
	 * @return the bPConfirmPassword
	 */
	public static String getBPConfirmPassword() {
		return BPConfirmPassword;
	}

	/**
	 * @param bPConfirmPassword the bPConfirmPassword to set
	 */
	public static void setBPConfirmPassword(String bPConfirmPassword) {
		BPConfirmPassword = bPConfirmPassword;
	}

	/**
	 * @return the scenarioName
	 */
	public static String getScenarioName() {
		return ScenarioName;
	}

	/**
	 * @param scenarioName the scenarioName to set
	 */
	public static void setScenarioName(String scenarioName) {
		ScenarioName = scenarioName;
	}

	/**
	 * @return the todoDeclineMessage
	 */
	public static String getTodoDeclineMessage() {
		return TodoDeclineMessage;
	}

	/**
	 * @param todoDeclineMessage the todoDeclineMessage to set
	 */
	public static void setTodoDeclineMessage(String todoDeclineMessage) {
		TodoDeclineMessage = todoDeclineMessage;
	}

	/**
	 * @return the todoApplicationNo
	 */
	public static String getTodoApplicationNo() {
		return TodoApplicationNo;
	}

	/**
	 * @param todoApplicationNo the todoApplicationNo to set
	 */
	public static void setTodoApplicationNo(String todoApplicationNo) {
		TodoApplicationNo = todoApplicationNo;
	}

	/**
	 * @return the todoVlidationMessage
	 */
	public static String getTodoVlidationMessage() {
		return TodoVlidationMessage;
	}

	/**
	 * @param todoVlidationMessage the todoVlidationMessage to set
	 */
	public static void setTodoVlidationMessage(String todoVlidationMessage) {
		TodoVlidationMessage = todoVlidationMessage;
	}

	/**
	 * @return the changePasswordPageRestButton
	 */
	public static String getChangePasswordPageRestButton() {
		return ChangePasswordPageRestButton;
	}

	/**
	 * @param changePasswordPageRestButton the changePasswordPageRestButton to set
	 */
	public static void setChangePasswordPageRestButton(String changePasswordPageRestButton) {
		ChangePasswordPageRestButton = changePasswordPageRestButton;
	}

	/**
	 * @return the businessStartDateErrorMessage
	 */
	public static String getBusinessStartDateErrorMessage() {
		return BusinessStartDateErrorMessage;
	}

	/**
	 * @param businessStartDateErrorMessage the businessStartDateErrorMessage to set
	 */
	public static void setBusinessStartDateErrorMessage(String businessStartDateErrorMessage) {
		BusinessStartDateErrorMessage = businessStartDateErrorMessage;
	}

	/**
	 * @return the ownerShipErrorMessage
	 */
	public static String getOwnerShipErrorMessage() {
		return OwnerShipErrorMessage;
	}

	/**
	 * @param ownerShipErrorMessage the ownerShipErrorMessage to set
	 */
	public static void setOwnerShipErrorMessage(String ownerShipErrorMessage) {
		OwnerShipErrorMessage = ownerShipErrorMessage;
	}

	/**
	 * @return the errormessageAge
	 */
	public static String getErrormessageAge() {
		return ErrormessageAge;
	}

	/**
	 * @param errormessageAge the errormessageAge to set
	 */
	public static void setErrormessageAge(String errormessageAge) {
		ErrormessageAge = errormessageAge;
	}

	/**
	 * @return the bPDuplicateCheckMessage
	 */
	public static String getBPDuplicateCheckMessage() {
		return BPDuplicateCheckMessage;
	}

	/**
	 * @param bPDuplicateCheckMessage the bPDuplicateCheckMessage to set
	 */
	public static void setBPDuplicateCheckMessage(String bPDuplicateCheckMessage) {
		BPDuplicateCheckMessage = bPDuplicateCheckMessage;
	}

	/**
	 * @return the validationMessageOnTodo
	 */
	public static String getValidationMessageOnTodo() {
		return ValidationMessageOnTodo;
	}

	/**
	 * @param validationMessageOnTodo the validationMessageOnTodo to set
	 */
	public static void setValidationMessageOnTodo(String validationMessageOnTodo) {
		ValidationMessageOnTodo = validationMessageOnTodo;
	}

	/**
	 * @return the financialTabProgressBar
	 */
	public static String getFinancialTabProgressBar() {
		return FinancialTabProgressBar;
	}

	/**
	 * @param financialTabProgressBar the financialTabProgressBar to set
	 */
	public static void setFinancialTabProgressBar(String financialTabProgressBar) {
		FinancialTabProgressBar = financialTabProgressBar;
	}

	/**
	 * @return the ownersTabProgressBar
	 */
	public static String getOwnersTabProgressBar() {
		return OwnersTabProgressBar;
	}

	/**
	 * @param ownersTabProgressBar the ownersTabProgressBar to set
	 */
	public static void setOwnersTabProgressBar(String ownersTabProgressBar) {
		OwnersTabProgressBar = ownersTabProgressBar;
	}

	/**
	 * @return the businessTabProgressBar
	 */
	public static String getBusinessTabProgressBar() {
		return BusinessTabProgressBar;
	}

	/**
	 * @param businessTabProgressBar the businessTabProgressBar to set
	 */
	public static void setBusinessTabProgressBar(String businessTabProgressBar) {
		BusinessTabProgressBar = businessTabProgressBar;
	}

	/**
	 * @return the bPProgressBar
	 */
	public static String getBPProgressBar() {
		return BPProgressBar;
	}

	/**
	 * @param bPProgressBar the bPProgressBar to set
	 */
	public static void setBPProgressBar(String bPProgressBar) {
		BPProgressBar = bPProgressBar;
	}

	/**
	 * @return the aSOwnerShip
	 */
	public static String getASOwnerShip() {
		return ASOwnerShip;
	}

	/**
	 * @param aSOwnerShip the aSOwnerShip to set
	 */
	public static void setASOwnerShip(String aSOwnerShip) {
		ASOwnerShip = aSOwnerShip;
	}

	/**
	 * @return the aSODOB
	 */
	public static String getASODOB() {
		return ASODOB;
	}

	/**
	 * @param aSODOB the aSODOB to set
	 */
	public static void setASODOB(String aSODOB) {
		ASODOB = aSODOB;
	}

	/**
	 * @return the aSSSN
	 */
	public static String getASSSN() {
		return ASSSN;
	}

	/**
	 * @param aSSSN the aSSSN to set
	 */
	public static void setASSSN(String aSSSN) {
		ASSSN = aSSSN;
	}

	/**
	 * @return the successMessageAftersuccessCreatePasswordRequest
	 */
	public static String getSuccessMessageAftersuccessCreatePasswordRequest() {
		return SuccessMessageAftersuccessCreatePasswordRequest;
	}

	/**
	 * @param successMessageAftersuccessCreatePasswordRequest the successMessageAftersuccessCreatePasswordRequest to set
	 */
	public static void setSuccessMessageAftersuccessCreatePasswordRequest(
			String successMessageAftersuccessCreatePasswordRequest) {
		SuccessMessageAftersuccessCreatePasswordRequest = successMessageAftersuccessCreatePasswordRequest;
	}

	/**
	 * @return the messageTitleOnCreatePasswordPage
	 */
	public static String getMessageTitleOnCreatePasswordPage() {
		return MessageTitleOnCreatePasswordPage;
	}

	/**
	 * @return the ownersEmail
	 */
	public static String getOwnersEmail() {
		return OwnersEmail;
	}

	/**
	 * @param ownersEmail the ownersEmail to set
	 */
	public static void setOwnersEmail(String ownersEmail) {
		OwnersEmail = ownersEmail;
	}

	/**
	 * @return the ownersFirstName
	 */
	public static String getOwnersFirstName() {
		return OwnersFirstName;
	}

	/**
	 * @param ownersFirstName the ownersFirstName to set
	 */
	public static void setOwnersFirstName(String ownersFirstName) {
		OwnersFirstName = ownersFirstName;
	}

	/**
	 * @return the ownersLastName
	 */
	public static String getOwnersLastName() {
		return OwnersLastName;
	}

	/**
	 * @param ownersLastName the ownersLastName to set
	 */
	public static void setOwnersLastName(String ownersLastName) {
		OwnersLastName = ownersLastName;
	}

	/**
	 * @return the ownersAddress
	 */
	public static String getOwnersAddress() {
		return OwnersAddress;
	}

	/**
	 * @param ownersAddress the ownersAddress to set
	 */
	public static void setOwnersAddress(String ownersAddress) {
		OwnersAddress = ownersAddress;
	}

	/**
	 * @return the ownersCity
	 */
	public static String getOwnersCity() {
		return OwnersCity;
	}

	/**
	 * @param ownersCity the ownersCity to set
	 */
	public static void setOwnersCity(String ownersCity) {
		OwnersCity = ownersCity;
	}

	/**
	 * @return the ownersState
	 */
	public static String getOwnersState() {
		return OwnersState;
	}

	/**
	 * @param ownersState the ownersState to set
	 */
	public static void setOwnersState(String ownersState) {
		OwnersState = ownersState;
	}

	/**
	 * @return the ownersZipcode
	 */
	public static String getOwnersZipcode() {
		return OwnersZipcode;
	}

	/**
	 * @param ownersZipcode the ownersZipcode to set
	 */
	public static void setOwnersZipcode(String ownersZipcode) {
		OwnersZipcode = ownersZipcode;
	}

	/**
	 * @return the ownersPhoneNo
	 */
	public static String getOwnersPhoneNo() {
		return OwnersPhoneNo;
	}

	/**
	 * @param ownersPhoneNo the ownersPhoneNo to set
	 */
	public static void setOwnersPhoneNo(String ownersPhoneNo) {
		OwnersPhoneNo = ownersPhoneNo;
	}

	/**
	 * @return the ownersmobileno
	 */
	public static String getOwnersmobileno() {
		return Ownersmobileno;
	}

	/**
	 * @param ownersmobileno the ownersmobileno to set
	 */
	public static void setOwnersmobileno(String ownersmobileno) {
		Ownersmobileno = ownersmobileno;
	}

	/**
	 * @return the ownersOwnership
	 */
	public static String getOwnersOwnership() {
		return OwnersOwnership;
	}

	/**
	 * @param ownersOwnership the ownersOwnership to set
	 */
	public static void setOwnersOwnership(String ownersOwnership) {
		OwnersOwnership = ownersOwnership;
	}

	/**
	 * @return the ownersSSN
	 */
	public static String getOwnersSSN() {
		return OwnersSSN;
	}

	/**
	 * @param ownersSSN the ownersSSN to set
	 */
	public static void setOwnersSSN(String ownersSSN) {
		OwnersSSN = ownersSSN;
	}

	/**
	 * @return the ownersMonth
	 */
	public static String getOwnersMonth() {
		return OwnersMonth;
	}

	/**
	 * @param ownersMonth the ownersMonth to set
	 */
	public static void setOwnersMonth(String ownersMonth) {
		OwnersMonth = ownersMonth;
	}

	/**
	 * @return the ownersDay
	 */
	public static String getOwnersDay() {
		return OwnersDay;
	}

	/**
	 * @param ownersDay the ownersDay to set
	 */
	public static void setOwnersDay(String ownersDay) {
		OwnersDay = ownersDay;
	}

	/**
	 * @return the ownersYear
	 */
	public static String getOwnersYear() {
		return OwnersYear;
	}

	/**
	 * @param ownersYear the ownersYear to set
	 */
	public static void setOwnersYear(String ownersYear) {
		OwnersYear = ownersYear;
	}

	/**
	 * @return the ownersNext
	 */
	public static String getOwnersNext() {
		return OwnersNext;
	}

	/**
	 * @param ownersNext the ownersNext to set
	 */
	public static void setOwnersNext(String ownersNext) {
		OwnersNext = ownersNext;
	}

	/**
	 * @return the finalFinish
	 */
	public static String getFinalFinish() {
		return FinalFinish;
	}

	/**
	 * @param finalFinish the finalFinish to set
	 */
	public static void setFinalFinish(String finalFinish) {
		FinalFinish = finalFinish;
	}

	/**
	 * @param messageTitleOnCreatePasswordPage the messageTitleOnCreatePasswordPage to set
	 */
	public static void setMessageTitleOnCreatePasswordPage(String messageTitleOnCreatePasswordPage) {
		MessageTitleOnCreatePasswordPage = messageTitleOnCreatePasswordPage;
	}

	/**
	 * @return the bPCreateNewPassword
	 */
	public static String getBPCreateNewPassword() {
		return BPCreateNewPassword;
	}

	/**
	 * @param bPCreateNewPassword the bPCreateNewPassword to set
	 */
	public static void setBPCreateNewPassword(String bPCreateNewPassword) {
		BPCreateNewPassword = bPCreateNewPassword;
	}

	/**
	 * @return the bPCreateConfirmPassword
	 */
	public static String getBPCreateConfirmPassword() {
		return BPCreateConfirmPassword;
	}

	/**
	 * @param bPCreateConfirmPassword the bPCreateConfirmPassword to set
	 */
	public static void setBPCreateConfirmPassword(String bPCreateConfirmPassword) {
		BPCreateConfirmPassword = bPCreateConfirmPassword;
	}

	/**
	 * @return the bPCreatePasswordButton
	 */
	public static String getBPCreatePasswordButton() {
		return BPCreatePasswordButton;
	}

	/**
	 * @param bPCreatePasswordButton the bPCreatePasswordButton to set
	 */
	public static void setBPCreatePasswordButton(String bPCreatePasswordButton) {
		BPCreatePasswordButton = bPCreatePasswordButton;
	}

	/**
	 * @return the messageAfterCraetePasswordRequest
	 */
	public static String getMessageAfterCraetePasswordRequest() {
		return MessageAfterCraetePasswordRequest;
	}

	/**
	 * @param messageAfterCraetePasswordRequest the messageAfterCraetePasswordRequest to set
	 */
	public static void setMessageAfterCraetePasswordRequest(String messageAfterCraetePasswordRequest) {
		MessageAfterCraetePasswordRequest = messageAfterCraetePasswordRequest;
	}

	/**
	 * @return the forgotPasswordTitle
	 */
	public static String getForgotPasswordTitle() {
		return ForgotPasswordTitle;
	}

	/**
	 * @param forgotPasswordTitle the forgotPasswordTitle to set
	 */
	public static void setForgotPasswordTitle(String forgotPasswordTitle) {
		ForgotPasswordTitle = forgotPasswordTitle;
	}

	/**
	 * @return the signInErrorMessage
	 */
	public static String getSignInErrorMessage() {
		return SignInErrorMessage;
	}

	/**
	 * @param signInErrorMessage the signInErrorMessage to set
	 */
	public static void setSignInErrorMessage(String signInErrorMessage) {
		SignInErrorMessage = signInErrorMessage;
	}

	/**
	 * @return the businessTabDBA
	 */
	public static String getBusinessTabDBA() {
		return BusinessTabDBA;
	}

	/**
	 * @param businessTabDBA the businessTabDBA to set
	 */
	public static void setBusinessTabDBA(String businessTabDBA) {
		BusinessTabDBA = businessTabDBA;
	}

	/**
	 * @return the businessTabBusinessAddress
	 */
	public static String getBusinessTabBusinessAddress() {
		return BusinessTabBusinessAddress;
	}

	/**
	 * @param businessTabBusinessAddress the businessTabBusinessAddress to set
	 */
	public static void setBusinessTabBusinessAddress(String businessTabBusinessAddress) {
		BusinessTabBusinessAddress = businessTabBusinessAddress;
	}

	/**
	 * @return the businessTabCity
	 */
	public static String getBusinessTabCity() {
		return BusinessTabCity;
	}

	/**
	 * @param businessTabCity the businessTabCity to set
	 */
	public static void setBusinessTabCity(String businessTabCity) {
		BusinessTabCity = businessTabCity;
	}

	/**
	 * @return the businessTabState
	 */
	public static String getBusinessTabState() {
		return BusinessTabState;
	}

	/**
	 * @param businessTabState the businessTabState to set
	 */
	public static void setBusinessTabState(String businessTabState) {
		BusinessTabState = businessTabState;
	}

	/**
	 * @return the businessTabZipcode
	 */
	public static String getBusinessTabZipcode() {
		return BusinessTabZipcode;
	}

	/**
	 * @param businessTabZipcode the businessTabZipcode to set
	 */
	public static void setBusinessTabZipcode(String businessTabZipcode) {
		BusinessTabZipcode = businessTabZipcode;
	}

	/**
	 * @return the businessTabBusinessPhone
	 */
	public static String getBusinessTabBusinessPhone() {
		return BusinessTabBusinessPhone;
	}

	/**
	 * @param businessTabBusinessPhone the businessTabBusinessPhone to set
	 */
	public static void setBusinessTabBusinessPhone(String businessTabBusinessPhone) {
		BusinessTabBusinessPhone = businessTabBusinessPhone;
	}

	/**
	 * @return the businessTabMonth
	 */
	public static String getBusinessTabMonth() {
		return BusinessTabMonth;
	}

	/**
	 * @param businessTabMonth the businessTabMonth to set
	 */
	public static void setBusinessTabMonth(String businessTabMonth) {
		BusinessTabMonth = businessTabMonth;
	}

	/**
	 * @return the businessTabDay
	 */
	public static String getBusinessTabDay() {
		return BusinessTabDay;
	}

	/**
	 * @param businessTabDay the businessTabDay to set
	 */
	public static void setBusinessTabDay(String businessTabDay) {
		BusinessTabDay = businessTabDay;
	}

	/**
	 * @return the businessTabYear
	 */
	public static String getBusinessTabYear() {
		return BusinessTabYear;
	}

	/**
	 * @param businessTabYear the businessTabYear to set
	 */
	public static void setBusinessTabYear(String businessTabYear) {
		BusinessTabYear = businessTabYear;
	}

	/**
	 * @return the businessTabBusinessWebsite
	 */
	public static String getBusinessTabBusinessWebsite() {
		return BusinessTabBusinessWebsite;
	}

	/**
	 * @param businessTabBusinessWebsite the businessTabBusinessWebsite to set
	 */
	public static void setBusinessTabBusinessWebsite(String businessTabBusinessWebsite) {
		BusinessTabBusinessWebsite = businessTabBusinessWebsite;
	}

	/**
	 * @return the businessTabLegalEntity
	 */
	public static String getBusinessTabLegalEntity() {
		return BusinessTabLegalEntity;
	}

	/**
	 * @param businessTabLegalEntity the businessTabLegalEntity to set
	 */
	public static void setBusinessTabLegalEntity(String businessTabLegalEntity) {
		BusinessTabLegalEntity = businessTabLegalEntity;
	}

	/**
	 * @return the businessTabBusinessLoacation
	 */
	public static String getBusinessTabBusinessLoacation() {
		return BusinessTabBusinessLoacation;
	}

	/**
	 * @param businessTabBusinessLoacation the businessTabBusinessLoacation to set
	 */
	public static void setBusinessTabBusinessLoacation(String businessTabBusinessLoacation) {
		BusinessTabBusinessLoacation = businessTabBusinessLoacation;
	}

	/**
	 * @return the businessTabTaxId
	 */
	public static String getBusinessTabTaxId() {
		return BusinessTabTaxId;
	}

	/**
	 * @param businessTabTaxId the businessTabTaxId to set
	 */
	public static void setBusinessTabTaxId(String businessTabTaxId) {
		BusinessTabTaxId = businessTabTaxId;
	}

	/**
	 * @return the businessTabIndustry
	 */
	public static String getBusinessTabIndustry() {
		return BusinessTabIndustry;
	}

	/**
	 * @param businessTabIndustry the businessTabIndustry to set
	 */
	public static void setBusinessTabIndustry(String businessTabIndustry) {
		BusinessTabIndustry = businessTabIndustry;
	}

	/**
	 * @return the businessTabImportant
	 */
	public static String getBusinessTabImportant() {
		return BusinessTabImportant;
	}

	/**
	 * @param businessTabImportant the businessTabImportant to set
	 */
	public static void setBusinessTabImportant(String businessTabImportant) {
		BusinessTabImportant = businessTabImportant;
	}

	/**
	 * @return the businessTabNext
	 */
	public static String getBusinessTabNext() {
		return BusinessTabNext;
	}

	/**
	 * @param businessTabNext the businessTabNext to set
	 */
	public static void setBusinessTabNext(String businessTabNext) {
		BusinessTabNext = businessTabNext;
	}

	/**
	 * @return the bPBrandingLogo
	 */
	public static String getBPBrandingLogo() {
		return BPBrandingLogo;
	}

	/**
	 * @param bPBrandingLogo the bPBrandingLogo to set
	 */
	public static void setBPBrandingLogo(String bPBrandingLogo) {
		BPBrandingLogo = bPBrandingLogo;
	}

	/**
	 * @return the aSBSD
	 */
	public static String getASBSD() {
		return ASBSD;
	}

	/**
	 * @param aSBSD the aSBSD to set
	 */
	public static void setASBSD(String aSBSD) {
		ASBSD = aSBSD;
	}

	/**
	 * @return the aSEmail
	 */
	public static String getASEmail() {
		return ASEmail;
	}

	/**
	 * @param aSEmail the aSEmail to set
	 */
	public static void setASEmail(String aSEmail) {
		ASEmail = aSEmail;
	}

	/**
	 * @return the aSAmountSeeking
	 */
	public static String getASAmountSeeking() {
		return ASAmountSeeking;
	}

	/**
	 * @param aSAmountSeeking the aSAmountSeeking to set
	 */
	public static void setASAmountSeeking(String aSAmountSeeking) {
		ASAmountSeeking = aSAmountSeeking;
	}

	/**
	 * @return the aSBusinessName
	 */
	public static String getASBusinessName() {
		return ASBusinessName;
	}

	/**
	 * @param aSBusinessName the aSBusinessName to set
	 */
	public static void setASBusinessName(String aSBusinessName) {
		ASBusinessName = aSBusinessName;
	}

	/**
	 * @return the aSFirstName
	 */
	public static String getASFirstName() {
		return ASFirstName;
	}

	/**
	 * @param aSFirstName the aSFirstName to set
	 */
	public static void setASFirstName(String aSFirstName) {
		ASFirstName = aSFirstName;
	}

	/**
	 * @return the aSLastName
	 */
	public static String getASLastName() {
		return ASLastName;
	}

	/**
	 * @param aSLastName the aSLastName to set
	 */
	public static void setASLastName(String aSLastName) {
		ASLastName = aSLastName;
	}

	/**
	 * @return the aSPhone
	 */
	public static String getASPhone() {
		return ASPhone;
	}

	/**
	 * @param aSPhone the aSPhone to set
	 */
	public static void setASPhone(String aSPhone) {
		ASPhone = aSPhone;
	}

	/**
	 * @return the aSTaxId
	 */
	public static String getASTaxId() {
		return ASTaxId;
	}

	/**
	 * @param aSTaxId the aSTaxId to set
	 */
	public static void setASTaxId(String aSTaxId) {
		ASTaxId = aSTaxId;
	}

	/**
	 * @return the aSPort
	 */
	public static String getASPort() {
		return ASPort;
	}

	/**
	 * @param aSPort the aSPort to set
	 */
	public static void setASPort(String aSPort) {
		ASPort = aSPort;
	}

	/**
	 * @return the screenShotPath
	 */
	public static String getScreenShotPath() {
		return ScreenShotPath;
	}

	/**
	 * @param screenShotPath the screenShotPath to set
	 */
	public static void setScreenShotPath(String screenShotPath) {
		ScreenShotPath = screenShotPath;
	}

	/**
	 * @return the fein
	 */
	public static int getFein() {
		return Fein;
	}

	/**
	 * @param fein the fein to set
	 */
	public static void setFein(int fein) {
		Fein = fein;
	}

	public static int MinimumMatchRequiredForSearch;
	public static boolean IsXml; 
	public static String phone;
	
	/**
	 * @return the phone
	 */
	public static String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public static void setPhone(String phone) {
		ApiParam.phone = phone;
	}

	/**
	 * @return the minimumMatchRequiredForSearch
	 */
	public static int getMinimumMatchRequiredForSearch() {
		return MinimumMatchRequiredForSearch;
	}

	/**
	 * @param minimumMatchRequiredForSearch the minimumMatchRequiredForSearch to set
	 */
	public static void setMinimumMatchRequiredForSearch(int minimumMatchRequiredForSearch) {
		MinimumMatchRequiredForSearch = minimumMatchRequiredForSearch;
	}

	/**
	 * @return the isXml
	 */
	public static boolean isIsXml() {
		return IsXml;
	}

	/**
	 * @param isXml the isXml to set
	 */
	public static void setIsXml(boolean isXml) {
		IsXml = isXml;
	}


	/**
	 * @return the portNo
	 */
	public static String getPortNo() {
		return portNo;
	}

	/**
	 * @param portNo the portNo to set
	 */
	public static void setPortNo(String portNo) {
		ApiParam.portNo = portNo;
	}

	/**
	 * @return the document
	 */
	public static String getDocument() {
		return Document;
	}

	/**
	 * @param document the document to set
	 */
	public static void setDocument(String document) {
		Document = document;
	}


	/**
	 * @return the iframeID
	 */
	public static String getIframeID() {
		return IframeID;
	}

	/**
	 * @param iframeID the iframeID to set
	 */
	public static void setIframeID(String iframeID) {
		IframeID = iframeID;
	}

	/**
	 * @return the inboxURL
	 */
	public static String getInboxURL() {
		return InboxURL;
	}

	/**
	 * @param inboxURL the inboxURL to set
	 */
	public static void setInboxURL(String inboxURL) {
		InboxURL = inboxURL;
	}

	/**
	 * @return the profilePage
	 */
	public static String getProfilePage() {
		return ProfilePage;
	}

	/**
	 * @param profilePage the profilePage to set
	 */
	public static void setProfilePage(String profilePage) {
		ProfilePage = profilePage;
	}

	/**
	 * @return the toDoPage
	 */
	public static String getToDoPage() {
		return ToDoPage;
	}

	/**
	 * @param toDoPage the toDoPage to set
	 */
	public static void setToDoPage(String toDoPage) {
		ToDoPage = toDoPage;
	}

	/**
	 * @return the overViewPage
	 */
	public static String getOverViewPage() {
		return OverViewPage;
	}

	/**
	 * @param overViewPage the overViewPage to set
	 */
	public static void setOverViewPage(String overViewPage) {
		OverViewPage = overViewPage;
	}

	/**
	 * @return the changePasswordPage
	 */
	public static String getChangePasswordPage() {
		return ChangePasswordPage;
	}

	/**
	 * @return the bPLogout
	 */
	public static String getBPLogout() {
		return BPLogout;
	}

	/**
	 * @param bPLogout the bPLogout to set
	 */
	public static void setBPLogout(String bPLogout) {
		BPLogout = bPLogout;
	}

	/**
	 * @param changePasswordPage the changePasswordPage to set
	 */
	public static void setChangePasswordPage(String changePasswordPage) {
		ChangePasswordPage = changePasswordPage;
	}

	/**
	 * @return the successMessageOnForgotPasswordPage
	 */
	public static String getSuccessMessageOnForgotPasswordPage() {
		return SuccessMessageOnForgotPasswordPage;
	}

	/**
	 * @param successMessageOnForgotPasswordPage the successMessageOnForgotPasswordPage to set
	 */
	public static void setSuccessMessageOnForgotPasswordPage(String successMessageOnForgotPasswordPage) {
		SuccessMessageOnForgotPasswordPage = successMessageOnForgotPasswordPage;
	}

	/**
	 * @return the successMessageOnChangePasswordPage
	 */
	public static String getSuccessMessageOnChangePasswordPage() {
		return SuccessMessageOnChangePasswordPage;
	}

	/**
	 * @param successMessageOnChangePasswordPage the successMessageOnChangePasswordPage to set
	 */
	public static void setSuccessMessageOnChangePasswordPage(String successMessageOnChangePasswordPage) {
		SuccessMessageOnChangePasswordPage = successMessageOnChangePasswordPage;
	}

	/**
	 * @return the borrowerName
	 */
	public static String getBorrowerName() {
		return BorrowerName;
	}

	/**
	 * @param borrowerName the borrowerName to set
	 */
	public static void setBorrowerName(String borrowerName) {
		BorrowerName = borrowerName;
	}

	/**
	 * @return the changePasswordLink
	 */
	public static String getChangePasswordLink() {
		return ChangePasswordLink;
	}

	/**
	 * @param changePasswordLink the changePasswordLink to set
	 */
	public static void setChangePasswordLink(String changePasswordLink) {
		ChangePasswordLink = changePasswordLink;
	}


	/**
	 * @return the forgotPasswordPageEmailAddress
	 */
	public static String getForgotPasswordPageEmailAddress() {
		return ForgotPasswordPageEmailAddress;
	}

	/**
	 * @param forgotPasswordPageEmailAddress the forgotPasswordPageEmailAddress to set
	 */
	public static void setForgotPasswordPageEmailAddress(String forgotPasswordPageEmailAddress) {
		ForgotPasswordPageEmailAddress = forgotPasswordPageEmailAddress;
	}

	/**
	 * @return the fotgotPasswordSubmitButton
	 */
	public static String getFotgotPasswordSubmitButton() {
		return FotgotPasswordSubmitButton;
	}

	/**
	 * @param fotgotPasswordSubmitButton the fotgotPasswordSubmitButton to set
	 */
	public static void setFotgotPasswordSubmitButton(String fotgotPasswordSubmitButton) {
		FotgotPasswordSubmitButton = fotgotPasswordSubmitButton;
	}

	/**
	 * @return the signInPageEmailAddress
	 */
	public static String getSignInPageEmailAddress() {
		return SignInPageEmailAddress;
	}

	/**
	 * @param signInPageEmailAddress the signInPageEmailAddress to set
	 */
	public static void setSignInPageEmailAddress(String signInPageEmailAddress) {
		SignInPageEmailAddress = signInPageEmailAddress;
	}

	/**
	 * @return the signInPagePassword
	 */
	public static String getSignInPagePassword() {
		return SignInPagePassword;
	}

	/**
	 * @param signInPagePassword the signInPagePassword to set
	 */
	public static void setSignInPagePassword(String signInPagePassword) {
		SignInPagePassword = signInPagePassword;
	}

	/**
	 * @return the signInPageLoginButton
	 */
	public static String getSignInPageLoginButton() {
		return SignInPageLoginButton;
	}

	/**
	 * @param signInPageLoginButton the signInPageLoginButton to set
	 */
	public static void setSignInPageLoginButton(String signInPageLoginButton) {
		SignInPageLoginButton = signInPageLoginButton;
	}

	/**
	 * @return the signInPageForgotPasswordLink
	 */
	public static String getSignInPageForgotPasswordLink() {
		return SignInPageForgotPasswordLink;
	}

	/**
	 * @param signInPageForgotPasswordLink the signInPageForgotPasswordLink to set
	 */
	public static void setSignInPageForgotPasswordLink(String signInPageForgotPasswordLink) {
		SignInPageForgotPasswordLink = signInPageForgotPasswordLink;
	}

	/**
	 * @return the bPURL
	 */
	public static String getBPURL() {
		return BPURL;
	}

	/**
	 * @return the bPBusinessName
	 */
	public static String getBPBusinessName() {
		return BPBusinessName;
	}

	/**
	 * @param bPBusinessName the bPBusinessName to set
	 */
	public static void setBPBusinessName(String bPBusinessName) {
		BPBusinessName = bPBusinessName;
	}

	/**
	 * @return the bPAmountSeeking
	 */
	public static String getBPAmountSeeking() {
		return BPAmountSeeking;
	}

	/**
	 * @param bPAmountSeeking the bPAmountSeeking to set
	 */
	public static void setBPAmountSeeking(String bPAmountSeeking) {
		BPAmountSeeking = bPAmountSeeking;
	}

	/**
	 * @return the bPUseOfFunds
	 */
	public static String getBPUseOfFunds() {
		return BPUseOfFunds;
	}

	/**
	 * @param bPUseOfFunds the bPUseOfFunds to set
	 */
	public static void setBPUseOfFunds(String bPUseOfFunds) {
		BPUseOfFunds = bPUseOfFunds;
	}

	/**
	 * @return the bPAnnualBusinessRevenue
	 */
	public static String getBPAnnualBusinessRevenue() {
		return BPAnnualBusinessRevenue;
	}

	/**
	 * @param bPAnnualBusinessRevenue the bPAnnualBusinessRevenue to set
	 */
	public static void setBPAnnualBusinessRevenue(String bPAnnualBusinessRevenue) {
		BPAnnualBusinessRevenue = bPAnnualBusinessRevenue;
	}

	/**
	 * @return the bPFirstName
	 */
	public static String getBPFirstName() {
		return BPFirstName;
	}

	/**
	 * @param bPFirstName the bPFirstName to set
	 */
	public static void setBPFirstName(String bPFirstName) {
		BPFirstName = bPFirstName;
	}

	/**
	 * @return the bPLastName
	 */
	public static String getBPLastName() {
		return BPLastName;
	}

	/**
	 * @param bPLastName the bPLastName to set
	 */
	public static void setBPLastName(String bPLastName) {
		BPLastName = bPLastName;
	}

	/**
	 * @return the bPPhone
	 */
	public static String getBPPhone() {
		return BPPhone;
	}

	/**
	 * @param bPPhone the bPPhone to set
	 */
	public static void setBPPhone(String bPPhone) {
		BPPhone = bPPhone;
	}

	/**
	 * @return the bPEmail
	 */
	public static String getBPEmail() {
		return BPEmail;
	}

	/**
	 * @param bPEmail the bPEmail to set
	 */
	public static void setBPEmail(String bPEmail) {
		BPEmail = bPEmail;
	}

	/**
	 * @return the bPPaasword
	 */
	public static String getBPPaasword() {
		return BPPaasword;
	}

	/**
	 * @param bPPaasword the bPPaasword to set
	 */
	public static void setBPPaasword(String bPPaasword) {
		BPPaasword = bPPaasword;
	}

	/**
	 * @return the bPNextButtonInBasicTab
	 */
	public static String getBPNextButtonInBasicTab() {
		return BPNextButtonInBasicTab;
	}

	/**
	 * @param bPNextButtonInBasicTab the bPNextButtonInBasicTab to set
	 */
	public static void setBPNextButtonInBasicTab(String bPNextButtonInBasicTab) {
		BPNextButtonInBasicTab = bPNextButtonInBasicTab;
	}

	/**
	 * @param bPURL the bPURL to set
	 */
	public static void setBPURL(String bPURL) {
		BPURL = bPURL;
	}

	/**
	 * @return the bPImageURL
	 */
	public static String getBPImageURL() {
		return BPImageURL;
	}

	/**
	 * @param bPImageURL the bPImageURL to set
	 */
	public static void setBPImageURL(String bPImageURL) {
		BPImageURL = bPImageURL;
	}

	/**
	 * @return the bPSignButton
	 */
	public static String getBPSignButton() {
		return BPSignButton;
	}

	/**
	 * @param bPSignButton the bPSignButton to set
	 */
	public static void setBPSignButton(String bPSignButton) {
		BPSignButton = bPSignButton;
	}

	/**
	 * @return the messageOnSearchPage
	 */
	public static String getMessageOnSearchPage() {
		return MessageOnSearchPage;
	}

	/**
	 * @param messageOnSearchPage the messageOnSearchPage to set
	 */
	public static void setMessageOnSearchPage(String messageOnSearchPage) {
		MessageOnSearchPage = messageOnSearchPage;
	}

	/**
	 * @return the dBHostName
	 */
	public static String getDBHostName() {
		return DBHostName;
	}

	/**
	 * @param dBHostName the dBHostName to set
	 */
	public static void setDBHostName(String dBHostName) {
		DBHostName = dBHostName;
	}

	/**
	 * @return the dBPort
	 */
	public static String getDBPort() {
		return DBPort;
	}

	/**
	 * @param dBPort the dBPort to set
	 */
	public static void setDBPort(String dBPort) {
		DBPort = dBPort;
	}

	/**
	 * @return the dBUserName
	 */
	public static String getDBUserName() {
		return DBUserName;
	}

	/**
	 * @param dBUserName the dBUserName to set
	 */
	public static void setDBUserName(String dBUserName) {
		DBUserName = dBUserName;
	}

	/**
	 * @return the dBPassword
	 */
	public static String getDBPassword() {
		return DBPassword;
	}

	/**
	 * @param dBPassword the dBPassword to set
	 */
	public static void setDBPassword(String dBPassword) {
		DBPassword = dBPassword;
	}

	/**
	 * @return the authenticationDB
	 */
	public static String getAuthenticationDB() {
		return AuthenticationDB;
	}

	/**
	 * @param authenticationDB the authenticationDB to set
	 */
	public static void setAuthenticationDB(String authenticationDB) {
		AuthenticationDB = authenticationDB;
	}

	/**
	 * @return the searchResult
	 */
	public static String getSearchResult() {
		return SearchResult;
	}

	/**
	 * @param searchResult the searchResult to set
	 */
	public static void setSearchResult(String searchResult) {
		SearchResult = searchResult;
	}


	/**
	 * @return the baseresturl
	 */
	public static String getBaseresturl() {
		return baseresturl;
	}

	/**
	 * @param baseresturl the baseresturl to set
	 */
	public static void setBaseresturl(String baseresturl) {
		ApiParam.baseresturl = baseresturl;
	}

	/**
	 * @return the authToken
	 */
	public static String getAuthToken() {
		return AuthToken;
	}

	/**
	 * @param authToken the authToken to set
	 */
	public static void setAuthToken(String authToken) {
		AuthToken = authToken;
	}

	/**
	 * @return the requestType
	 */
	public static String getRequestType() {
		return requestType;
	}

	/**
	 * @param requestType the requestType to set
	 */
	public static void setRequestType(String requestType) {
		ApiParam.requestType = requestType;
	}

	/**
	 * @return the contentType
	 */
	public static String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public static void setContentType(String contentType) {
		ApiParam.contentType = contentType;
	}

	/**
	 * @return the line1
	 */
	public static String getLine1() {
		return line1;
	}

	/**
	 * @param line1 the line1 to set
	 */
	public static void setLine1(String line1) {
		ApiParam.line1 = line1;
	}

	/**
	 * @return the backOfficeURL
	 */
	public static String getBackOfficeURL() {
		return BackOfficeURL;
	}

	/**
	 * @param backOfficeURL the backOfficeURL to set
	 */
	public static void setBackOfficeURL(String backOfficeURL) {
		BackOfficeURL = backOfficeURL;
	}

	/**
	 * @return the borrowerPortal
	 */
	public static String getBorrowerPortal() {
		return BorrowerPortal;
	}

	/**
	 * @param borrowerPortal the borrowerPortal to set
	 */
	public static void setBorrowerPortal(String borrowerPortal) {
		BorrowerPortal = borrowerPortal;
	}

	/**
	 * @return the backOfficeImageURL
	 */
	public static String getBackOfficeImageURL() {
		return BackOfficeImageURL;
	}

	/**
	 * @param backOfficeImageURL the backOfficeImageURL to set
	 */
	public static void setBackOfficeImageURL(String backOfficeImageURL) {
		BackOfficeImageURL = backOfficeImageURL;
	}

	/**
	 * @return the environment
	 */
	public static String getEnvironment() {
		return environment;
	}

	/**
	 * @param environment the environment to set
	 */
	public static void setEnvironment(String environment) {
		ApiParam.environment = environment;
	}

	/**
	 * @return the port
	 */
	public static String getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public static void setPort(String port) {
		ApiParam.port = port;
	}

	/**
	 * @return the brandingLogoSearchPage
	 */
	public static String getBrandingLogoSearchPage() {
		return BrandingLogoSearchPage;
	}

	/**
	 * @param brandingLogoSearchPage the brandingLogoSearchPage to set
	 */
	public static void setBrandingLogoSearchPage(String brandingLogoSearchPage) {
		BrandingLogoSearchPage = brandingLogoSearchPage;
	}

	/**
	 * @return the brandingLogoLoginPage
	 */
	public static String getBrandingLogoLoginPage() {
		return BrandingLogoLoginPage;
	}

	/**
	 * @param brandingLogoLoginPage the brandingLogoLoginPage to set
	 */
	public static void setBrandingLogoLoginPage(String brandingLogoLoginPage) {
		BrandingLogoLoginPage = brandingLogoLoginPage;
	}

	/**
	 * @return the userName
	 */
	public static String getUserName() {
		return UserName;
	}

	/**
	 * @param userName the userName to set
	 */
	public static void setUserName(String userName) {
		UserName = userName;
	}

	/**
	 * @return the password
	 */
	public static String getPassword() {
		return Password;
	}

	/**
	 * @param password the password to set
	 */
	public static void setPassword(String password) {
		Password = password;
	}

	/**
	 * @return the consentCheckBox
	 */
	public static String getConsentCheckBox() {
		return ConsentCheckBox;
	}

	/**
	 * @param consentCheckBox the consentCheckBox to set
	 */
	public static void setConsentCheckBox(String consentCheckBox) {
		ConsentCheckBox = consentCheckBox;
	}

	/**
	 * @return the signButton
	 */
	public static String getSignButton() {
		return SignButton;
	}

	/**
	 * @param signButton the signButton to set
	 */
	public static void setSignButton(String signButton) {
		SignButton = signButton;
	}

	/**
	 * @return the profileLink
	 */
	public static String getProfileLink() {
		return ProfileLink;
	}

	/**
	 * @param profileLink the profileLink to set
	 */
	public static void setProfileLink(String profileLink) {
		ProfileLink = profileLink;
	}

	/**
	 * @return the logOutLink
	 */
	public static String getLogOutLink() {
		return LogOutLink;
	}

	/**
	 * @param logOutLink the logOutLink to set
	 */
	public static void setLogOutLink(String logOutLink) {
		LogOutLink = logOutLink;
	}

	/**
	 * @return the errorMessageonHomePage
	 */
	public static String getErrorMessageonHomePage() {
		return ErrorMessageonHomePage;
	}

	/**
	 * @param errorMessageonHomePage the errorMessageonHomePage to set
	 */
	public static void setErrorMessageonHomePage(String errorMessageonHomePage) {
		ErrorMessageonHomePage = errorMessageonHomePage;
	}

	/**
	 * @return the consentMessageonHomePage
	 */
	public static String getConsentMessageonHomePage() {
		return ConsentMessageonHomePage;
	}

	/**
	 * @param consentMessageonHomePage the consentMessageonHomePage to set
	 */
	public static void setConsentMessageonHomePage(String consentMessageonHomePage) {
		ConsentMessageonHomePage = consentMessageonHomePage;
	}

	/**
	 * @return the forgotpasswordlink
	 */
	public static String getForgotpasswordlink() {
		return Forgotpasswordlink;
	}

	/**
	 * @param forgotpasswordlink the forgotpasswordlink to set
	 */
	public static void setForgotpasswordlink(String forgotpasswordlink) {
		Forgotpasswordlink = forgotpasswordlink;
	}

	/**
	 * @return the messageonResetPasswordPage
	 */
	public static String getMessageonResetPasswordPage() {
		return MessageonResetPasswordPage;
	}

	/**
	 * @param messageonResetPasswordPage the messageonResetPasswordPage to set
	 */
	public static void setMessageonResetPasswordPage(String messageonResetPasswordPage) {
		MessageonResetPasswordPage = messageonResetPasswordPage;
	}

	/**
	 * @return the sendRestPasswordButton
	 */
	public static String getSendRestPasswordButton() {
		return SendRestPasswordButton;
	}

	/**
	 * @param sendRestPasswordButton the sendRestPasswordButton to set
	 */
	public static void setSendRestPasswordButton(String sendRestPasswordButton) {
		SendRestPasswordButton = sendRestPasswordButton;
	}

	/**
	 * @return the successMessage
	 */
	public static String getSuccessMessage() {
		return SuccessMessage;
	}

	/**
	 * @param successMessage the successMessage to set
	 */
	public static void setSuccessMessage(String successMessage) {
		SuccessMessage = successMessage;
	}

	/**
	 * @return the emailtextField
	 */
	public static String getEmailtextField() {
		return EmailtextField;
	}

	/**
	 * @param emailtextField the emailtextField to set
	 */
	public static void setEmailtextField(String emailtextField) {
		EmailtextField = emailtextField;
	}

	/**
	 * @return the go
	 */
	public static String getGo() {
		return Go;
	}

	/**
	 * @param go the go to set
	 */
	public static void setGo(String go) {
		Go = go;
	}

	/**
	 * @return the restPasswordSubject
	 */
	public static String getRestPasswordSubject() {
		return RestPasswordSubject;
	}

	/**
	 * @param restPasswordSubject the restPasswordSubject to set
	 */
	public static void setRestPasswordSubject(String restPasswordSubject) {
		RestPasswordSubject = restPasswordSubject;
	}

	/**
	 * @return the restPasswordlink
	 */
	public static String getRestPasswordlink() {
		return RestPasswordlink;
	}

	/**
	 * @param restPasswordlink the restPasswordlink to set
	 */
	public static void setRestPasswordlink(String restPasswordlink) {
		RestPasswordlink = restPasswordlink;
	}

	/**
	 * @return the timePeriod
	 */
	public static String getTimePeriod() {
		return TimePeriod;
	}

	/**
	 * @param timePeriod the timePeriod to set
	 */
	public static void setTimePeriod(String timePeriod) {
		TimePeriod = timePeriod;
	}

	/**
	 * @return the textOnRestPasswordPage
	 */
	public static String getTextOnRestPasswordPage() {
		return TextOnRestPasswordPage;
	}

	/**
	 * @param textOnRestPasswordPage the textOnRestPasswordPage to set
	 */
	public static void setTextOnRestPasswordPage(String textOnRestPasswordPage) {
		TextOnRestPasswordPage = textOnRestPasswordPage;
	}

	/**
	 * @return the newPasswordField
	 */
	public static String getNewPasswordField() {
		return NewPasswordField;
	}

	/**
	 * @param newPasswordField the newPasswordField to set
	 */
	public static void setNewPasswordField(String newPasswordField) {
		NewPasswordField = newPasswordField;
	}

	/**
	 * @return the confirmPasswordField
	 */
	public static String getConfirmPasswordField() {
		return ConfirmPasswordField;
	}

	/**
	 * @param confirmPasswordField the confirmPasswordField to set
	 */
	public static void setConfirmPasswordField(String confirmPasswordField) {
		ConfirmPasswordField = confirmPasswordField;
	}

	/**
	 * @return the restButton
	 */
	public static String getRestButton() {
		return RestButton;
	}

	/**
	 * @param restButton the restButton to set
	 */
	public static void setRestButton(String restButton) {
		RestButton = restButton;
	}

	/**
	 * @return the successMessageAfterRest
	 */
	public static String getSuccessMessageAfterRest() {
		return SuccessMessageAfterRest;
	}

	/**
	 * @param successMessageAfterRest the successMessageAfterRest to set
	 */
	public static void setSuccessMessageAfterRest(String successMessageAfterRest) {
		SuccessMessageAfterRest = successMessageAfterRest;
	}

	/**
	 * @return the reLogin
	 */
	public static String getReLogin() {
		return ReLogin;
	}

	/**
	 * @param reLogin the reLogin to set
	 */
	public static void setReLogin(String reLogin) {
		ReLogin = reLogin;
	}

	/**
	 * @return the errorMessageAfterRest
	 */
	public static String getErrorMessageAfterRest() {
		return ErrorMessageAfterRest;
	}

	/**
	 * @param errorMessageAfterRest the errorMessageAfterRest to set
	 */
	public static void setErrorMessageAfterRest(String errorMessageAfterRest) {
		ErrorMessageAfterRest = errorMessageAfterRest;
	}

	/**
	 * @return the verifyPasswordErrorMessage
	 */
	public static String getVerifyPasswordErrorMessage() {
		return verifyPasswordErrorMessage;
	}

	/**
	 * @param verifyPasswordErrorMessage the verifyPasswordErrorMessage to set
	 */
	public static void setVerifyPasswordErrorMessage(String verifyPasswordErrorMessage) {
		ApiParam.verifyPasswordErrorMessage = verifyPasswordErrorMessage;
	}

	/**
	 * @return the firstName
	 */
	public static String getFirstName() {
		return FirstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public static void setFirstName(String firstName) {
		FirstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public static String getLastName() {
		return LastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public static void setLastName(String lastName) {
		LastName = lastName;
	}

	/**
	 * @return the businessEmail
	 */
	public static String getBusinessEmail() {
		return BusinessEmail;
	}

	/**
	 * @param businessEmail the businessEmail to set
	 */
	public static void setBusinessEmail(String businessEmail) {
		BusinessEmail = businessEmail;
	}

	/**
	 * @return the businessName
	 */
	public static String getBusinessName() {
		return BusinessName;
	}

	/**
	 * @param businessName the businessName to set
	 */
	public static void setBusinessName(String businessName) {
		BusinessName = businessName;
	}

	/**
	 * @return the status
	 */
	public static String getStatus() {
		return Status;
	}

	/**
	 * @param status the status to set
	 */
	public static void setStatus(String status) {
		Status = status;
	}

	/**
	 * @return the applicationDate
	 */
	public static String getApplicationDate() {
		return ApplicationDate;
	}

	/**
	 * @param applicationDate the applicationDate to set
	 */
	public static void setApplicationDate(String applicationDate) {
		ApplicationDate = applicationDate;
	}

	/**
	 * @return the expiryDate
	 */
	public static String getExpiryDate() {
		return ExpiryDate;
	}

	/**
	 * @param expiryDate the expiryDate to set
	 */
	public static void setExpiryDate(String expiryDate) {
		ExpiryDate = expiryDate;
	}

	/**
	 * @return the mobileNumber
	 */
	public static String getMobileNumber() {
		return MobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public static void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	/**
	 * @return the searchButton
	 */
	public static String getSearchButton() {
		return SearchButton;
	}

	/**
	 * @param searchButton the searchButton to set
	 */
	public static void setSearchButton(String searchButton) {
		SearchButton = searchButton;
	}

	/**
	 * @return the applicationNumber
	 */
	public static String getApplicationNumber() {
		return ApplicationNumber;
	}

	/**
	 * @return the workflow
	 */
	public static String getWorkflow() {
		return Workflow;
	}

	/**
	 * @param workflow the workflow to set
	 */
	public static void setWorkflow(String workflow) {
		Workflow = workflow;
	}

	/**
	 * @param applicationNumber the applicationNumber to set
	 */
	public static void setApplicationNumber(String applicationNumber) {
		ApplicationNumber = applicationNumber;
	}

	/**
	 * @return the productID
	 */
	public static String getProductID() {
		return ProductID;
	}

	/**
	 * @param productID the productID to set
	 */
	public static void setProductID(String productID) {
		ProductID = productID;
	}
	
	
}
