package net.sigmainfo.lf.automation.api.dataset;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

/**
 * Created by Rengarajan.M on 30-10-2017.
 */
@Component
public class Owners {
	private String FirstName;
	private String LastName;
	private String OwnershipPercentage;
	private String SSN;
	private String DOB;
	private ArrayList<Addresses> addresses;
	private ArrayList<PhoneNumbers> phoneNumbers;
	private ArrayList<EmailAddresses> emailAddresses;
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return this.FirstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.FirstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return this.LastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.LastName = lastName;
	}
	/**
	 * @return the ownershipPercentage
	 */
	public String getOwnershipPercentage() {
		return this.OwnershipPercentage;
	}
	/**
	 * @param ownershipPercentage the ownershipPercentage to set
	 */
	public void setOwnershipPercentage(String ownershipPercentage) {
		this.OwnershipPercentage = ownershipPercentage;
	}
	/**
	 * @return the sSN
	 */
	public String getSSN() {
		return this.SSN;
	}
	/**
	 * @param sSN the sSN to set
	 */
	public void setSSN(String sSN) {
		this.SSN = sSN;
	}
	/**
	 * @return the dOB
	 */
	public String getDOB() {
		return this.DOB;
	}
	/**
	 * @param dOB the dOB to set
	 */
	public void setDOB(String dOB) {
		this.DOB = dOB;
	}
	/**
	 * @return the addresses
	 */
	public ArrayList<Addresses> getAddresses() {
		return this.addresses;
	}
	/**
	 * @param addresses the addresses to set
	 */
	public void setAddresses(ArrayList<Addresses> addresses) {
	this.addresses = addresses;}
	/**
	 * @return the phoneNumbers
	 */
	public ArrayList<PhoneNumbers> getPhoneNumbers() {
		return this.phoneNumbers;
	}
	/**
	 * @param phoneNumbers the phoneNumbers to set
	 */
	public void setPhoneNumbers(ArrayList<PhoneNumbers> phoneNumbers) {
	this.phoneNumbers = phoneNumbers;}
	/**
	 * @return the emailAddresses
	 */
	public ArrayList<EmailAddresses> getEmailAddresses() {
		return this.emailAddresses;
	}
	/**
	 * @param emailAddresses the emailAddresses to set
	 */
	public void setEmailAddresses(ArrayList<EmailAddresses> emailAddresses) {
	this.emailAddresses = emailAddresses;}
	

}
