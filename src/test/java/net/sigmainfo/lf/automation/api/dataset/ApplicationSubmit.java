package net.sigmainfo.lf.automation.api.dataset;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

/**
 * Created by Rengarajan.M on 30-10-2017.
 */
@Component
public class ApplicationSubmit {
	private String BusinessName;
	private String AmountSeeking;
	private String UseOfFunds;
	private String AnnualBusinessRevenue;
	private String FirstName;
	private String LastName;
	private String Phone;
	private String Email;
	private String Password;
	private String DBA;
	private String BusinessAddress;
	private String City;
	private String State;
	private String ZipCode;
	private String BusinessPhone;
	private String BusinessStartDate;
	private String BusinessWebsite;
	private String LegalEntity;
	private String BusinessLocation;
	private String FederalSalesTaxID;
	private String Industry;
	private String LoanPriority;
	private String ClientIpAddress;
	private ArrayList<Owners> owners;
	/**
	 * @return the owners
	 */
	public ArrayList<Owners> getOwners() {
		return this.owners;
	}
	/**
	 * @param owners the owners to set
	 */
	public void setOwners(ArrayList<Owners> owners) {
	this.owners = owners;}
	/**
	 * @return the businessName
	 */
	public String getBusinessName() {
		return this.BusinessName;
	}
	/**
	 * @param businessName the businessName to set
	 */
	public void setBusinessName(String businessName) {
		this.BusinessName = businessName;
	}
	/**
	 * @return the amountSeeking
	 */
	public String getAmountSeeking() {
		return this.AmountSeeking;
	}
	/**
	 * @param amountSeeking the amountSeeking to set
	 */
	public void setAmountSeeking(String amountSeeking) {
		this.AmountSeeking = amountSeeking;
	}
	/**
	 * @return the useOfFunds
	 */
	public String getUseOfFunds() {
		return this.UseOfFunds;
	}
	/**
	 * @param useOfFunds the useOfFunds to set
	 */
	public void setUseOfFunds(String useOfFunds) {
		this.UseOfFunds = useOfFunds;
	}
	/**
	 * @return the annualBusinessRevenue
	 */
	public String getAnnualBusinessRevenue() {
		return this.AnnualBusinessRevenue;
	}
	/**
	 * @param annualBusinessRevenue the annualBusinessRevenue to set
	 */
	public void setAnnualBusinessRevenue(String annualBusinessRevenue) {
		this.AnnualBusinessRevenue = annualBusinessRevenue;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return this.FirstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.FirstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return this.LastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.LastName = lastName;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return this.Phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.Phone = phone;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return this.Email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.Email = email;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return this.Password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.Password = password;
	}
	/**
	 * @return the dBA
	 */
	public String getDBA() {
		return this.DBA;
	}
	/**
	 * @param dBA the dBA to set
	 */
	public void setDBA(String dBA) {
		this.DBA = dBA;
	}
	/**
	 * @return the businessAddress
	 */
	public String getBusinessAddress() {
		return this.BusinessAddress;
	}
	/**
	 * @param businessAddress the businessAddress to set
	 */
	public void setBusinessAddress(String businessAddress) {
		this.BusinessAddress = businessAddress;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return this.City;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.City = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return this.State;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.State = state;
	}
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return this.ZipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.ZipCode = zipCode;
	}
	/**
	 * @return the businessPhone
	 */
	public String getBusinessPhone() {
		return this.BusinessPhone;
	}
	/**
	 * @param businessPhone the businessPhone to set
	 */
	public void setBusinessPhone(String businessPhone) {
		this.BusinessPhone = businessPhone;
	}
	/**
	 * @return the businessStartDate
	 */
	public String getBusinessStartDate() {
		return this.BusinessStartDate;
	}
	/**
	 * @param businessStartDate the businessStartDate to set
	 */
	public void setBusinessStartDate(String businessStartDate) {
		this.BusinessStartDate = businessStartDate;
	}
	/**
	 * @return the businessWebsite
	 */
	public String getBusinessWebsite() {
		return this.BusinessWebsite;
	}
	/**
	 * @param businessWebsite the businessWebsite to set
	 */
	public void setBusinessWebsite(String businessWebsite) {
		this.BusinessWebsite = businessWebsite;
	}
	/**
	 * @return the legalEntity
	 */
	public String getLegalEntity() {
		return this.LegalEntity;
	}
	/**
	 * @param legalEntity the legalEntity to set
	 */
	public void setLegalEntity(String legalEntity) {
		this.LegalEntity = legalEntity;
	}
	/**
	 * @return the businessLocation
	 */
	public String getBusinessLocation() {
		return this.BusinessLocation;
	}
	/**
	 * @param businessLocation the businessLocation to set
	 */
	public void setBusinessLocation(String businessLocation) {
		this.BusinessLocation = businessLocation;
	}
	/**
	 * @return the federalSalesTaxID
	 */
	public String getFederalSalesTaxID() {
		return this.FederalSalesTaxID;
	}
	/**
	 * @param federalSalesTaxID the federalSalesTaxID to set
	 */
	public void setFederalSalesTaxID(String federalSalesTaxID) {
		this.FederalSalesTaxID = federalSalesTaxID;
	}
	/**
	 * @return the industry
	 */
	public String getIndustry() {
		return this.Industry;
	}
	/**
	 * @param industry the industry to set
	 */
	public void setIndustry(String industry) {
		this.Industry = industry;
	}
	/**
	 * @return the loanPriority
	 */
	public String getLoanPriority() {
		return this.LoanPriority;
	}
	/**
	 * @param loanPriority the loanPriority to set
	 */
	public void setLoanPriority(String loanPriority) {
		this.LoanPriority = loanPriority;
	}
	/**
	 * @return the clientIpAddress
	 */
	public String getClientIpAddress() {
		return this.ClientIpAddress;
	}
	/**
	 * @param clientIpAddress the clientIpAddress to set
	 */
	public void setClientIpAddress(String clientIpAddress) {
		this.ClientIpAddress = clientIpAddress;
	}

}
