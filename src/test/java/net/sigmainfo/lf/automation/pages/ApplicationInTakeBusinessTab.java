package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 06-11-2017. Test class :
 * ApplicationInTakeBusinessTab.java Description :This class holder page objects
 * for ApplicationIntake of Business Tab page of Borrower portal
 */
public class ApplicationInTakeBusinessTab {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By BusinessTabProgressBar = By.cssSelector(apiParam.BusinessTabProgressBar);

	By BusinessTabDBA = By.cssSelector(apiParam.BusinessTabDBA);

	By BusinessTabBusinessAddress = By.cssSelector(apiParam.BusinessTabBusinessAddress);

	By BusinessTabCity = By.cssSelector(apiParam.BusinessTabCity);

	By BusinessTabState = By.cssSelector(apiParam.BusinessTabState);

	By BusinessTabZipcode = By.cssSelector(apiParam.BusinessTabZipcode);

	By BusinessTabBusinessPhone = By.cssSelector(apiParam.BusinessTabBusinessPhone);

	By BusinessTabMonth = By.cssSelector(apiParam.BusinessTabMonth);

	By BusinessTabDay = By.cssSelector(apiParam.BusinessTabDay);

	By BusinessTabYear = By.cssSelector(apiParam.BusinessTabYear);

	By BusinessTabBusinessWebsite = By.cssSelector(apiParam.BusinessTabBusinessWebsite);

	By BusinessTabLegalEntity = By.cssSelector(apiParam.BusinessTabLegalEntity);

	By BusinessTabBusinessLoacation = By.cssSelector(apiParam.BusinessTabBusinessLoacation);

	By BusinessTabTaxId = By.cssSelector(apiParam.BusinessTabTaxId);

	By BusinessTabIndustry = By.cssSelector(apiParam.BusinessTabIndustry);

	By BusinessTabImportant = By.cssSelector(apiParam.BusinessTabImportant);

	By BusinessTabNext = By.cssSelector(apiParam.BusinessTabNext);

	By BusinessStartDateErrorMessage = By.cssSelector(apiParam.BusinessStartDateErrorMessage);

	// Validating Progress Bar
	public String ValidationProgressbar() {
		String msg = ABtests.getext(BusinessTabProgressBar);
		return msg;
	}

	// Validating Error message for Business Start Date
	public String ErrorMessageForBusinessDate() {
		String msg = ABtests.getext(BusinessStartDateErrorMessage);
		return msg;
	}

	// Set value in DBA
	public void setDBA(String BusinessName) throws FileNotFoundException, IOException {
		ABtests.InputBox(BusinessTabDBA, BusinessName);
	}

	// Set value in BusinessAddress
	public void setBusinessAddress(String businessAddress) throws FileNotFoundException, IOException {
		ABtests.InputBox(BusinessTabBusinessAddress, businessAddress);
	}

	// Set value in BusinessCity
	public void setBusinessCity(String businesscity) throws FileNotFoundException, IOException {
		ABtests.InputBox(BusinessTabCity, businesscity);
	}

	// Set value in BusinessZipcode
	public void setBusinessZipcode(String businessZipcode) throws FileNotFoundException, IOException {
		ABtests.InputBox(BusinessTabZipcode, businessZipcode);
	}

	// Set value in BusinessPhoneNo
	public void setBusinessPhoneNo(String businessPhoneNo) throws FileNotFoundException, IOException {
		ABtests.InputBox(BusinessTabBusinessPhone, businessPhoneNo);
	}

	// Set value in BusinessWebsite
	public void setBusinessWebsite(String businessWebsite) throws FileNotFoundException, IOException {
		ABtests.InputBox(BusinessTabBusinessWebsite, businessWebsite);
	}

	// Set value in BusinessTaxId
	public void setBusinessTaxId(String businessTaxId) throws FileNotFoundException, IOException {
		ABtests.InputBox(BusinessTabTaxId, businessTaxId);
	}

	// Set value in BusinessState
	public void setBusinessState(String businessState) throws FileNotFoundException, IOException {
		ABtests.DropDown(BusinessTabState, businessState);
	}

	// Set value in BusinessMonth
	public void setBusinessMonth(String businessMonth) throws FileNotFoundException, IOException, InterruptedException {
		ABtests.DropDown(BusinessTabMonth, businessMonth);
		Thread.sleep(1000);
	}

	// Set value in BusinessDay
	public void setBusinessDay(String businessDay) throws FileNotFoundException, IOException, InterruptedException {
		ABtests.DropDown(BusinessTabDay, businessDay);
		Thread.sleep(1000);
	}

	// Set value in BusinessYear
	public void setBusinessYear(String businessYear) throws FileNotFoundException, IOException, InterruptedException {
		ABtests.DropDown(BusinessTabYear, businessYear);
		Thread.sleep(1000);
	}

	// Set value in BusinessLegalEntity
	public void setBusinessLegalEntity(String businessLegalEntity) throws FileNotFoundException, IOException {
		ABtests.DropDown(BusinessTabLegalEntity, businessLegalEntity);
	}

	// Set value in BusinessLoacation
	public void setBusinessLoacation(String businessLoacation) throws FileNotFoundException, IOException {
		ABtests.DropDown(BusinessTabBusinessLoacation, businessLoacation);
	}

	// Set value in BusinessIndustry
	public void setBusinessIndustry(String businessIndustry) throws FileNotFoundException, IOException {
		ABtests.DropDown(BusinessTabIndustry, businessIndustry);
	}

	// Set value in BusinessImportant
	public void setBusinessImportant(String businessImportant) throws FileNotFoundException, IOException {
		ABtests.DropDown(BusinessTabImportant, businessImportant);
	}

	// Click on Next Button
	public void ClickNextButton() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.Button(BusinessTabNext);
		Thread.sleep(5000);
	}

	public void Submit_Business_Tab(String BusinessName, String businessImportant, String businessIndustry,
			String businessLoacation, String businessLegalEntity, String businessDOB, String businessState,
			String businessTaxId, String businessAddress, String businesscity, String Zipcode, String BusinessPhoneNo,
			String businessWebsite) throws FileNotFoundException, IOException, InterruptedException {

		Thread.sleep(5000);
		String DateOfBirth = businessDOB;
		String[] parts = DateOfBirth.split("/");
		System.out.println(DateOfBirth);
		String businessDay = ABtests.day(parts[0]);
		String businessYear = " " + parts[2] + " ";
		String businessMonth = ABtests.month(parts[1]);
		ABtests.MouseScrollUp();
		setDBA(BusinessName);
		setBusinessAddress(businessAddress);
		setBusinessCity(businesscity);
		setBusinessState(businessState);
		setBusinessZipcode(Zipcode);
		setBusinessPhoneNo(BusinessPhoneNo);
		ABtests.MouseScrollDown();
		setBusinessMonth(businessMonth);
		setBusinessDay(businessDay);
		setBusinessYear(businessYear);
		setBusinessWebsite(businessWebsite);
		setBusinessLegalEntity(businessLegalEntity);
		setBusinessLoacation(businessLoacation);
		setBusinessTaxId(businessTaxId);
		setBusinessIndustry(businessIndustry);
		setBusinessImportant(businessImportant);
		ClickNextButton();

	}
}
