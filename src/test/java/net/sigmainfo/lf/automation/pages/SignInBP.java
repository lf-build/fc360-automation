package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 16-10-2017. Test class : SignInBP.java
 * Description :This class holder page objects for SignIn page of Borrower
 * portal
 */
public class SignInBP {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By SignInPageEmailAddress = By.cssSelector(apiParam.SignInPageEmailAddress);

	By SignInPagePassword = By.cssSelector(apiParam.SignInPagePassword);

	By SignInPageLoginButton = By.cssSelector(apiParam.SignInPageLoginButton);

	By SignInPageForgotPasswordLink = By.cssSelector(apiParam.SignInPageForgotPasswordLink);

	By SignInErrorMessage = By.cssSelector(apiParam.SignInErrorMessage);

	// Set value in Email Address Field
	public void setEmailAddressField(String Email) throws FileNotFoundException, IOException {
		ABtests.InputBox(SignInPageEmailAddress, Email);
	}

	// Set value in PasswordField
	public void setPasswordField(String Password) throws FileNotFoundException, IOException {
		ABtests.InputBox(SignInPagePassword, Password);
	}

	// Click on Login button
	public void clickLoginButton() throws FileNotFoundException, IOException {
		ABtests.Button(SignInPageLoginButton);
	}

	// Click on Forgot password link
	public void clickForgotPasswordLink() throws FileNotFoundException, IOException {
		ABtests.Button(SignInPageForgotPasswordLink);
	}

	// Sign In Method
	public void SignInMethod(String Email, String password)
			throws InterruptedException, FileNotFoundException, IOException {
		setEmailAddressField(Email);
		setPasswordField(password);
		clickLoginButton();
		Thread.sleep(14000);
	}

	// Validating message displayed on SignIn page once Login failed
	public String ErrorMessageOnSignInPage() {
		String msg = ABtests.getext(SignInErrorMessage);
		return msg;
	}
}
