package net.sigmainfo.lf.automation.pages;

import static org.testng.Assert.assertEquals;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.openqa.selenium.By;
import com.jayway.restassured.response.Response;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.ApiFuncUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 14-10-2017. Test class : PlaidBankLinking.java
 * Description :This class holder page objects for Plaid
 */
public class PlaidBankLinking {

	public static AbstractTests ABtests = new AbstractTests();

	public static ApiFuncUtils apiFuncUtils = new ApiFuncUtils();

	@Autowired
	ApiParam apiParam;

	By IFrameIDForPlaid = By.id(apiParam.IFrameIDForPlaid);

	By PlaidSearch = By.xpath(apiParam.PlaidSearch);

	By SelectionOfPlaidBank = By.xpath(apiParam.SelectionOfPlaidBank);

	By PlaidUserName = By.cssSelector(apiParam.PlaidUserName);

	By PlaidPassword = By.cssSelector(apiParam.PlaidPassword);

	By SubmitPlaidCredential = By.xpath(apiParam.SubmitPlaidCredential);

	By PlaidReCaptcha = By.xpath(apiParam.PlaidReCaptcha);

	By SendType = By.xpath(apiParam.SendType);

	By ContinueButtonAfterSendType = By.xpath(apiParam.ContinueButtonAfterSendType);

	By PlaidCode = By.xpath(apiParam.PlaidCode);

	By SubmitPlaidBank = By.xpath(apiParam.SubmitPlaidBank);

	By ContinueButtonAfterAccountLinked = By.xpath(apiParam.ContinueButtonAfterAccountLinked);

	By ClosePlaid = By.xpath(apiParam.ClosePlaid);

	// Switch Plaid IFrame
	public void SwitchPlaidIFrame() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.SwitchToIframeID(apiParam.IFrameIDForPlaid);
		Thread.sleep(7000);
	}

	// Set value in PlaidSearch
	public void setPlaidSearch(String BankName) throws FileNotFoundException, IOException, Throwable {
		ABtests.InputBox(PlaidSearch, BankName);
		Thread.sleep(5000);
	}

	// select the Bank
	public void selectTheBank() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(SelectionOfPlaidBank);
		Thread.sleep(5000);
	}

	// Set value in PlaidUserName
	public void setPlaidUserName(String plaidUserName) throws FileNotFoundException, IOException {
		ABtests.InputBox(PlaidUserName, plaidUserName);
	}

	// Set value in PlaidPassword
	public void setPlaidPassword(String plaidPassword) throws FileNotFoundException, IOException {
		ABtests.InputBox(PlaidPassword, plaidPassword);
	}

	// SubmitPlaidCredential
	public void SubmitPlaidCredential() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(SubmitPlaidCredential);
		Thread.sleep(12000);
	}

	// SubmitReCaptcha
	public void SubmitReCaptcha() throws FileNotFoundException, IOException, Throwable {
		Thread.sleep(2000);
		ABtests.webdriver.switchTo().frame(ABtests.webdriver.findElement(By.cssSelector(
				"#plaid-link-container > div > div > div > div.App__content > div.Pane.RecaptchaPane > div > div > div.Recaptcha__recaptcha-container > div > div > iframe")));
		Thread.sleep(7000);
		WebElement input = ABtests.webdriver.findElement(PlaidReCaptcha);
		new Actions(ABtests.webdriver).moveToElement(input).click().perform();
		Thread.sleep(10000);
	}

	// selectSendType
	public void selectSendType() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(SendType);
		Thread.sleep(5000);
	}

	// ContinueButtonAfterSendType
	public void ContinueButtonAfterSendType() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(ContinueButtonAfterSendType);
		Thread.sleep(6000);
	}

	// Set value in PlaidCode
	public void setPlaidCode(String code) throws FileNotFoundException, IOException {
		ABtests.InputBox(PlaidCode, code);
	}

	// SubmitPlaidBank
	public void SubmitPlaidBank() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(SubmitPlaidBank);
		Thread.sleep(6000);
	}

	// ContinueButtonAfterAccountLinked
	public void ContinueButtonAfterAccountLinked() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(ContinueButtonAfterAccountLinked);
		Thread.sleep(7000);
	}

	// ReturnToDefaultContent
	public void ReturnToDefaultContent() throws FileNotFoundException, IOException, Throwable {
		ABtests.ReturnToDefaultContent();
		Thread.sleep(5000);
	}

	public void Submit_Plaid_Bank() throws Throwable, IOException, InterruptedException {
		SwitchPlaidIFrame();
		setPlaidSearch("chase");
		selectTheBank();
		setPlaidUserName("user_good");
		setPlaidPassword("pass_good");
		SubmitPlaidCredential();
		// SubmitReCaptcha();
		selectSendType();
		ContinueButtonAfterSendType();
		setPlaidCode("1234");
		SubmitPlaidBank();
		ContinueButtonAfterAccountLinked();
		ReturnToDefaultContent();
	}

	public void Submit_Plaid_Bank(String BankName) throws Throwable, IOException, InterruptedException {
		SwitchPlaidIFrame();
		setPlaidSearch(BankName);
		selectTheBank();
		setPlaidUserName("user_good");
		setPlaidPassword("pass_good");
		SubmitPlaidCredential();
		Thread.sleep(2000);
		ContinueButtonAfterAccountLinked();
		ReturnToDefaultContent();
		Thread.sleep(1500);
	}

	public void ClosePlaidpopup() throws Throwable, IOException, InterruptedException {
		SwitchPlaidIFrame();
		ABtests.Button(ClosePlaid);
		Thread.sleep(2000);
		ReturnToDefaultContent();
	}

	public void Submit_Plaid_Bank_Using_API() throws Throwable, IOException, InterruptedException {
		String serviceUrl = null;
		boolean isCorrectAuthorization = true;
		char t = '"';
		String body = "{\"PublicToken\" : " + t + apiParam.PlaidBankName + t
				+ ",\r\n \"BankSupportedProductType\" : \"connect\",\r\n \"EntityId\":" + t
				+ apiParam.LoanApplicationNumber + t + ",\r\n \"EntityType\":\"application\"\r\n}";
		apiParam.requestType = "/plaid?token=SzqwCSPHEzE5HzY1ftH8CjTMBnlKoHojCNx9YcDiFzW290KfTctHVg==";
		serviceUrl = apiParam.baseresturl + ":" + apiParam.PlaidBankPort + apiParam.requestType;
		Response responsecode = apiFuncUtils.triggerPostRequest(serviceUrl, isCorrectAuthorization,
				apiParam.PlaidBankAuthToken, body);
		assertEquals(responsecode.getStatusCode(), ABtests.SUCCESS_RESPONSE);
	}

}
