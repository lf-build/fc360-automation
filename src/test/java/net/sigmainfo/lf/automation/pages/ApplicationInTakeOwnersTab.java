package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 07-11-2017. Test class :
 * ApplicationInTakeOwnersTab.java Description :This class holder page objects
 * for ApplicationIntake of About You page of Borrower portal
 */
public class ApplicationInTakeOwnersTab {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By OwnersTabProgressBar = By.cssSelector(apiParam.OwnersTabProgressBar);

	By OwnersEmail = By.cssSelector(apiParam.OwnersEmail);

	By OwnersFirstName = By.cssSelector(apiParam.OwnersFirstName);

	By OwnersLastName = By.cssSelector(apiParam.OwnersLastName);

	By OwnersAddress = By.cssSelector(apiParam.OwnersAddress);

	By OwnersCity = By.cssSelector(apiParam.OwnersCity);

	By OwnersState = By.cssSelector(apiParam.OwnersState);

	By OwnersZipcode = By.cssSelector(apiParam.OwnersZipcode);

	By OwnersPhoneNo = By.cssSelector(apiParam.OwnersPhoneNo);

	By Ownersmobileno = By.cssSelector(apiParam.Ownersmobileno);

	By OwnersOwnership = By.cssSelector(apiParam.OwnersOwnership);

	By OwnersSSN = By.cssSelector(apiParam.OwnersSSN);

	By OwnersMonth = By.cssSelector(apiParam.OwnersMonth);

	By OwnersDay = By.cssSelector(apiParam.OwnersDay);

	By OwnersYear = By.cssSelector(apiParam.OwnersYear);

	By OwnersNext = By.cssSelector(apiParam.OwnersNext);

	By OwnerShipErrorMessage = By.cssSelector(apiParam.OwnerShipErrorMessage);

	By ErrormessageAge = By.cssSelector(apiParam.ErrormessageAge);

	// Validating Progress Bar
	public String ValidationProgressbar() {
		String msg = ABtests.getext(OwnersTabProgressBar);
		return msg;
	}

	// Validating Error message for OwnerShip
	public String ErrorMessageForOwnerShip() {
		String msg = ABtests.getext(OwnerShipErrorMessage);
		return msg;
	}

	// Validating Error message for Age
	public String ErrorMessageForAge() {
		String msg = ABtests.getext(ErrormessageAge);
		return msg;
	}

	// Set value in OwnerEmail
	public void setOwnerEmail(String ownerEmail) throws FileNotFoundException, IOException {
		ABtests.InputBox(OwnersEmail, ownerEmail);
	}

	// Set value in OwnerFirstName
	public void setOwnerFirstName(String ownerFirstName) throws FileNotFoundException, IOException {
		ABtests.InputBox(OwnersFirstName, ownerFirstName);
	}

	// Set value in OwnerLastName
	public void setOwnerLastName(String ownerLastName) throws FileNotFoundException, IOException {
		ABtests.InputBox(OwnersLastName, ownerLastName);
	}

	// Set value in OwnerAddress
	public void setOwnerAddress(String ownerAddress) throws FileNotFoundException, IOException {
		ABtests.InputBox(OwnersAddress, ownerAddress);
	}

	// Set value in OwnerCity
	public void setOwnerCity(String ownerCity) throws FileNotFoundException, IOException {
		ABtests.InputBox(OwnersCity, ownerCity);
	}

	// Set value in OwnerZipcode
	public void setOwnerZipcode(String ownerZipcode) throws FileNotFoundException, IOException {
		ABtests.InputBox(OwnersZipcode, ownerZipcode);
	}

	// Set value in OwnerPhoneNo
	public void setOwnerPhoneNo(String ownerPhoneNo) throws FileNotFoundException, IOException {
		ABtests.InputBox(OwnersPhoneNo, ownerPhoneNo);
	}

	// Set value in OwnerMobileNo
	public void setOwnerMobileNo(String ownerMobileNo) throws FileNotFoundException, IOException {
		ABtests.InputBox(Ownersmobileno, ownerMobileNo);
	}

	// Set value in OwnerOwnerShip
	public void setOwnerShip(String ownerShip) throws FileNotFoundException, IOException {
		ABtests.InputBox(OwnersOwnership, ownerShip);
	}

	// Set value in OwnerSSN
	public void setOwnerSSN(String ownerSSN) throws FileNotFoundException, IOException {
		ABtests.InputBox(OwnersSSN, ownerSSN);
	}

	// Set value in OwnerState
	public void setOwnerState(String ownerState) throws FileNotFoundException, IOException {
		ABtests.DropDown(OwnersState, ownerState);
	}

	// Set value in ownerMonth
	public void setOwnerMonth(String OwnerMonth) throws FileNotFoundException, IOException {
		ABtests.DropDown(OwnersMonth, OwnerMonth);
	}

	// Set value in ownerDay
	public void setownerDay(String ownerDay) throws FileNotFoundException, IOException {
		ABtests.DropDown(OwnersDay, ownerDay);
	}

	// Set value in ownerYear
	public void setownerYear(String ownerYear) throws FileNotFoundException, IOException {
		ABtests.DropDown(OwnersYear, ownerYear);
	}

	// Click on Next Button
	public void ClickNextButton() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.Button(OwnersNext);
		Thread.sleep(5000);
	}

	public void Submit_Owners_Tab(String Email, String FirstName, String LastName, String Address, String City,
			String DOB, String State, String Zipcode, String PhoneNo, String mobileno, String Ownership, String SSN)
			throws FileNotFoundException, IOException, InterruptedException {

		Thread.sleep(2000);
		String DateOfBirth = DOB;
		String[] parts = DateOfBirth.split("/");
		System.out.println(DateOfBirth);
		String Day = ABtests.day(parts[0]);
		String Year = " " + parts[2] + " ";
		String Month = ABtests.month(parts[1]);
		ABtests.MouseScrollUp();
		setOwnerEmail(Email);
		setOwnerFirstName(FirstName);
		setOwnerLastName(LastName);
		setOwnerAddress(Address);
		setOwnerCity(City);
		setOwnerState(State);
		setOwnerZipcode(Zipcode);
		setOwnerPhoneNo(PhoneNo);
		setOwnerMobileNo(mobileno);
		setOwnerShip(Ownership);
		setOwnerSSN(SSN);
		setOwnerMonth(Month);
		setownerDay(Day);
		setownerYear(Year);
		ClickNextButton();

	}

}
