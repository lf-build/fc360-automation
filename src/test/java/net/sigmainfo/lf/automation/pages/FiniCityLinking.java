package net.sigmainfo.lf.automation.pages;

import static org.testng.Assert.assertEquals;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.openqa.selenium.By;
import com.jayway.restassured.response.Response;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.ApiFuncUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 28-12-2017. Test class : FiniCityLinking.java
 * Description :This class holder page objects for Finicity
 */
public class FiniCityLinking {

	public static AbstractTests ABtests = new AbstractTests();

	public static ApiFuncUtils apiFuncUtils = new ApiFuncUtils();

	@Autowired
	ApiParam apiParam;

	By FinicityIframe = By.id(apiParam.FinicityIframe);

	By FinicityConsent = By.xpath(apiParam.FinicityConsent);

	By AddNewAccountButton = By.xpath(apiParam.AddNewAccountButton);

	By FinicitySearch = By.xpath(apiParam.FinicitySearch);

	By SelectFinBank = By.xpath(apiParam.SelectFinBank);

	By BankingUsername = By.xpath(apiParam.BankingUsername);

	By BankingPassword = By.xpath(apiParam.BankingPassword);

	By SubmitFinBankCredential = By.xpath(apiParam.SubmitFinBankCredential);

	By AddSelectedAccounts = By.xpath(apiParam.AddSelectedAccounts);

	By MakeDoneForAddingAccounts = By.xpath(apiParam.MakeDoneForAddingAccounts);

	// Switch Finicity IFrame
	public void SwitchFincityIFrame() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.SwitchToIframeID(apiParam.FinicityIframe);
		Thread.sleep(7000);
	}

	// Check FinicityConsent
	public void selectFinicityConsent() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(FinicityConsent);
		Thread.sleep(5000);
	}

	// click AccountButton
	public void clickAccountButton() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(AddNewAccountButton);
		Thread.sleep(5000);
	}

	// Search FinBank
	public void SearchFinBank(String BankName) throws FileNotFoundException, IOException, Throwable {
		ABtests.InputBox(FinicitySearch, BankName);
		Thread.sleep(5000);
	}

	// select FinBank
	public void SelectFinBank() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(SelectFinBank);
		Thread.sleep(5000);
	}

	// Set value in Finicity BankingUsername
	public void setBankingUsername(String bankingUsername) throws FileNotFoundException, IOException {
		ABtests.InputBox(BankingUsername, bankingUsername);
	}

	// Set value in Finicity BankingPassword
	public void setBankingPassword(String bankingPassword) throws FileNotFoundException, IOException {
		ABtests.InputBox(BankingPassword, bankingPassword);
	}

	// Submit FinBankCredential
	public void SubmitFinBankCredential() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(SubmitFinBankCredential);
		Thread.sleep(5000);
	}

	// Click on AddSelectedAccounts Button
	public void ClickOnAddSelectedAccounts() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(AddSelectedAccounts);
		Thread.sleep(5000);
	}

	// click on Done Link
	public void clickonDoneLink() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(MakeDoneForAddingAccounts);
		Thread.sleep(5000);
	}

	public void LinkingWithFinicity() throws FileNotFoundException, IOException, Throwable {
		SwitchFincityIFrame();
		selectFinicityConsent();
		clickAccountButton();
		SearchFinBank("FinBank");
		SelectFinBank();
		setBankingUsername("demo");
		setBankingPassword("go");
		SubmitFinBankCredential();
		ClickOnAddSelectedAccounts();
		clickonDoneLink();
	}
}
