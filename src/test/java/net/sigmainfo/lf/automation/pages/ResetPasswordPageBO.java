package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 05-10-2017. Test class :
 * ResetPasswordPageBO.java Description :This class holder page objects for
 * ResetPassword page of Backoffice
 */
public class ResetPasswordPageBO {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By UserName = By.id(apiParam.UserName);

	By SendRestPasswordButton = By.xpath(apiParam.SendRestPasswordButton);

	By MessageonResetPasswordPage = By.xpath(apiParam.MessageonResetPasswordPage);

	By SuccessMessage = By.xpath(apiParam.SuccessMessage);

	// Set value in Username

	public void setUsername(String username) throws FileNotFoundException, IOException {
		ABtests.InputBox(UserName, username);
	}

	// Click on Rest Button

	public void ClickRestButton() throws InterruptedException, FileNotFoundException, IOException {
		ABtests.Button(SendRestPasswordButton);
	}

	// Request for rest password
	public void RestPassword(String username) throws InterruptedException, FileNotFoundException, IOException {
		setUsername(username);
		ClickRestButton();
		Thread.sleep(57000);

	}

	// Text message on Reset password page which will used for asserting page
	// appearance
	public String MessageonResetPasswordPage() {
		String msg = ABtests.getext(MessageonResetPasswordPage);
		return msg;
	}

	// Validating message displayed on Reset password page once Request for
	// Reset password success
	public String SuccessMessage() {
		String msg = ABtests.getext(SuccessMessage);
		return msg;
	}
}
