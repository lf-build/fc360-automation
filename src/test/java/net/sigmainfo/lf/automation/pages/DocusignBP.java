package net.sigmainfo.lf.automation.pages;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 19-10-2017. Test class : Docusign.java
 * Description :This class holder page objects for Docusign of Borrower portal
 */
public class DocusignBP {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By DocusignAcceptCheckBox = By.cssSelector(apiParam.DocusignAcceptCheckBox);

	By DocusignContinue = By.cssSelector(apiParam.DocusignContinue);

	By DocusignStart = By.cssSelector(apiParam.DocusignStart);

	By DocusignAdoptSign = By.cssSelector(apiParam.DocusignAdoptSign);

	By DocusignFinish = By.cssSelector(apiParam.DocusignFinish);

	By DocuSignLabel = By.cssSelector(apiParam.DocuSignLabel);

	// Get Button id
	public void GetButtonID() throws Exception {
		apiParam.DocusignSign = "#" + ABtests.getElementByAttribute(DocuSignLabel, "for");
		Thread.sleep(1000);
	}

	// Click on Accept Check Box
	public void ClickAcceptBox() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.SwitchToIframeID("docuSignIframe");
		Thread.sleep(7000);
		ABtests.Button(DocusignAcceptCheckBox);
	}

	// Click on Continue Button
	public void ClickContinueButton() throws FileNotFoundException, IOException {
		ABtests.Button(DocusignContinue);
	}

	// Click on Start Button
	public void ClickStartButton() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.Button(DocusignStart);
	}

	// Click on Signature Button
	public void ClickSignatureButton() throws FileNotFoundException, IOException, AWTException {
		By DocusignSign = By.cssSelector(apiParam.DocusignSign);
		ABtests.Button(DocusignSign);
	}

	// Click on AdoptSign
	public void ClickAdoptSignButton() throws FileNotFoundException, IOException {
		ABtests.Button(DocusignAdoptSign);
	}

	// Click on Finish Button
	public void ClickFinishButton() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.Button(DocusignFinish);
		Thread.sleep(2000);
		ABtests.ReturnToDefaultContent();
	}

	// Submit DocuSign
	public void Submit_DocuSign() throws FileNotFoundException, IOException, Throwable {
		ClickAcceptBox();
		ClickContinueButton();
		Thread.sleep(3000);
		ClickStartButton();
		Thread.sleep(3000);
		GetButtonID();
		ClickSignatureButton();
		Thread.sleep(4000);
		ClickAdoptSignButton();
		Thread.sleep(5000);
		ClickFinishButton();
		Thread.sleep(9000);
	}

}
