package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 04-10-2017. Test class : SearchPageBO.java
 * Description :This class holder page objects for Search page of Backoffice
 */
public class SearchPageBO {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By BrandingLogoSearchPage = By.xpath(apiParam.BrandingLogoSearchPage);

	By ProfileLink = By.xpath(apiParam.ProfileLink);

	By LogOutLink = By.xpath(apiParam.LogOutLink);

	By ApplicationNumber = By.name(apiParam.ApplicationNumber);

	By ProductID = By.name(apiParam.ProductID);

	By FirstName = By.name(apiParam.FirstName);

	By LastName = By.name(apiParam.LastName);

	By BusinessEmail = By.name(apiParam.BusinessEmail);

	By BusinessName = By.name(apiParam.BusinessName);

	By Status = By.name(apiParam.Status);

	By ApplicationDate = By.name(apiParam.ApplicationDate);

	By ExpiryDate = By.name(apiParam.ExpiryDate);

	By MobileNumber = By.name(apiParam.MobileNumber);

	By SearchButton = By.xpath(apiParam.SearchButton);

	By SearchResult = By.xpath(apiParam.SearchResult);

	By MessageOnSearchPage = By.xpath(apiParam.MessageOnSearchPage);
	
	By Workflow = By.name(apiParam.Workflow);

	// FC-360 Logo
	public String LogoURL() {
		String img = ABtests.getElementByAttribute(BrandingLogoSearchPage, "src");
		return img;
	}

	// Logout from BackOffice
	public void logout() throws InterruptedException, FileNotFoundException, IOException {
		ABtests.Button(ProfileLink);
		Thread.sleep(2000);
		ABtests.Button(LogOutLink);
		Thread.sleep(4000);
	}

	// Set value in ApplicationNumber
	public void setApplicationNo(String applicationNumber) throws FileNotFoundException, IOException {
		ABtests.InputBox(ApplicationNumber, applicationNumber);
	}

	// Set value in FirstName
	public void setFirstName(String firstName) throws FileNotFoundException, IOException {
		ABtests.InputBox(FirstName, firstName);
	}

	// Set value in LastName
	public void setlastName(String lastName) throws FileNotFoundException, IOException {
		ABtests.InputBox(LastName, lastName);
	}

	// Set value in Business Email
	public void setbusinessEmail(String businessEmail) throws FileNotFoundException, IOException {
		ABtests.InputBox(BusinessEmail, businessEmail);
	}

	// Set value in Business name
	public void setbusinessName(String businessName) throws FileNotFoundException, IOException {
		ABtests.InputBox(BusinessName, businessName);
	}

	// Set value in Mobile number
	public void setmobileNumber(String mobileNumber) throws FileNotFoundException, IOException {
		ABtests.InputBox(MobileNumber, mobileNumber);
	}

	// Select value in ProductId
	public void setproductID(String productID) throws FileNotFoundException, IOException {
		ABtests.InputBox(ProductID, productID);
		ABtests.webdriver.findElement(ProductID).sendKeys(Keys.TAB);
	}

	// Select value in status
	public void setstatus(String status) throws FileNotFoundException, IOException {
		ABtests.InputBox(Status, status);
	}

	// Select value in Application date
	public void setapplicationDate(String applicationDate) throws FileNotFoundException, IOException {
		ABtests.InputBox(ApplicationDate, applicationDate);
	}

	// Select value in Expiry date
	public void setexpiryDate(String expiryDate) throws FileNotFoundException, IOException {
		ABtests.InputBox(ExpiryDate, expiryDate);
	}
	
	// Select value in WorkFlow 
	public void setworkFlow(String workFlow) throws FileNotFoundException, IOException {
		ABtests.InputBox(Workflow, workFlow);
	}

	// Click on search button
	public void ClickOnSearchButton() throws InterruptedException, FileNotFoundException, IOException {
		ABtests.Button(SearchButton);
		Thread.sleep(3000);

	}

	// Validating Search result
	public String ValidateSearchResult() {
		String msg = ABtests.getext(SearchResult);
		return msg;
	}

	// Validate Message on search page when no match found
	public String ValidateMessageNoMatch() {
		String msg = ABtests.getext(MessageOnSearchPage);
		return msg;
	}
}
