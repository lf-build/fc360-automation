package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 07-11-2017. Test class :
 * ApplicationInTakeFinancialReviewTab.java Description :This class holder page
 * objects for ApplicationIntake of FinancialReview page of Borrower portal
 */
public class ApplicationInTakeFinancialReviewTab {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By FinancialTabProgressBar = By.cssSelector(apiParam.FinancialTabProgressBar);

	By FinalFinish = By.cssSelector(apiParam.FinalFinish);

	By LinkBankAccount = By.cssSelector(apiParam.LinkBankAccount);

	By SuccessMessageAfterBankLinked = By.cssSelector(apiParam.SuccessMessageAfterBankLinked);

	By FinicityBankButton = By.cssSelector(apiParam.FinicityBankButton);

	// Click on Finsh Button
	public void ClickFinsihButton() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.Button(FinalFinish);
		Thread.sleep(10000);
	}

	// Validating Progress Bar
	public String ValidationProgressbar() {
		String msg = ABtests.getext(FinancialTabProgressBar);
		return msg;
	}

	// Click on Link Bank Account
	public void LinkBankAccount() throws FileNotFoundException, IOException, Throwable {
		ABtests.Button(LinkBankAccount);
		Thread.sleep(10000);
	}

	// Validating SuccessMessageAfterBankLinked
	public String SuccessMessageAfterBankLinked() {
		String msg = ABtests.getext(SuccessMessageAfterBankLinked);
		return msg;
	}

	// Click on Finicity Button
	public void ClickFinicityButton() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.Button(FinicityBankButton);
		Thread.sleep(10000);
	}
}
