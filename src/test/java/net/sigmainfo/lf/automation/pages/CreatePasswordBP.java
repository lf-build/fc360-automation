package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 06-11-2017. Test class : CreatePasswordBP.java
 * Description :This class holder page objects for CreatePassword page of
 * Borrower portal
 */
public class CreatePasswordBP {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By MessageTitleOnCreatePasswordPage = By.cssSelector(apiParam.MessageTitleOnCreatePasswordPage);

	By BPCreateNewPassword = By.cssSelector(apiParam.BPCreateNewPassword);

	By BPCreateConfirmPassword = By.cssSelector(apiParam.BPCreateConfirmPassword);

	By BPCreatePasswordButton = By.cssSelector(apiParam.BPCreatePasswordButton);

	By MessageAfterCraetePasswordRequest = By.cssSelector(apiParam.MessageAfterCraetePasswordRequest);

	By SuccessMessageAftersuccessCreatePasswordRequest = By
			.cssSelector(apiParam.SuccessMessageAftersuccessCreatePasswordRequest);

	// Set value in NewPasswordField

	public void setNewPasswordField(String NewPassword) throws FileNotFoundException, IOException {
		ABtests.InputBox(BPCreateNewPassword, NewPassword);
	}

	// Set value in ConfirmPasswordField

	public void setConfirmPasswordField(String ConfirmPassword) throws FileNotFoundException, IOException {
		ABtests.InputBox(BPCreateConfirmPassword, ConfirmPassword);
	}

	// Click on Rest button

	public void clickRestButton() throws FileNotFoundException, IOException {
		ABtests.Button(BPCreatePasswordButton);
	}

	// Create a Password for the User
	public void CreatePaasword(String NewPassword, String ConfirmPassword)
			throws InterruptedException, FileNotFoundException, IOException {
		setNewPasswordField(NewPassword);
		setConfirmPasswordField(ConfirmPassword);
		clickRestButton();
		Thread.sleep(10000);

	}

	// Text message on Create password page which will used for asserting page
	// appearance
	public String TextOnRestPasswordPage() {
		String msg = ABtests.getext(MessageTitleOnCreatePasswordPage);
		return msg;
	}

	// Validating Error message displayed on Create password page once Create
	// password
	// successfully
	public String ValidationMessageOnPage() {
		String msg = ABtests.getext(MessageAfterCraetePasswordRequest);
		return msg;
	}

	// Validating Success message displayed on Create password page once Create
	// password successfully
	public String SuccessMessageOnPage() {
		String msg = ABtests.getext(SuccessMessageAftersuccessCreatePasswordRequest);
		return msg;
	}
}
