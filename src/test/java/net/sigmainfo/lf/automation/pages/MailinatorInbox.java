package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 23-10-2017. Test class : MailinatorInbox.java
 * Description :This class holder page objects for MailinatorInbox
 */
public class MailinatorInbox {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By EmailtextField = By.xpath(apiParam.EmailtextField);

	By Go = By.xpath(apiParam.Go);

	By TimePeriod = By.cssSelector(apiParam.TimePeriod);

	By RestPasswordSubject = By.cssSelector(apiParam.RestPasswordSubject);

	By RestPasswordlink = By.xpath(apiParam.RestPasswordlink);

	public String URL = apiParam.InboxURL;

	// Set value in EmailtextField
	public void setEmailtextField(String emailtextField) throws FileNotFoundException, IOException {
		ABtests.InputBox(EmailtextField, emailtextField);
	}

	// Click on Go Button
	public void ClickGoButton() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.Button(Go);
		Thread.sleep(6000);
	}

	// Get Mailinator URL
	public void GetURL() throws InterruptedException {
		ABtests.webdriver.get(URL);
		Thread.sleep(5000);
	}

	// Navigate to custom Mailinator Inbox
	public void NavigateToInboxWithEmailID(String emailtextField)
			throws FileNotFoundException, IOException, InterruptedException {
		GetURL();
		setEmailtextField(emailtextField);
		ClickGoButton();
	}

	// Text message on Milinator Inbox to check Time stamp as moments ago
	public String TimeStampAt(int Id) throws FileNotFoundException, IOException {
		String msg = ABtests.getext(TimePeriod, Id);
		return msg;
	}

	// Click on RestPassword Subject Mail
	public void ClickRestPasswordMailAt(int Id) throws FileNotFoundException, IOException {
		ABtests.Button(RestPasswordSubject, Id);
	}

	// Click on RestPassword Link
	public void ClickRestPasswordLink() throws FileNotFoundException, IOException {
		ABtests.Button(RestPasswordlink);
	}
}
