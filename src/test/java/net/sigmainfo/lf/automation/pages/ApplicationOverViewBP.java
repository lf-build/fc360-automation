package net.sigmainfo.lf.automation.pages;

import static org.testng.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import com.jayway.restassured.response.Response;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.ApiFuncUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 19-10-2017. Test class :
 * ApplicationOverViewBP.java Description :This class holder page objects for
 * ApplicationOverView page of Borrower portal
 */

public class ApplicationOverViewBP {
	public static AbstractTests ABtests = new AbstractTests();

	public static ApiFuncUtils apiFuncUtils = new ApiFuncUtils();

	@Autowired
	ApiParam apiParam;

	By ValidationMessageOnTodo = By.cssSelector(apiParam.ValidationMessageOnTodo);

	By ProfilePage = By.cssSelector(apiParam.ProfilePage);

	By ToDoPage = By.cssSelector(apiParam.ToDoPage);

	By OverViewPage = By.cssSelector(apiParam.OverViewPage);

	By ChangePasswordPage = By.cssSelector(apiParam.ChangePasswordPage);

	By BorrowerName = By.cssSelector(apiParam.BorrowerName);

	By ChangePasswordLink = By.cssSelector(apiParam.ChangePasswordLink);

	By BPLogout = By.cssSelector(apiParam.BPLogout);

	By SuccessMessageOnChangePasswordPage = By.cssSelector(apiParam.SuccessMessageOnChangePasswordPage);

	By BPBrandingLogo = By.xpath(apiParam.BPBrandingLogo);

	By ChangePasswordPageRestButton = By.cssSelector(apiParam.ChangePasswordPageRestButton);

	By TodoVlidationMessage = By.cssSelector(apiParam.TodoVlidationMessage);

	By TodoDeclineMessage = By.cssSelector(apiParam.TodoDeclineMessage);

	By TodoApplicationNo = By.cssSelector(apiParam.TodoApplicationNo);

	By LinkBankAccount = By.cssSelector(apiParam.LinkBankAccount);

	By TodoBankSuccessMsg = By.cssSelector(apiParam.TodoBankSuccessMsg);

	// Link Bank Account
	public void ClickLinkBankAccount() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.Button(LinkBankAccount);
		Thread.sleep(2000);
	}

	// FC-360 Logo
	public String LogoURL() {
		String img = ABtests.getElementByAttribute(BPBrandingLogo, "src");
		return img;
	}

	// TodoVlidationMessage
	public String TodoVlidationMessage() {
		String msg = ABtests.getext(TodoVlidationMessage);
		return msg;
	}

	// TodoDeclineMessage
	public String TodoDeclineMessage() {
		String msg = ABtests.getext(TodoDeclineMessage);
		return msg;
	}

	// Get ApplicationNo
	public String GetApplicationNo() {
		String msg = ABtests.getext(TodoApplicationNo);
		return msg;
	}

	// View Profile page
	public void ViewProfilePage() throws FileNotFoundException, IOException {
		ABtests.Button(ProfilePage);
	}

	// View ToDo page
	public void ViewToDoPage() throws FileNotFoundException, IOException {
		ABtests.Button(ToDoPage);
	}

	// View OverViewPage
	public void ViewOverViewPage() throws FileNotFoundException, IOException {
		ABtests.Button(OverViewPage);
	}

	// View ChangePasswordPage
	public void ViewChangePasswordPage() throws FileNotFoundException, IOException {
		ABtests.Button(ChangePasswordPage);
	}

	// Get Borrower Name from Page
	public String GetBorrowerName() {
		String msg = ABtests.getext(BorrowerName);
		return msg;
	}

	// Click on Borrower Name
	public void ClickBorrowerName() throws FileNotFoundException, IOException {
		ABtests.Button(BorrowerName);
	}

	// Click on Change Password Link
	public void ClickChangePasswordLink() throws FileNotFoundException, IOException {
		ABtests.Button(ChangePasswordLink);
	}

	// Click on Logout
	public void ClickLogOutLink() throws FileNotFoundException, IOException {
		ABtests.Button(BPLogout);
	}

	// Validating message displayed on change password page once change password
	// successfully
	public String SuccessMessageOnFotgotPasswordPage() {
		String msg = ABtests.getext(SuccessMessageOnChangePasswordPage);
		return msg;
	}

	// Validating message displayed on ToDo page After Bank Linked
	// successfully
	public String SuccessMsgBankLinked() {
		String msg = ABtests.getext(TodoBankSuccessMsg);
		return msg;
	}

	// Validating presence of Todo Page
	public String ValidationPresenceOfTodo() {
		String msg = ABtests.getext(ValidationMessageOnTodo);
		return msg;
	}

	// Logout from Borrower portal
	public void logout() throws InterruptedException, FileNotFoundException, IOException {
		ClickBorrowerName();
		Thread.sleep(2000);
		ClickLogOutLink();
		Thread.sleep(7000);
	}

	// Click on Rest Password button
	public void ClickRestPasswordButton() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.Button(ChangePasswordPageRestButton);
		Thread.sleep(35000);
	}

}
