package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 16-10-2017. Test class : FotgotPasswordBP.java
 * Description :This class holder page objects for FotgotPassword page of
 * Borrower portal
 */

public class FotgotPasswordBP {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By ForgotPasswordPageEmailAddress = By.cssSelector(apiParam.ForgotPasswordPageEmailAddress);

	By FotgotPasswordSubmitButton = By.cssSelector(apiParam.FotgotPasswordSubmitButton);

	By SuccessMessageOnForgotPasswordPage = By.cssSelector(apiParam.SuccessMessageOnForgotPasswordPage);
	
	By ForgotPasswordTitle = By.cssSelector(apiParam.ForgotPasswordTitle);

	// Set value in Email Address Field
	public void setEmailAddressField(String Email) throws FileNotFoundException, IOException {
		ABtests.InputBox(ForgotPasswordPageEmailAddress, Email);
	}

	// Click on Submit button
	public void clickSubmitButton() throws FileNotFoundException, IOException {
		ABtests.Button(FotgotPasswordSubmitButton);
	}

	// Forgot password Method
	public void RestPassword(String Email) throws InterruptedException, FileNotFoundException, IOException {
		setEmailAddressField(Email);
		clickSubmitButton();
		Thread.sleep(45000);

	}
	
	// Text message on Reset password page which will used for asserting page
	// appearance
	public String MessageTitle() {
		String msg = ABtests.getext(ForgotPasswordTitle);
		return msg;
	}

	// Validating message displayed on Fotgot password page once forgot password
	// successfully
	public String SuccessMessageOnFotgotPasswordPage() {
		String msg = ABtests.getext(SuccessMessageOnForgotPasswordPage);
		return msg;
	}

}
