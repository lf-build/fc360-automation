package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 06-10-2017. Test class : CreatePasswordBO.java
 * Description :This class holder page objects for CreatePassword page of
 * Backoffice
 */
public class CreatePasswordBO {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By NewPasswordField = By.id(apiParam.NewPasswordField);

	By ConfirmPasswordField = By.id(apiParam.ConfirmPasswordField);

	By RestButton = By.cssSelector(apiParam.RestButton);

	By TextOnRestPasswordPage = By.xpath(apiParam.TextOnRestPasswordPage);

	By SuccessMessageAfterRest = By.xpath(apiParam.SuccessMessageAfterRest);

	By ReLogin = By.xpath(apiParam.ReLogin);

	By ErrorMessageAfterRest = By.xpath(apiParam.ErrorMessageAfterRest);

	By verifyPasswordErrorMessage = By.xpath(apiParam.verifyPasswordErrorMessage);
	// Set value in NewPasswordField

	public void setNewPasswordField(String NewPassword) throws FileNotFoundException, IOException {
		ABtests.InputBox(NewPasswordField, NewPassword);
	}

	// Set value in ConfirmPasswordField

	public void setConfirmPasswordField(String ConfirmPassword) throws FileNotFoundException, IOException {
		ABtests.InputBox(ConfirmPasswordField, ConfirmPassword);
	}

	// Click on Rest button

	public void clickRestButton() throws FileNotFoundException, IOException {
		ABtests.Button(RestButton);
	}

	// Create a Password for the User
	public void CreatePaasword(String NewPassword, String ConfirmPassword) throws InterruptedException, FileNotFoundException, IOException {
		setNewPasswordField(NewPassword);
		setConfirmPasswordField(ConfirmPassword);
		clickRestButton();
		Thread.sleep(8000);

	}

	// Text message on Create password page which will used for asserting page
	// appearance
	public String TextOnRestPasswordPage() {
		String msg = ABtests.getext(TextOnRestPasswordPage);
		return msg;
	}

	// Validating message displayed on Create password page once Create password
	// successfully
	public String ValidationMessageOnPage() {
		String msg = ABtests.getext(SuccessMessageAfterRest);
		return msg;
	}

	// Re Login button displayed on Create password page once Create password
	// successfully
	public void ReLogin() throws Throwable {
		ABtests.Button(ReLogin);
		Thread.sleep(4000);
	}

	// Validating message displayed on Create password page once create password
	// functionality failed
	public String ErrorMessageAfterReset() {
		String msg = ABtests.getext(ErrorMessageAfterRest);
		return msg;
	}

	// Validating message displayed on Create password page when mismatch b/t
	// new password and confirm password field
	public String verifyPasswordErrorMessage() {
		String msg = ABtests.getext(verifyPasswordErrorMessage);
		return msg;
	}
}
