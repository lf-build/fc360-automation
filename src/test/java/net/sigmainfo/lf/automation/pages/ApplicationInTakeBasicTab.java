package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 19-10-2017. Test class :
 * ApplicationIntakeBasicTabBP.java Description :This class holder page objects
 * for ApplicationIntake of BasicTab page of Borrower portal
 */

public class ApplicationInTakeBasicTab {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By BPProgressBar = By.cssSelector(apiParam.BPProgressBar);

	By BPBusinessName = By.cssSelector(apiParam.BPBusinessName);

	By BPAmountSeeking = By.cssSelector(apiParam.BPAmountSeeking);

	By BPUseOfFunds = By.cssSelector(apiParam.BPUseOfFunds);

	By BPAnnualBusinessRevenue = By.cssSelector(apiParam.BPAnnualBusinessRevenue);

	By BPFirstName = By.cssSelector(apiParam.BPFirstName);

	By BPLastName = By.cssSelector(apiParam.BPLastName);

	By BPPhone = By.cssSelector(apiParam.BPPhone);

	By BPEmail = By.cssSelector(apiParam.BPEmail);

	By BPPassword = By.cssSelector(apiParam.BPPaasword);

	By BPNextButtonInBasicTab = By.cssSelector(apiParam.BPNextButtonInBasicTab);

	By BPBrandingLogo = By.xpath(apiParam.BPBrandingLogo);

	By BPSignButton = By.xpath(apiParam.BPSignButton);

	By BPDuplicateCheckMessage = By.xpath(apiParam.BPDuplicateCheckMessage);

	By BPConfirmPassword = By.cssSelector(apiParam.BPConfirmPassword);

	// Validating Progress Bar
	public String ValidationProgressbar() {
		String msg = ABtests.getext(BPProgressBar);
		return msg;
	}

	// Validating DuplicateCheckMessage
	public String ValidateDuplicateCheckMessage() {
		String msg = ABtests.getext(BPDuplicateCheckMessage);
		return msg;
	}

	// FC-360 Logo
	public String LogoURL() {
		String img = ABtests.getElementByAttribute(BPBrandingLogo, "src");
		return img;
	}

	// Set value in BusinessName
	public void setBusinessName(String BusinessName) throws FileNotFoundException, IOException {
		ABtests.InputBox(BPBusinessName, BusinessName);
	}

	// Set value in AmountSeeking
	public void setAmountSeeking(String AmountSeeking) throws FileNotFoundException, IOException {
		ABtests.InputBox(BPAmountSeeking, AmountSeeking);
	}

	// Select value in UseOfFunds
	public void setUseOfFunds(String UseOfFunds) throws FileNotFoundException, IOException {
		ABtests.DropDown(BPUseOfFunds, UseOfFunds);
	}

	// Set value in AnnualBusinessRevenue
	public void setAnnualBusinessRevenue(String AnnualBusinessRevenue) throws FileNotFoundException, IOException {
		ABtests.InputBox(BPAnnualBusinessRevenue, AnnualBusinessRevenue);
	}

	// Set value in FirstName
	public void setFirstName(String FirstName) throws FileNotFoundException, IOException {
		ABtests.InputBox(BPFirstName, FirstName);
	}

	// Set value in LastName
	public void setLastName(String LastName) throws FileNotFoundException, IOException {
		ABtests.InputBox(BPLastName, LastName);
	}

	// Set value in Phone
	public void setPhone(String Phone) throws FileNotFoundException, IOException {
		ABtests.InputBox(BPPhone, Phone);
	}

	// Set value in Email
	public void setEmail(String Email) throws FileNotFoundException, IOException {
		ABtests.InputBox(BPEmail, Email);
	}

	// Set value in Password
	public void setPassword(String Password) throws FileNotFoundException, IOException {
		ABtests.InputBox(BPPassword, Password);
	}

	// Set value in ConfirmPassword
	public void setConfirmPassword(String Password) throws FileNotFoundException, IOException {
		ABtests.InputBox(BPConfirmPassword, Password);
	}

	// Click on Next Button
	public void ClickNextButton() throws FileNotFoundException, IOException {
		ABtests.Button(BPNextButtonInBasicTab);
	}

	// Click on Sign In Button
	public void ClickSignInButton() throws FileNotFoundException, IOException, InterruptedException {
		ABtests.Button(BPSignButton);
		Thread.sleep(5000);
	}

	// Submit Basic Tab
	public void Submit_Basic_Tab(String BusinessName, String AmountSeeking, String UseOfFunds,
			String AnnualBusinessRevenue, String FirstName, String LastName, String Phone, String Email,
			String Password) throws FileNotFoundException, IOException, Throwable {
		Thread.sleep(2000);
		setBusinessName(BusinessName);
		setAmountSeeking(AmountSeeking);
		setUseOfFunds(UseOfFunds);
		setAnnualBusinessRevenue(AnnualBusinessRevenue);
		setFirstName(FirstName);
		setLastName(LastName);
		setPhone(Phone);
		setEmail(Email);
		setPassword(Password);
		setConfirmPassword(Password);
		ClickNextButton();

	}
}
