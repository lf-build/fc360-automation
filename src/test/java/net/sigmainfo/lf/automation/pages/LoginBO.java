package net.sigmainfo.lf.automation.pages;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by : Rengarajan.m on 03-10-2017. Test class : LoginBO.java
 * Description :This class holder page objects for Login page of Backoffice
 */
public class LoginBO {

	public static AbstractTests ABtests = new AbstractTests();

	@Autowired
	ApiParam apiParam;

	By UserName = By.id(apiParam.UserName);

	By Password = By.id(apiParam.Password);

	By ConsentCheckBox = By.id(apiParam.ConsentCheckBox);

	By SignButton = By.id(apiParam.SignButton);

	By BrandingLogoLoginPage = By.xpath(apiParam.BrandingLogoLoginPage);

	By ErrorMessageonHomePage = By.xpath(apiParam.ErrorMessageonHomePage);

	By ConsentMessageonHomePage = By.xpath(apiParam.ConsentMessageonHomePage);

	By Forgotpasswordlink = By.xpath(apiParam.Forgotpasswordlink);

	// Set value in Username

	public void setUsername(String username) throws FileNotFoundException, IOException {
		ABtests.InputBox(UserName, username);
	}

	// Set value in password

	public void setPassword(String password) throws FileNotFoundException, IOException {
		ABtests.InputBox(Password, password);
	}

	// Click on consent Check Box

	public void clickConsentCheckBox() throws FileNotFoundException, IOException {
		ABtests.Button(ConsentCheckBox);
	}

	// Click on login Button

	public void ClickOnSignIn() throws InterruptedException, FileNotFoundException, IOException {
		ABtests.Button(SignButton);
		Thread.sleep(12000);
	}

	// FC-360 Logo
	public String LogoURL() {
		String img = ABtests.getElementByAttribute(BrandingLogoLoginPage, "src");
		return img;
	}

	// Validating message displayed on Login page once Login failed
	public String ErrorMessage() {
		String msg = ABtests.getext(ErrorMessageonHomePage);
		return msg;
	}

	// Click on ForgotPassword link

	public void clickForGotPassworsdLink() throws FileNotFoundException, IOException {
		ABtests.Button(Forgotpasswordlink);
	}

	// Validating message displayed on Login page once Login without accepting
	// consent
	public String ConsentMessageonHomePage() {
		String msg = ABtests.getext(ConsentMessageonHomePage);
		return msg;
	}

	// Login Method
	public void LoginMethod(String username, String password) throws InterruptedException, FileNotFoundException, IOException {
		setUsername(username);
		setPassword(password);
		clickConsentCheckBox();
		ClickOnSignIn();
		Thread.sleep(12000);

	}
}
