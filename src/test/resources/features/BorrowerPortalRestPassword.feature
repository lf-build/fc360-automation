Feature: RestPaasword Functionality on Borrower portal
@BorrowerPortalRestPassword
  Scenario: Validate whether Reset Password page displayed or not
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Click on Forgot Link
     Then Validate Message "Forgot Password" on Rest Password page
  @BorrowerPortalRestPassword
  Scenario: Validate Reset Password by providing valid UserName
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Click on Forgot Link
     Then Validate Message "Forgot Password" on Rest Password page
     Then provide "Email" "renga_1510211974338@mailinator.com" and Click on Send Rest link button
     Then Validate Success Message "Email sent successfully" on Rest Password page
  @BorrowerPortalRestPassword
  Scenario: Validate Reset Password by providing Invalid UserName
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Click on Forgot Link
     Then Validate Message "Forgot Password" on Rest Password page
     Then provide "Email" "renga_000001@manator.com" and Click on Send Rest link button
     Then Validate Success Message "Email sent successfully" on Rest Password page
  @BorrowerPortalRestPassword    
  Scenario: Validate Whether Reset Password mail triggered or not
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Click on Forgot Link
     Then Validate Message "Forgot Password" on Rest Password page
     Then provide "Email" "renga_1510211974338@mailinator.com" and Click on Send Rest link button
     Then Validate Success Message "Email sent successfully" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "renga_1510211974338@mailinator.com"
  @BorrowerPortalRestPassword
  Scenario: Verify Rest Password Page
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Click on Forgot Link
     Then Validate Message "Forgot Password" on Rest Password page
     Then provide "Email" "renga_1510211974338@mailinator.com" and Click on Send Rest link button
     Then Validate Success Message "Email sent successfully" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "renga_1510211974338@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
  @BorrowerPortalRestPassword
  Scenario: Rest the password by providing NewPassword and Confirm Password field with same value
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Click on Forgot Link
     Then Validate Message "Forgot Password" on Rest Password page
     Then provide "Email" "renga_1510211974338@mailinator.com" and Click on Send Rest link button
     Then Validate Success Message "Email sent successfully" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "renga_1510211974338@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
     Then Rest by Providing New Password and Confirm Password field with "same" value
     Then Verify Success Message "Password updated successfully" on Page
  @BorrowerPortalRestPassword
  Scenario: Login with New or Changed Password
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Click on Forgot Link
     Then Validate Message "Forgot Password" on Rest Password page
     Then provide "Email" "renga_1510211974338@mailinator.com" and Click on Send Rest link button
     Then Validate Success Message "Email sent successfully" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "renga_1510211974338@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
     Then Rest by Providing New Password and Confirm Password field with "same" value
     Then Verify Success Message "Password updated successfully" on Page
     Then Login with Changed Password for "Email" "renga_1510211974338@mailinator.com"
     Then Validate Branding logo URL "BP" on "ToDo" Page
  @BorrowerPortalRestPassword
  Scenario: Rest the password by providing NewPassword and Confirm Password field with different value
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Click on Forgot Link
     Then Validate Message "Forgot Password" on Rest Password page
     Then provide "Email" "renga_1510211974338@mailinator.com" and Click on Send Rest link button
     Then Validate Success Message "Email sent successfully" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "renga_1510211974338@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
     Then Rest by Providing New Password and Confirm Password field with "different" value
     Then Verify Error Message "Password and Confirm Password should match" on Page
  @BorrowerPortalRestPassword
  Scenario: Rest the password by providing NewPassword and Confirm Password field with invalid value
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Click on Forgot Link
     Then Validate Message "Forgot Password" on Rest Password page
     Then provide "Email" "renga_1510211974338@mailinator.com" and Click on Send Rest link button
     Then Validate Success Message "Email sent successfully" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "renga_1510211974338@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
     Then Rest by Providing New Password and Confirm Password field with "invalid" value
     Then Verify Error Message "Your password must be eight characters including one uppercase letter, one special character and alphanumeric characters" on Page
  @BorrowerPortalRestPassword
  Scenario: Rest the password by providing NewPassword and Confirm Password field with current Password value
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Click on Forgot Link
     Then Validate Message "Forgot Password" on Rest Password page
     Then provide "Email" "renga_1510211974338@mailinator.com" and Click on Send Rest link button
     Then Validate Success Message "Email sent successfully" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "renga_1510211974338@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
     Then Rest by Providing New Password and Confirm Password field with "current Password" value
     Then Verify Error Message "The new password cannot be accepted. The entered value is the same as current passwords" on Page
