Feature: Application submission through Borrower portal

@AllSuccessSyndicationmm
  Scenario Outline: All Success
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate presence of "Financial Review Tab"
     Then Link Plaid Bank "Bank of America"
     Then Submit "Financial Review Tab"
     Then Validate presence of "ToDoPageForActiveApplication" with Bank Linked
     
    Examples: 
      | Sceanrio                | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | All Syndication Success | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | All Syndication Success | 2500          | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
  
  @SaveLead
  Scenario Outline: Add / Save lead with Basic Tab
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
    Examples: 
      | Scenario            | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Save lead basic Tab | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
  
  @SaveLead
  Scenario Outline: Add / Save lead with BusinessTab
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
    Examples: 
      | Scenario               | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Save lead Business Tab | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
  
  @SaveLead
  Scenario Outline: Add / Save lead with OwnersTab
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate presence of "Financial Review Tab"
    Examples: 
      | Scenario             | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Save lead Owners Tab | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
  
  @ValidationMsgOnBorrowerPortal
  Scenario Outline: Validate Duplicate Email check on Basic Tab
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab" with Duplicate Email "renga_1510049117289@mailinator.com"
     Then Validate Error Message for "Duplicate Email Check"
    Examples: 
      | Scenario                           | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Duplicate Email check on Basic Tab | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
  
  @ValidationMsgOnBorrowerPortal
  Scenario Outline: Validate Business Start should not be further
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate Error Message for "Business Start Date as Further date"
    Examples: 
      | Scenario                                      | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Validate Business Start should not be further | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/2018        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
  
  @ValidationMsgOnBorrowerPortal
  Scenario Outline: Validate OwnerShip Should not be Zero
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate Error Message for "OwnerShip Zero"
    Examples: 
      | Scenario                              | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Validate OwnerShip Should not be Zero | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 0         | Office           | Texas         | 
  
  @ValidationMsgOnBorrowerPortal
  Scenario Outline: Validate OwnerShip Should not be Greater than 100
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate Error Message for "OwnerShip GT 100"
    Examples: 
      | Scenario                                          | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Validate OwnerShip Should not be Greater than 100 | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 101       | Office           | Texas         | 
  
  @ValidationMsgOnBorrowerPortal
  Scenario Outline: Validate Age Should be Lesser than 18
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate Error Message for "Age LT 18"
    Examples: 
      | Scenario                              | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Validate Age Should be Lesser than 18 | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/2006 | 131231231 | 80        | Office           | Texas         | 
  
  @DeclineDueToDataMerch
  Scenario Outline: Auto Decline due to Fien is not good standing
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate presence of "Financial Review Tab"
     Then Link Plaid Bank "Bank of America"
     Then Submit "Financial Review Tab"
     Then Validate presence of "ToDoPageForDeclineApplication"
    Examples: 
      | Sceanrio                           | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Auto decline fien Default Account  | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114165678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Auto decline fien Fraudulent       | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114265678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Auto decline fien Slow Pay         | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114365678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Auto decline fien Split Payer      | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114565678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Auto decline fien Stacking History | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114665678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Auto decline fien Other            | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114765678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Auto decline fien Criminal History | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114865678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
  
  @DeclineDueToWhitePages
  Scenario Outline: Auto decline WhitePages IP address is ouside the US
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate presence of "Financial Review Tab"
     Then Link Plaid Bank "Bank of America"
     Then Submit "Financial Review Tab"
     Then Validate presence of "ToDoPageForDeclineApplication"
    Examples: 
      | Sceanrio                                                                   | AmountSeeking | BusinessName             | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Auto decline WhitePages IP address is ouside the US                        | 12000         | Anchor Bay Entertainment | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Auto Decline due to IP address is ouside the US and Address not commerical | 12000         | CloudSend                | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
  
  @ManualReviewDataVerification
  Scenario Outline: Manual review Application Data Verification
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate presence of "Financial Review Tab"
     Then Link Plaid Bank "Bank of America"
     Then Submit "Financial Review Tab"
     Then Validate presence of "ToDoPageForActiveApplication" with Bank Linked
    Examples: 
      | Sceanrio                                                                        | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Manual review due to loan amount less than 2500                                 | 2499          | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual review due to time in business less than 9 months                        | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/08/2017        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual review due to time in business less than 2 years for home based business | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/2016        | 06/06/1979 | 131231231 | 80        | Home             | Texas         | 
      | Manual review due to CA based business                                          | 12000         | Berkshire Hathaway | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | California    | 
  
  @ManualReviewWhitePages
  Scenario Outline: Manual Review WhitePages verification
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate presence of "Financial Review Tab"
     Then Link Plaid Bank "Bank of America"
     Then Submit "Financial Review Tab"
     Then Validate presence of "ToDoPageForActiveApplication" with Bank Linked
    Examples: 
      | Sceanrio                                                                                          | AmountSeeking | BusinessName              | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Manual Review due to Proxy IP addresses                                                           | 12000         | Apple Inc                 | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Disposable Email                                                             | 12000         | ExxonMobil                | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Line Type Non-fixed VOIP                                                     | 12000         | McKesson Corporation      | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Line Type Toll Free                                                          | 12000         | Walmart                   | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Email first seen  LST 90 days                                                | 12000         | ABM Industries            | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Phone No is PrePaid                                                          | 12000         | ABS Capital Partners      | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to IP Address distamce GRT 1000                                                 | 12000         | AC Lens                   | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review when Phone Country Code is different country than physical address                  | 12000         | Academi                   | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Invalid Phone                                                                | 12000         | Oreck Corporation         | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Invalid Address                                                              | 12000         | Ameriprise Financial      | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Invalid Email                                                                | 12000         | AmerisourceBergen         | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Name is not linked with Phone                                                | 12000         | Ametek                    | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Name is not linked with Address                                              | 12000         | American Licorice Company | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Name is not linked with Email                                                | 12000         | Amys Kitchen              | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Proxy IP addresses Disposable Email and Line  Type Non-fixed VOIP            | 12000         | Browning Arms Company     | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Email first seen <90 days Phone is prepaid and IP  Address distance GRT 1000 | 12000         | Sunny Delight Beverages   | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Address not commerical                                                       | 12000         | Otho                      | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
  
  
  @ManualReviewYelp
  Scenario Outline: Manual Review Yelp verification
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate presence of "Financial Review Tab"
     Then Link Plaid Bank "Bank of America"
     Then Submit "Financial Review Tab"
     Then Validate presence of "ToDoPageForActiveApplication" with Bank Linked
    Examples: 
      | Sceanrio                                             | AmountSeeking | BusinessName       | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Manual Review yelp isclosed is true and Rating is 1  | 12000         | Berkshire Hathaway | Test     | Automation    | 8978245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review yelp isclosed is false and Rating is 1 | 12000         | Berkshire Hathaway | Test     | Automation    | 8778245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review yelp isclosed is true and Rating is 2  | 12000         | Berkshire Hathaway | Test     | Automation    | 8678245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review yelp isclosed is false and Rating is 0 | 12000         | Berkshire Hathaway | Test     | Automation    | 8578245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review yelp isclosed is true and Rating is 0  | 12000         | Berkshire Hathaway | Test     | Automation    | 8478245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
  
  @ManualReviewSICCodeVerification
  Scenario Outline: Manual Review SIC code verification
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate presence of "Financial Review Tab"
     Then Link Plaid Bank "Bank of America"
     Then Submit "Financial Review Tab"
     Then Validate presence of "ToDoPageForActiveApplication" with Bank Linked
    Examples: 
      | Sceanrio                                                 | AmountSeeking | BusinessName     | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Manual Review due to Both SIC_1 and SIC_2 are Prohibited | 12000         | BlackRock        | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to Both SIC_1 and SIC_2 are Restricted | 12000         | Blackstone Group | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to SIC_1 is Restricted                 | 12000         | Birdwell         | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to SIC_2 is Restricted                 | 12000         | Biomet           | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to SIC_2 is Prohibited                 | 12000         | Biogen Idec      | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual Review due to SIC_1 is Prohibited                 | 12000         | Big Lots         | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
  
  @ManualReviewPaynet
  Scenario Outline: Manual review pending paynet call
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Sceanrio <Sceanrio> AmountSeeking <AmountSeeking> and BusinessName <BusinessName> and FirstName <FirstName> and LastName <LastName> and PhoneNo <PhoneNo> and TaxId <TaxId> and BusinessStartDate <BusinessStartDate> and OwnerDOB <OwnerDOB> and OwnerSSN <OwnerSSN> and OwnerShip <OwnerShip> and BusinessLocation <BusinessLocation> and BusinessState <BusinessState>
     Then Submit "BasicTab"
     Then Validate presence of "BusinessTab"
     Then Submit "BusinessTab"
     Then Validate presence of "OwnersTab"
     Then Submit "OwnersTab"
     Then Validate presence of "Financial Review Tab"
     Then Link Plaid Bank "Bank of America"
     Then Submit "Financial Review Tab"
     Then Validate presence of "ToDoPageForActiveApplication" with Bank Linked
    Examples: 
      | Sceanrio                                                          | AmountSeeking | BusinessName   | FirstName | LastName | PhoneNo    | TaxId     | BusinessStartDate | OwnerDOB   | OwnerSSN  | OwnerShip | BusinessLocation | BusinessState | 
      | Manual review due to more than one company score greater than 90  | 12000         | Cargill        | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual review due to more than one company score between 60 to 90 | 12000         | KKR            | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | Manual review due to more than one company score less than 60     | 12000         | Primerica      | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | One company Match Score between 60 to 90 Delinquency GTR than 30  | 12000         | Leidos         | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | One company Match Score less than 60 Delinquency LST than 30      | 12000         | Fortive        | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | One company Match Score GTR than 90 Delinquency GTR than 30       | 12000         | Go Daddy       | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | One company Match Score between 60 to 90 Delinquency Equal to 30  | 12000         | Kohler Company | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | One company Match Score less than 60 Delinquency Equal to 30      | 12000         | Kraft Heinz    | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | One company Match Score GTR than 90 Delinquency Equal to 30       | 12000         | Line and Space | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
      | One company Match Score less than 60 Delinquency GTR than 30      | 12000         | Little Caesars | Test     | Automation    | 8878245952 | 114465678 | 06/06/1990        | 06/06/1979 | 131231231 | 80        | Office           | Texas         | 
