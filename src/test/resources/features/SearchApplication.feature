Feature: Search Functionality on BackOffice

  Background: 
    Given Browser "chrome" and navigate to "BO Office"
     When Login with "Username" "mrrajan" and Password "Sigma@123"
     Then Validate Branding logo URL "BO" on "Search" Page
  @SearchApplication
  Scenario: Perform Search with valid Application Number against ProductID system
     Then Search with two parameter "ApplicationNumber" "FC0000065" against "ProductID" "system"
     Then Validate Search result with "FC0000065"
  @SearchApplication
  Scenario: Perform Search with valid Application Number against ProductID StarterMCA
     Then Search with two parameter "ApplicationNumber" "FC0000062" against "ProductID" "StarterMCA"
     Then Validate Search result with "FC0000062"
  @SearchApplication
  Scenario: Perform Search with valid Application Number against ProductID TraditionalMCA
     Then Search with two parameter "ApplicationNumber" "FC0000058" against "ProductID" "TraditionalMCA"
     Then Validate Search result with "FC0000058"
  @SearchApplication
  Scenario: Perform Search with valid Application Number against ProductID StreamLineLoc
     Then Search with two parameter "ApplicationNumber" "FC0000064" against "ProductID" "StreamLineLoc"
     Then Validate Search result with "FC0000064"
  @SearchApplication
  Scenario: Perform Search with valid Application Number against ProductID PremiumLoc
     Then Search with two parameter "ApplicationNumber" "FC0000060" against "ProductID" "PremiumLoc"
     Then Validate Search result with "FC0000060"
  @SearchApplication
  Scenario: Perform Search with valid Application Number against ProductID OtherLenders
     Then Search with two parameter "ApplicationNumber" "FC0000048" against "ProductID" "OtherLenders"
     Then Validate Search result with "FC0000048"
  @SearchApplication
  Scenario: Perform Search with valid FirstName against ProductID StreamLineLoc
     Then Search with two parameter "FirstName" "AutomationTestUser" against "ProductID" "StreamLineLoc"
     Then Validate Search result with "FC0000067"
  @SearchApplication
  Scenario: Perform Search with valid BusinessEmail against ProductID StarterMCA
     Then Search with two parameter "BusinessEmail" "khkk@gmail.com" against "ProductID" "StarterMCA"
     Then Validate Search result with "FC0000052"
  @SearchApplication
  Scenario: Perform Search with valid MobileNumber against ProductID OtherLenders
     Then Search with two parameter "MobileNumber" "3568901231" against "ProductID" "OtherLenders"
     Then Validate Search result with "FC0000066"
  @SearchApplication
  Scenario: Perform Search with valid BusinessName against ProductID TraditionalMCA
     Then Search with two parameter "BusinessName" "Automation" against "ProductID" "TraditionalMCA"
     Then Validate Search result with "FC0000068"
  @SearchApplication
  Scenario: Perform Search with valid LastName against ProductID PremiumLoc
     Then Search with two parameter "LastName" "QA" against "ProductID" "PremiumLoc"
     Then Validate Search result with "FC0000069"
  @SearchApplication
  Scenario: Perform Search with valid BusinessName and Mobile Number against ProductID PremiumLoc
     Then Search with three parameter "BusinessName" "JackRabbit" AND "MobileNumber" "7657657647" against "ProductID" "PremiumLoc"
     Then Validate Search result with "FC0000060"
  @SearchApplication
  Scenario: Perform Search with valid ApplicatioNumber and BusinessEmail against ProductID StreamLineLoc
     Then Search with three parameter "ApplicationNumber" "FC0000067" AND "BusinessEmail" "wey@jchmxxbauiwu.com" against "ProductID" "StreamLineLoc"
     Then Validate Search result with "FC0000067"
  @SearchApplication
  Scenario: Perform Search with Invalid Application Number against ProductID PremiumLoc
     Then Search with two parameter "ApplicationNumber" "FC0000065" against "ProductID" "PremiumLoc"
     Then Validate Message "No applications found." on search page
  @SearchApplication
  Scenario: Perform Search with Invalid BusinessName and Mobile Number against ProductID OtherLenders
     Then Search with three parameter "BusinessName" "JackRabbit" AND "MobileNumber" "7657657647" against "ProductID" "OtherLenders"
     Then Validate Message "No applications found." on search page
  @SearchApplication
  Scenario: Perform Search with Invalid FirstName against ProductID StarterMCA
     Then Search with two parameter "FirstName" "AutomationTestUser" against "ProductID" "StarterMCA"
     Then Validate Message "No applications found." on search page
  @SearchApplication
  Scenario: Perform Search with Invalid LastName against and Invalid Mobile Number ProductID TraditionalMCA
     Then Search with three parameter "LastName" "SystemUser" AND "MobileNumber" "7657657647" against "ProductID" "TraditionalMCA"
     Then Validate Message "No applications found." on search page
  
