Feature: Branding Logo on Backoffice
@BackOfficeBrandingLogo
  Scenario: Validate Branding logo on BackOffice Home Page
    Given Browser "chrome" and navigate to "BO Office"
     Then Validate Branding logo URL "BO" on "Login" Page
@BackOfficeBrandingLogo    
  Scenario: Validate Branding logo on Search Page of Backoffice
    Given Browser "chrome" and navigate to "BO Office"
     When Login with "Username" "system" and Password "system"
     Then Validate Branding logo URL "BO" on "Search" Page
@BackOfficeBrandingLogo
  Scenario: Validate Branding logo on Home page After User Logout from Back office
    Given Browser "chrome" and navigate to "BO Office"
     When Login with "Username" "system" and Password "system"
      And Logout from "BO"
     Then Validate Branding logo URL "BO" on "Login" Page