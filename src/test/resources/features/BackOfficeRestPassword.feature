Feature: RestPassword Functionality on BackOffice
@BackOfficeRestPassword
  Scenario: Validate whether Reset Password page displayed or not
    Given Browser "chrome" and navigate to "BO Office"
     When Click on Forgot Link
     Then Validate Message "Having trouble signing in?" on Rest Password page
@BackOfficeRestPassword
  Scenario: Validate Reset Password by providing valid UserName
    Given Browser "chrome" and navigate to "BO Office"
     When Click on Forgot Link
     Then Validate Message "Having trouble signing in?" on Rest Password page
     Then provide "UserName" "Automation" and Click on Send Rest link button
     Then Validate Success Message "E-mail to reset your password has been sent" on Rest Password page
 @BackOfficeRestPassword
  Scenario: Validate Reset Password by providing Invalid UserName
    Given Browser "chrome" and navigate to "BO Office"
     When Click on Forgot Link
     Then Validate Message "Having trouble signing in?" on Rest Password page
     Then provide "UserName" "Aut" and Click on Send Rest link button
     Then Validate Success Message "E-mail to reset your password has been sent" on Rest Password page
 @BackOfficeRestPassword     
  Scenario: Validate Whether Reset Password mail triggered or not
    Given Browser "chrome" and navigate to "BO Office"
     When Click on Forgot Link
     Then Validate Message "Having trouble signing in?" on Rest Password page
     Then provide "UserName" "Automation" and Click on Send Rest link button
     Then Validate Success Message "E-mail to reset your password has been sent" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "rengaraja000000@mailinator.com"
@BackOfficeRestPassword  
  Scenario: Verify Rest Password Page
    Given Browser "chrome" and navigate to "BO Office"
     When Click on Forgot Link
     Then Validate Message "Having trouble signing in?" on Rest Password page
     Then provide "UserName" "Automation" and Click on Send Rest link button
     Then Validate Success Message "E-mail to reset your password has been sent" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "rengaraja000000@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
@BackOfficeRestPassword
  Scenario: Rest the password by providing NewPassword and Confirm Password field with same value
    Given Browser "chrome" and navigate to "BO Office"
     When Click on Forgot Link
     Then Validate Message "Having trouble signing in?" on Rest Password page
     Then provide "UserName" "Automation" and Click on Send Rest link button
     Then Validate Success Message "E-mail to reset your password has been sent" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "rengaraja000000@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
     Then Rest by Providing New Password and Confirm Password field with "same" value
     Then Verify Success Message "Password Reset" on Page
@BackOfficeRestPassword
  Scenario: Login with New or Changed Password
    Given Browser "chrome" and navigate to "BO Office"
     When Click on Forgot Link
     Then Validate Message "Having trouble signing in?" on Rest Password page
     Then provide "UserName" "Automation" and Click on Send Rest link button
     Then Validate Success Message "E-mail to reset your password has been sent" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "rengaraja000000@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
     Then Rest by Providing New Password and Confirm Password field with "same" value
     Then Verify Success Message "Password Reset" on Page
     Then Login with Changed Password for "Username" "Automation"
     Then Validate Branding logo URL "BO" on "Search" Page
@BackOfficeRestPassword   
  Scenario: Rest the password by providing NewPassword and Confirm Password field with Different value
    Given Browser "chrome" and navigate to "BO Office"
     When Click on Forgot Link
     Then Validate Message "Having trouble signing in?" on Rest Password page
     Then provide "UserName" "Automation" and Click on Send Rest link button
     Then Validate Success Message "E-mail to reset your password has been sent" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "rengaraja000000@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
     Then Rest by Providing New Password and Confirm Password field with "different" value
     Then Verify Error Message "Please enter the same value again." on Page
@BackOfficeRestPassword         
  Scenario: Rest the password by providing NewPassword and Confirm Password field with invalid value
    Given Browser "chrome" and navigate to "BO Office"
     When Click on Forgot Link
     Then Validate Message "Having trouble signing in?" on Rest Password page
     Then provide "UserName" "Automation" and Click on Send Rest link button
     Then Validate Success Message "E-mail to reset your password has been sent" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "rengaraja000000@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
     Then Rest by Providing New Password and Confirm Password field with "invalid" value
     Then Verify Error Message "Bad Request" on Page
@BackOfficeRestPassword     
  Scenario: Rest the password by providing NewPassword and Confirm Password field with current Password value
    Given Browser "chrome" and navigate to "BO Office"
     When Click on Forgot Link
     Then Validate Message "Having trouble signing in?" on Rest Password page
     Then provide "UserName" "Automation" and Click on Send Rest link button
     Then Validate Success Message "E-mail to reset your password has been sent" on Rest Password page
     Then Navigate to mailnator and check whether Rest Password mail triggerred to "rengaraja000000@mailinator.com"
     Then Verify Rest Password page by clicking on Rest password link from Email body
     Then Rest by Providing New Password and Confirm Password field with "current Password" value
     Then Verify Error Message "Bad Request" on Page
