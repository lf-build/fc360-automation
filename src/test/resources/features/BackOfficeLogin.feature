Feature: Login Functionality on BackOffice
@BackOfficeLogin
  Scenario: Login with valid UserName and Password
    Given Browser "chrome" and navigate to "BO Office"
     When Login with "Username" "system" and Password "system"
     Then Validate Branding logo URL "BO" on "Search" Page
 @BackOfficeLogin        
  Scenario: Login with Invalid UserName and Valid Password
    Given Browser "chrome" and navigate to "BO Office"
     When Login with "Username" "stem" and Password "system"
     Then Validate Error Message "Invalid Username or Password" on "Login" Page
 @BackOfficeLogin       
  Scenario: Login with valid UserName and Invalid Password
    Given Browser "chrome" and navigate to "BO Office"
     When Login with "Username" "system" and Password "syem"
     Then Validate Error Message "Invalid Username or Password" on "Login" Page
@BackOfficeLogin      
  Scenario: Login with Invalid UserName and Invalid Password
    Given Browser "chrome" and navigate to "BO Office"
     When Login with "Username" "syem" and Password "syem"
     Then Validate Error Message "Invalid Username or Password" on "Login" Page
@BackOfficeLogin 
  Scenario: Login with valid UserName and Password and Without Accepting consent
    Given Browser "chrome" and navigate to "BO Office"
     When Login with "Username" "syem" and Password "syem" and without Accepting consent
     Then Validate Error Message "Please accept the consent." for Consent
  
  
