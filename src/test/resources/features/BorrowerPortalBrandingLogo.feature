Feature: Branding Logo on Borrower portal
@BorrowerPortalBrandingLogo
  Scenario: Validate Branding logo on Borrower portal Home Page
    Given Browser "chrome" and navigate to "BorrowerPortal"
     Then Validate Branding logo URL "BP" on "Home" Page
@BorrowerPortalBrandingLogo   
  Scenario: Validate Branding logo on Search Page of Backoffice
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Login with "Email" "renga_1510217671245@mailinator.com" and Password "Sigma@1234"
     Then Validate Branding logo URL "BP" on "ToDo" Page
@BorrowerPortalBrandingLogo
  Scenario: Validate Branding logo on Home page After User Logout from Back office
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Login with "Email" "renga_1510217671245@mailinator.com" and Password "Sigma@1234"
      And Logout from "BP"
     Then Validate Branding logo URL "BP" on "Home" Page