Feature: Login Functionality on Borrower Portal
@BorrowerPortalLogin
  Scenario: Login with valid EmailId and Password
   Given Browser "chrome" and navigate to "BorrowerPortal"
     When Login with "Email" "renga_1510217671245@mailinator.com" and Password "Sigma@1234"
     Then Validate Branding logo URL "BP" on "ToDo" Page
@BorrowerPortalLogin      
  Scenario: Login with Invalid EmailId and Valid Password
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Login with "Email" "renga_151045@mailinator.com" and Password "Sigma@1234"
     Then Validate Error Message "The email address or password you entered is incorrect." on "SignIn" Page
@BorrowerPortalLogin       
  Scenario: Login with valid EmailId and Invalid Password
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Login with "Email" "renga_1510217671245@mailinator.com" and Password "Sigma@"
     Then Validate Error Message "The email address or password you entered is incorrect." on "SignIn" Page
@BorrowerPortalLogin      
  Scenario: Login with Invalid EmailId and Invalid Password
    Given Browser "chrome" and navigate to "BorrowerPortal"
     When Login with "Email" "renga_151021245@mailinator.com" and Password "Sigma@"
     Then Validate Error Message "The email address or password you entered is incorrect." on "SignIn" Page

  
  
